var favorites = {
    refresh_after_ajax: false,
    
    /**
     * This function update or create a favorite. Once the ajax call is complete, if refresh_after_ajax is true, it refresh the location.
     *
     * @return  {void}
     */
    add: function() {
        var new_lists = [];
        var bind_to_lists = [];
        
        $('#fav-list>.new-list').each(function() {
            new_lists.push({
                name: $(this).find(':input[type=text]').val(),
                bind: $(this).find(':checkbox').prop('checked'),
            });
        });
        
        $('#fav-list>:not(.new-list) :checked').each(function() {
            bind_to_lists.push($(this).val());
        });
        
        $.ajax({
            url: $('#fav-url-manager').attr("data-add-url"),
            data: {
                type: $('#fav-item-type').val(),
                id: $('#fav-item-id').val(),
                note: $('#fav-item-note').val(),
                new_lists: JSON.stringify(new_lists),
                bind_to_lists: JSON.stringify(bind_to_lists),
            },
            dataType: 'json',
            method: 'post',
            success: function (data) {
                if (data.error) {
                    alert(data.error);
                    return;
                }
                
                $('a.fav-link').attr('title', 'Retirer des favoris').find('i').removeClass('far').addClass('fas');
                $('#add-item-to-fav').modal('hide');
                
                if (favorites.refresh_after_ajax) {
                    window.location.reload();
                }
            }
        });
    },
    
    copy_item: function( event, ui ) {
        console.log(
            'data-type',
            $(ui.draggable[0]).closest('li[data-type]').attr('data-type'),
            'with id',
            $(ui.draggable[0]).closest('li[data-item-id]').attr('data-item-id'),
            'must be copied in target',
            $(event.target).closest('.card').attr('data-fav-list-id')
        );
        
        $.ajax({
            url: $('#fav-url-manager').attr("data-copy-item-url"),
            data: {
                type: $(ui.draggable[0]).closest('li[data-type]').attr('data-type'),
                id: $(ui.draggable[0]).closest('li[data-item-id]').attr('data-item-id'),
                add_to_list: $(event.target).closest('.card').attr('data-fav-list-id'),
            },
            dataType: 'json',
            method: 'post',
            success: function (data) {
                if (data.error) {
                    alert(data.error);
                    return;
                }
                
                window.location.reload();
            }
        });
    },
    
    /**
     * This function delete definitively a favorite. Once the ajax call is complete, if refresh_after_ajax is true, it refresh the location.
     *
     * @return  {void}
     */
    del: function() {
        $.ajax({
            url: $('#fav-url-manager').attr("data-del-url"),
            data: {
                type: $('#fav-item-type').val(),
                id: $('#fav-item-id').val(),
            },
            dataType: 'json',
            method: 'post',
            success: function (data) {
                if (data.error) {
                    alert(data.error);
                    return;
                }
                
                $('a.fav-link').attr('title', 'Ajouter aux favoris').find('i').removeClass('fas').addClass('far');
                $('#editItemToFav').modal('hide');
                
                if (favorites.refresh_after_ajax) {
                    window.location.reload();
                }
            }
        });
    },
    
    /**
     * This function is called only on the favorites list page. It sets up the form to know what to do once the list is deleted.
     *
     * @return  {void}
     */
    del_list: function() {
        $('#fav-list-id').val(parseInt($(this).closest('.card').attr('data-fav-list-id'), 10));
        $('#fav-list-item-action').val('keep');
        favorites.refresh_after_ajax = true;
        $('#delete-list').modal('show');
    },
    
    /**
     * This function is called only on the favorites list page. It sets up the form to be pointing to the good favorite to update.
     *
     * @return  {void}
     */
    edit_item: function() {
        $('#fav-item-type').val($(this).closest('li').attr('data-type'));
        $('#fav-item-id').val(parseInt($(this).closest('li').attr('data-item-id'), 10));
        $('#fav-item-note').val($(this).closest('li').find('.fav-note').text());
        $('#fav-list :checkbox').prop('checked', false);
        $(this).closest('li').attr('data-fav-lists-id').split(',').forEach(list_id => {
            if (list_id !== '') {
                $('#fav-list :checkbox[value='+list_id+']').prop('checked', true);
            }
        });
        favorites.refresh_after_ajax = true;
        $('#edit-item').modal('show');
    },
    
    /**
     * This function is called only on the favorites list page. It sets up the form to be pointing to the good list to update.
     *
     * @return  {void}
     */
    edit_list: function() {
        var list_id = parseInt($(this).closest('.card').attr('data-fav-list-id'), 10);
        if (isNaN(list_id)) {
            list_id = 0;
        }
        $('#fav-list-id').val(list_id);
        $('#fav-list-name').val($(this).closest('.card-header').find('.fav-list-title').text());
        favorites.refresh_after_ajax = true;
        $('#edit-list').modal('show');
    },
    
    move_item: function( event, ui ) {
        console.log(
            'data-type',
            $(ui.draggable[0]).closest('li[data-type]').attr('data-type'),
            'with id',
            $(ui.draggable[0]).closest('li[data-item-id]').attr('data-item-id'),
            'of list ',
            $(ui.draggable[0]).closest('.card').attr('data-fav-list-id'),
            'must be moved to target',
            $(event.target).closest('.card').attr('data-fav-list-id')
        );
        
        $.ajax({
            url: $('#fav-url-manager').attr("data-copy-item-url"),
            data: {
                type: $(ui.draggable[0]).closest('li[data-type]').attr('data-type'),
                id: $(ui.draggable[0]).closest('li[data-item-id]').attr('data-item-id'),
                add_to_list: $(event.target).closest('.card').attr('data-fav-list-id'),
                remove_from_list: $(ui.draggable[0]).closest('.card').attr('data-fav-list-id'),
            },
            dataType: 'json',
            method: 'post',
            success: function (data) {
                if (data.error) {
                    alert(data.error);
                    return;
                }
                
                window.location.reload();
            }
        });
    },
    
    new_list: function() {
        var new_div = $('<div class="col-12 col-md-6 new-list">' +
            '<div class="input-group input-group-sm mb-3">' +
                '<div class="input-group-prepend">' +
                    '<div class="input-group-text">' +
                        '<input type="checkbox">' +
                    '</div>' +
                '</div>' +
                '<input type="text" class="form-control" aria-label="Text input with checkbox" placeholder="Nom">' +
                '<div class="input-group-append">' +
                    '<button class="btn btn-danger" type="button"><i class="fas fa-times"></i></button> ' +
                '</div>' +
            '</div>' +
        '</div>');
        
        new_div.find('button.btn-danger').on('click', function() {
            $(this).closest('.new-list').remove();
        });
        $('#fav-list').append(new_div);
        
        return false;
    },
    
    order: function(event) {
        new_order = $(event.target).find('div.sortable-item').map(function(index, node) {
            return {
                key: parseInt($(node).find('.card').attr('data-fav-list-id'), 10),
                order: index,
            };
        }).get();
        
        $.ajax({
            url: $('#fav-url-manager').attr("data-order-url"),
            data: {
                new_order: JSON.stringify(new_order),
            },
            dataType: 'json',
            method: 'post',
            success: function (data) {
                if (data.error) {
                    alert(data.error);
                    return;
                }
            }
        });
    },
    
    rm_item: function() {
        $.ajax({
            url: $('#fav-url-manager').attr("data-delete-item-url"),
            data: {
                type: $(this).closest('li').attr('data-type'),
                id: parseInt($(this).closest('li').attr('data-item-id'), 10),
                list: parseInt($(this).closest('div.card').attr('data-fav-list-id'), 10),
            },
            dataType: 'json',
            method: 'post',
            success: function (data) {
                if (data.error) {
                    alert(data.error);
                    return;
                }
                
                window.location.reload();
            }
        });
    },
    
    rm_list: function() {
        $.ajax({
            url: $('#fav-url-manager').attr("data-delete-list-url"),
            data: {
                id: $('#fav-list-id').val(),
                action: $('#fav-list-item-action').val(),
            },
            dataType: 'json',
            method: 'post',
            success: function (data) {
                if (data.error) {
                    alert(data.error);
                    return;
                }
                
                window.location.reload();
            }
        });
    },
    
    save_item: function() {
        var new_lists = [];
        var bind_to_lists = [];
        
        $('#fav-list>.new-list').each(function() {
            new_lists.push({
                name: $(this).find(':input[type=text]').val(),
                bind: $(this).find(':checkbox').prop('checked'),
            });
        });
        
        $('#fav-list>:not(.new-list) :checked').each(function() {
            bind_to_lists.push($(this).val());
        });
        
        $.ajax({
            url: $('#fav-url-manager').attr("data-edit-item-url"),
            data: {
                type: $('#fav-item-type').val(),
                id: $('#fav-item-id').val(),
                note: $('#fav-item-note').val(),
                new_lists: JSON.stringify(new_lists),
                bind_to_lists: JSON.stringify(bind_to_lists),
            },
            dataType: 'json',
            method: 'post',
            success: function (data) {
                if (data.error) {
                    alert(data.error);
                    return;
                }
                
                $('a.fav-link').attr('title', 'Retirer des favoris').find('i').removeClass('far').addClass('fas');
                $('#add-item-to-fav').modal('hide');
                
                if (favorites.refresh_after_ajax) {
                    window.location.reload();
                }
            }
        });
    },
    
    save_list: function() {
        $.ajax({
            url: $('#fav-url-manager').attr("data-edit-list-url"),
            data: {
                id: $('#fav-list-id').val(),
                name: $('#fav-list-name').val(),
            },
            dataType: 'json',
            method: 'post',
            success: function (data) {
                if (data.error) {
                    alert(data.error);
                    return;
                }
                
                window.location.reload();
            }
        });
    },
    
    /**
     * This function is called only on a detailed item page. Its behaviour depends on whether the star is full (the item
     * is already a fav) or not (the item need to be a fav).
     *
     * @return  {void}
     */
    toggle: function() {
        if ($(this).find('i').is('.far')) {
            // Not favorite yet, we must ask the note.
            switch (location.pathname.split('/')[2]) {
                case 'weapon':
                case 'effect':
                case 'module':
                case 'armour':
                case 'enhancement':
                case 'mecha':
                    $('#add-item-to-fav').modal('show');
                    break;
                
                default:
                    return false;
            }
        }
        else {
            // Favorite already, delete it.
            favorites.del();
        }
    },
    
    /**
     * This function is called only on the favorites list page. It sets up the form to be pointing to the good favorite to delete.
     *
     * @return  {void}
     */
    trash_item: function() {
        $('#fav-item-type').val($(this).closest('li').attr('data-type'));
        $('#fav-item-id').val(parseInt($(this).closest('li').attr('data-item-id'), 10));
        favorites.refresh_after_ajax = true;
        favorites.del();
    },
};

$(function() {
    
    list_type_fav = ['weapon', 'module', 'enhancement', 'effect', 'armour', 'mecha'];
    
    $('.drop-zone').hide();
    
    $('.fav-link').off('click').on('click', favorites.toggle);
    $('.fav-edit').off('click').on('click', favorites.edit_item);
    $('.fav-trash').off('click').on('click', favorites.rm_item);
    
    $('.edit-fav-list>button').off('click').on('click', favorites.edit_list);
    $('.rm-fav-list>button').off('click').on('click', favorites.del_list);
    $('.fav-list-add').off('click').on('click', favorites.edit_list);
    $( ".fav-grip" ).draggable({
        revert: "invalid",
        helper: function(event, ui) {
            return $(event.target).closest('li').find('a.sortable-target').clone();
        },
        cursorAt: { left: 10, top: 10 },
        start: function() {
            $('.drop-zone').show();
            $(this).closest('div.card').find('div.drop-zone').hide();
            
            if ($(this).closest('div.card').attr('data-fav-list-id') === '0') {
                $('.drop-zone-move').removeClass('w-50').addClass('w-100');
                $('.drop-zone-copy').removeClass('d-table-cell').addClass('d-none');
            }
            else {
                $('.drop-zone-move').removeClass('w-100').addClass('w-50');
                $('.drop-zone-copy').removeClass('d-none').addClass('d-table-cell');
            }
        },
        stop: function(event) {
            $('.drop-zone').hide();
        },
    });
    
    
    $(" .sortable" ).sortable({
        items: "> .sortable-item",
        cursor: "grabbing",
        placeholder: "ui-state-highlight",
        update: favorites.order,
        forcePlaceholderSize: true,
        handle: 'h3',
    });
    $( ".drop-zone-copy" ).droppable({
        drop: favorites.copy_item,
    });
    $( ".drop-zone-move" ).droppable({
        drop: favorites.move_item,
    });
    
    $('#new-list').on('click', favorites.new_list);
    
    $('input[data-toggle-checkbox]').off('click').on('click', function() {
        $(this).closest('.input-group').find(':checkbox').click();
    });
});
