from rest_framework import serializers
from gm.models.character import Archetype
from .common import CharacteristicSerializer

class DetailArchetypeSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Archetype instance into JSON format from an Archetype Detail view."""
    characteristic_bonus_1 = CharacteristicSerializer(read_only=True)
    characteristic_bonus_2 = CharacteristicSerializer(read_only=True)
    characteristic_bonus_3 = CharacteristicSerializer(read_only=True)
    
    class Meta:
        model = Archetype
        fields = (
            'id',
            'name',
            'characteristic_bonus_1',
            'characteristic_bonus_2',
            'characteristic_bonus_3',
            'slug',
        )
        read_only_fields = (
            'id',
            'name',
            'characteristic_bonus_1',
            'characteristic_bonus_2',
            'characteristic_bonus_3',
            'slug',
        )

class ListArchetypeSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Archetype instance into JSON format from an Archetype List view."""
    characteristic_bonus_1 = CharacteristicSerializer(read_only=True)
    characteristic_bonus_2 = CharacteristicSerializer(read_only=True)
    characteristic_bonus_3 = CharacteristicSerializer(read_only=True)
    
    class Meta:
        model = Archetype
        fields = (
            'id',
            'name',
            'characteristic_bonus_1',
            'characteristic_bonus_2',
            'characteristic_bonus_3',
            'slug',
        )
        read_only_fields = (
            'id',
            'name',
            'characteristic_bonus_1',
            'characteristic_bonus_2',
            'characteristic_bonus_3',
            'slug',
        )