from rest_framework import serializers
from gm.models.weaponry import Effect, EnhancementEffect, Enhancement, Weapon
from .common import NameRelatedSerializer
from .weapons import WDWeaponCategorySerializer
from .effects import ELEffectSerializer

# ENHANCEMENT DETAIL SERIALIZERS: prefix BD (B for Better because E is taken for Effect :D )

class BDWeaponSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Weapon instance into JSON format from a Enhancement Detail view."""
    
    class Meta:
        model = Weapon
        fields = ('id', 'name', 'slug')
        read_only_fields = ('id', 'name', 'slug')

class BDEnhancementEffectSerializer(serializers.ModelSerializer):
    """Serializer to map the Model EnhancementEffect instance into JSON format from a Enhancement Detail view."""
    
    effect = ELEffectSerializer(read_only=True)
    
    class Meta:
        model = EnhancementEffect
        fields = ('effect_level', 'damage_dice', 'violence_dice', 'effect_condition', 'effect')
        read_only_fields = ('effect_level', 'damage_dice', 'violence_dice', 'effect_condition')

class BDEnhancementSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Enhancement instance into JSON format from a Enhancement Detail view."""
    
    group = serializers.SerializerMethodField()
    weapons = BDWeaponSerializer(many=True, read_only=True)
    effects = BDEnhancementEffectSerializer(many=True, read_only=True)
    category = WDWeaponCategorySerializer(read_only=True)
    
    def get_group(self, obj):
        return obj.get_group_display()
    
    class Meta:
        model = Enhancement
        fields = ('id', 'name', 'slug', 'description', 'restriction', 'cost', 'category', 'group', 'weapons', 'effects')
        read_only_fields = ('id', 'name', 'slug', 'description', 'restriction', 'cost')


# ENHANCEMENT LIST SERIALIZERS: prefix BL (see B explanation on detail :) )

class BLEnhancementSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Enhancement instance into JSON format from a Enhancement List view."""
    
    group = serializers.SerializerMethodField()
    category = WDWeaponCategorySerializer(read_only=True)
    
    def get_group(self, obj):
        return obj.get_group_display()
    
    class Meta:
        model = Enhancement
        fields = ('id', 'name', 'slug', 'description', 'cost', 'category', 'group')
        read_only_fields = ('id', 'name', 'slug', 'description', 'cost')
