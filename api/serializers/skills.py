from rest_framework import serializers
from gm.models.weaponry import HeroicSkill, HeroicSkillCategory
from .common import NameRelatedSerializer

class DetailCategorySerializer(serializers.ModelSerializer):
    """Serializer to map the Model HeroicSkill instance into JSON format from a heroic skill Detail view."""
    
    class Meta:
        model = HeroicSkillCategory
        fields = (
            'id',
            'name',
        )
        read_only_fields = (
            'id',
            'name',
        )

class DetailHeroicSkillSerializer(serializers.ModelSerializer):
    """Serializer to map the Model HeroicSkill instance into JSON format from a heroic skill Detail view."""
    activation = NameRelatedSerializer(read_only=True)
    category = DetailCategorySerializer(read_only=True)
    
    class Meta:
        model = HeroicSkill
        fields = (
            'id',
            'name',
            'description',
            'slug',
            'activation',
            'category',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'slug',
            'activation',
            'category',
        )

class ListHeroicSkillSerializer(serializers.ModelSerializer):
    """Serializer to map the Model HeroicSkill instance into JSON format from a heroic skill List view."""
    activation = NameRelatedSerializer(read_only=True)
    category = DetailCategorySerializer(read_only=True)
    
    class Meta:
        model = HeroicSkill
        fields = (
            'id',
            'name',
            'description',
            'slug',
            'activation',
            'category',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'slug',
            'activation',
            'category',
        )

class ListHeroicSkillWithoutCategorySerializer(serializers.ModelSerializer):
    """Serializer to map the Model HeroicSkill instance into JSON format from a heroic skill List view."""
    activation = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = HeroicSkill
        fields = (
            'id',
            'name',
            'description',
            'slug',
            'activation',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'slug',
            'activation',
        )

class ListHeroicSkillCategorySerializer(serializers.ModelSerializer):
    """Serializer to map the Model HeroicSkillCategory instance into JSON format from a heroic skill category (list or detail) view."""
    skills = ListHeroicSkillWithoutCategorySerializer(read_only=True, many=True)
    
    class Meta:
        model = HeroicSkillCategory
        fields = (
            'id',
            'name',
            'skills',
        )
        read_only_fields = (
            'id',
            'name',
            'skills',
        )