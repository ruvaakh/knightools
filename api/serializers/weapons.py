from rest_framework import serializers
from gm.models.weaponry import WeaponCategory, Effect, Enhancement, WeaponAttackEffect, WeaponAttack, Weapon
from .common import NameRelatedSerializer

# WEAPON CATEGORY SERIALIZERS: prefix WC

class WCWeaponAttackSerializer(serializers.ModelSerializer):
    """Serializer to map the Model WeaponAttack instance into JSON format from a Weapon Category (list or detail) view."""
    reach = NameRelatedSerializer(read_only=True)
    
    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = WeaponAttack
        fields = ('position', 'name', 'damage_dice', 'damage_bonus', 'violence_dice', 'violence_bonus', 'reach')
        read_only_fields = ('position', 'name', 'damage_dice', 'damage_bonus', 'violence_dice', 'violence_bonus', 'reach')

class WCWeaponSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Weapon instance into JSON format from a Weapon Category (list or detail) view."""
    attacks = WCWeaponAttackSerializer(many=True, read_only=True)
    rarity = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = Weapon
        fields = ('id', 'name', 'slug', 'description', 'rarity', 'cost', 'attacks')
        read_only_fields = ('id', 'name', 'slug', 'description', 'rarity', 'cost')

class WCWeaponCategorySerializer(serializers.ModelSerializer):
    """Serializer to map the Model WeaponCategory instance into JSON format from a Weapon Category (list or detail) view."""
    weapons = WCWeaponSerializer(many=True, read_only=True)
    
    class Meta:
        model = WeaponCategory
        fields = ('id', 'name', 'weapons')
        read_only_fields = ('id', 'name')

# WEAPON DETAIL SERIALIZERS: prefix WD

class WDEffectSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Effect instance into JSON format from a Weapon Detail view."""
    
    class Meta:
        model = Effect
        fields = ('id', 'name', 'slug', 'description')
        fields = ('id', 'name', 'slug', 'description')

class WDWeaponCategorySerializer(serializers.ModelSerializer):
    """Serializer to map the Model WeaponCategory instance into JSON format from a Weapon Detail view."""
    
    class Meta:
        model = WeaponCategory
        fields = ('id', 'name')
        read_only_fields = ('id', 'name')

class WDEnhancementSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Enhancement instance into JSON format from a Weapon Detail view."""
    
    class Meta:
        model = Enhancement
        fields = ('id', 'name', 'slug')
        read_only_fields = ('id', 'name', 'slug')

class WDWeaponAttackEffectSerializer(serializers.ModelSerializer):
    """Serializer to map the Model WeaponAttackEffect instance into JSON format from a Weapon Detail view."""
    effect = WDEffectSerializer(read_only=True)
    
    class Meta:
        model = WeaponAttackEffect
        fields = ('effect', 'effect_level', 'energy', 'effect_condition')
        read_only_fields = ('effect', 'effect_level', 'energy', 'effect_condition')

class WDWeaponAttackSerializer(serializers.ModelSerializer):
    """Serializer to map the Model WeaponAttack instance into JSON format from a Weapon Detailed view"""
    effects = WDWeaponAttackEffectSerializer(many=True, read_only=True)
    reach = NameRelatedSerializer(read_only=True)
    
    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = WeaponAttack
        fields = ('position', 'name', 'damage_dice', 'damage_bonus', 'violence_dice', 'violence_bonus', 'reach', 'energy', 'effects')
        read_only_fields = ('position', 'name', 'damage_dice', 'damage_bonus', 'violence_dice', 'violence_bonus', 'reach', 'energy', 'effects')

class WDWeaponSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Weapon instance into JSON format from a Weapon Detail view."""
    attacks = WDWeaponAttackSerializer(many=True, read_only=True)
    enhancements = WDEnhancementSerializer(many=True, read_only=True)
    category = WDWeaponCategorySerializer(read_only=True)
    rarity = NameRelatedSerializer(read_only=True)
    
    class Meta:
        """Meta class to map serializer's fields with the model fields."""
        model = Weapon
        fields = ('id', 'name', 'slug', 'description', 'nicknames', 'rarity', 'category', 'cost', 'enhancements', 'attacks', 'enhancements')
        read_only_fields = ('id', 'name', 'slug', 'description', 'nicknames', 'rarity', 'category', 'cost', 'enhancements', 'attacks', 'enhancements')

# WEAPON LIST SERIALIZERS: prefix WL

class WLWeaponSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Weapon instance into JSON format from a Weapon List view."""
    attacks = WCWeaponAttackSerializer(many=True, read_only=True)
    category = WDWeaponCategorySerializer(read_only=True)
    rarity = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = Weapon
        fields = ('id', 'name', 'slug', 'description', 'rarity', 'category', 'cost', 'attacks')
        read_only_fields = ('id', 'name', 'slug', 'description', 'rarity', 'category', 'cost')