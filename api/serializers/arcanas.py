from rest_framework import serializers
from gm.models.character import MajorArcana
from .common import NameRelatedSerializer

class DetailMajorArcanaSerializer(serializers.ModelSerializer):
    """Serializer to map the Model MajorArcana instance into JSON format from a major arcana Detail view."""
    forced_aspect = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = MajorArcana
        fields = (
            'id',
            'name',
            'description',
            'slug',
            'destiny_quote',
            'destiny_effect',
            'past',
            'creation_bonus',
            'aspect_point',
            'characteristic_point',
            'advantage_name',
            'advantage',
            'disadvantage_name',
            'disadvantage',
            'ai_description',
            'ai_letter',
            'ai_keywords',
            'ai_behavior',
            'ai_advantage_name',
            'ai_advantage',
            'ai_disadvantage_name',
            'ai_disadvantage',
            'roman_number',
            'number',
            'forced_aspect',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'slug',
            'destiny_quote',
            'destiny_effect',
            'past',
            'creation_bonus',
            'aspect_point',
            'characteristic_point',
            'advantage_name',
            'advantage',
            'disadvantage_name',
            'disadvantage',
            'ai_description',
            'ai_letter',
            'ai_keywords',
            'ai_behavior',
            'ai_advantage_name',
            'ai_advantage',
            'ai_disadvantage_name',
            'ai_disadvantage',
            'roman_number',
            'number',
            'forced_aspect',
        )

class ListMajorArcanaSerializer(serializers.ModelSerializer):
    """Serializer to map the Model MajorArcana instance into JSON format from a major arcana List view."""
    forced_aspect = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = MajorArcana
        fields = (
            'id',
            'name',
            'description',
            'slug',
            'destiny_quote',
            'destiny_effect',
            'past',
            'creation_bonus',
            'aspect_point',
            'characteristic_point',
            'advantage_name',
            'advantage',
            'disadvantage_name',
            'disadvantage',
            'ai_description',
            'ai_letter',
            'ai_keywords',
            'ai_behavior',
            'ai_advantage_name',
            'ai_advantage',
            'ai_disadvantage_name',
            'ai_disadvantage',
            'roman_number',
            'number',
            'forced_aspect',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'slug',
            'destiny_quote',
            'destiny_effect',
            'past',
            'creation_bonus',
            'aspect_point',
            'characteristic_point',
            'advantage_name',
            'advantage',
            'disadvantage_name',
            'disadvantage',
            'ai_description',
            'ai_letter',
            'ai_keywords',
            'ai_behavior',
            'ai_advantage_name',
            'ai_advantage',
            'ai_disadvantage_name',
            'ai_disadvantage',
            'roman_number',
            'number',
            'forced_aspect',
        )