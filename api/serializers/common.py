from rest_framework import serializers
from gm.models.character import Characteristic, NonPlayerCharacter, NonPlayerCharacterAspect, NonPlayerCharacterCharacteristic
from gm.models.weaponry import ModuleLevel, Module, ModuleCategory

class NameRelatedSerializer(serializers.RelatedField):
    """Serializer to return only the name value for the instance of Model."""
    def to_representation(self, value):
        return value.name

class CharacteristicSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Characteristic instance into JSON format."""
    
    aspect = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = Characteristic
        fields = ('name', 'aspect')
        read_only_fields = ('name', 'aspect')
    
class CommonModuleCategorySerializer(serializers.ModelSerializer):
    """Serializer to map the Model ModuleCategory instance into JSON format."""
    
    class Meta:
        model = ModuleCategory
        fields = ('id', 'name')
        read_only_fields = ('id', 'name')

class CommonModuleSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Module instance into JSON format."""
    category = CommonModuleCategorySerializer(read_only=True)
    
    class Meta:
        model = Module
        fields = ('id', 'name', 'slug', 'category')
        read_only_fields = ('id', 'name', 'slug', 'category')

class CommonModuleLevelSerializer(serializers.ModelSerializer):
    """Serializer to map the Model ModuleLevel instance into JSON format."""
    module = CommonModuleSerializer(read_only=True)
    activation = NameRelatedSerializer(read_only=True)
    reach = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = ModuleLevel
        fields = ('level', 'description', 'activation', 'duration', 'damage_dice', 'damage_bonus', 'violence_dice', 'violence_bonus', 'reach', 'energy', 'module')
        read_only_fields = ('level', 'description', 'activation', 'duration', 'damage_dice', 'damage_bonus', 'violence_dice', 'violence_bonus', 'reach', 'energy', 'module')

class CommonNonPlayerCharacterAspectSerializer(serializers.ModelSerializer):
    """Serializer to map the Model NonPlayerCharacterAspect instance into JSON format."""
    aspect = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = NonPlayerCharacterAspect
        fields = (
            'aspect',
            'basic_value',
            'except_value',
            'except_major',
        )
        read_only_fields = (
            'aspect',
            'basic_value',
            'except_value',
            'except_major',
        )

class CommonNonPlayerCharacterCharacteristicSerializer(serializers.ModelSerializer):
    """Serializer to map the Model ModuleLevelNPCCharacteristic instance into JSON format."""
    characteristic = CharacteristicSerializer(read_only=True)
    
    class Meta:
        model = NonPlayerCharacterCharacteristic
        fields = ('characteristic', 'test_dice', 'overdrive_bonus')
        read_only_fields = ('characteristic', 'test_dice', 'overdrive_bonus')

class CommonNonPlayerCharacterSerializer(serializers.ModelSerializer):
    """Serializer to map the Model NonPlayerCharacter instance into JSON format."""
    aspects = CommonNonPlayerCharacterAspectSerializer(read_only=True, many=True)
    characteristics = CommonNonPlayerCharacterCharacteristicSerializer(read_only=True, many=True)
    
    class Meta:
        model = NonPlayerCharacter
        fields = (
            'name',
            'defence',
            'reaction',
            'initiative',
            'health_points',
            'energy_points',
            'armour_points',
            'force_field',
            'cohesion',
            'outbreak',
            'speed',
            'simplified',
            'aspects',
            'characteristics',
        )
        read_only_fields = (
            'name',
            'defence',
            'reaction',
            'initiative',
            'health_points',
            'energy_points',
            'armour_points',
            'force_field',
            'cohesion',
            'outbreak',
            'speed',
            'simplified',
            'aspects',
            'characteristics',
        )
    