from rest_framework import serializers
from gm.models.character import Division, DivisionOverdrive
from gm.models.weaponry import DivisionModule, DivisionWeapon, Module, ModuleLevel
from .common import NameRelatedSerializer, CharacteristicSerializer, CommonModuleLevelSerializer
from .weapons import WLWeaponSerializer
from .modules import MDModuleCategorySerializer
from .effects import ELEffectSerializer


class DetailDivisionModuleSerializer(serializers.ModelSerializer):
    """Serializer to map the Model DivisionModule instance into JSON format from a Division Detail view."""
    module_level = CommonModuleLevelSerializer(read_only=True)
    forced_choice_effect = ELEffectSerializer(read_only=True)
    
    class Meta:
        model = DivisionModule
        fields = ('module_level','forced_choice_effect')
        read_only_fields = ('module_level','forced_choice_effect')

class DetailDivisionOverdriveSerializer(serializers.ModelSerializer):
    """Serializer to map the Model DivisionOverdrive instance into JSON format from a Division Detail view."""
    characteristic = CharacteristicSerializer(read_only=True)
    
    class Meta:
        model = DivisionOverdrive
        fields = ('characteristic',)
        read_only_fields = ('characteristic',)

class DetailDivisionWeaponSerializer(serializers.ModelSerializer):
    """Serializer to map the Model DivisionWeapon instance into JSON format from a Division Detail view."""
    weapon = WLWeaponSerializer(read_only=True)
    
    class Meta:
        model = DivisionWeapon
        fields = ('weapon',)
        read_only_fields = ('weapon',)

class DetailDivisionSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Division instance into JSON format from a Division Detail view."""
    aspect_bonus = NameRelatedSerializer(read_only=True)
    weapons = DetailDivisionWeaponSerializer(read_only=True, many=True)
    modules = DetailDivisionModuleSerializer(read_only=True, many=True)
    overdrives = DetailDivisionOverdriveSerializer(read_only=True, many=True)
    
    class Meta:
        model = Division
        fields = (
            'id',
            'name',
            'description',
            'aspect_bonus',
            'disadvantage_name',
            'disadvantage',
            'motto',
            'slug',
            'weapons',
            'overdrives',
            'modules',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'aspect_bonus',
            'disadvantage_name',
            'disadvantage',
            'motto',
            'slug',
            'weapons',
            'overdrives',
            'modules',
        )

class ListDivisionSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Division instance into JSON format from a Division List view."""
    aspect_bonus = NameRelatedSerializer(read_only=True)
    
    class Meta:
        model = Division
        fields = (
            'id',
            'name',
            'description',
            'aspect_bonus',
            'disadvantage_name',
            'disadvantage',
            'motto',
            'slug',
        )
        read_only_fields = (
            'id',
            'name',
            'description',
            'aspect_bonus',
            'disadvantage_name',
            'disadvantage',
            'motto',
            'slug',
        )