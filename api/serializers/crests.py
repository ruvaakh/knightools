from rest_framework import serializers
from gm.models.character import Crest

class DetailCrestSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Crest instance into JSON format from a Crest Detail view."""
    
    class Meta:
        model = Crest
        fields = (
            'id',
            'name',
            'vow',
            'description',
            'slug',
        )
        read_only_fields = (
            'id',
            'name',
            'vow',
            'description',
            'slug',
        )

class ListCrestSerializer(serializers.ModelSerializer):
    """Serializer to map the Model Crest instance into JSON format from a Crest List view."""
    
    class Meta:
        model = Crest
        fields = (
            'id',
            'name',
            'vow',
            'description',
            'slug',
        )
        read_only_fields = (
            'id',
            'name',
            'vow',
            'description',
            'slug',
        )