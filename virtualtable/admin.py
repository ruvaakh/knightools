from django.db import models
from django.contrib import admin
from martor.widgets import AdminMartorWidget

from .models import AvalonExtension

class AvalonExtensionAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'cost', 'get_prerequired_names')
    search_fields = ['name', 'description']
    prepopulated_fields = {"slug": ("name",)}
    
admin.site.register(AvalonExtension, AvalonExtensionAdmin)
