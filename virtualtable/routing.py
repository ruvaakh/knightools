from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
    url(r'^ws/vt/(?P<vt_id>[0-9]+)/$', consumers.VirtualTableConsumer),
]
