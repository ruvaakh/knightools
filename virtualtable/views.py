from secrets import token_urlsafe
from gettext import gettext
import json

from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import get_object_or_404, render
from django.urls import reverse, reverse_lazy
from django.views import generic
from django.utils import timezone
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Max, Prefetch, Q, Case, When, BooleanField
from django.db import transaction

from martor.utils import markdownify

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

from gm.models.character import Aspect, Characteristic

from .models import VirtualTable, Character, VirtualTablePlayer, VirtualTableAvalon, VirtualTableAvalonExtension
from .forms import VirtualTableForm, CharacterCreationForm, TestForm, PlayerJoinTableForm

# TODO : RANGER LES JS POUR EVITER DE SURCHARGER LES PAGES INUTILEMENT !
# TODO : GERER LE FORM EN CRISPY :)

class CreateTableView(LoginRequiredMixin, generic.CreateView):
    template_name = 'virtualtable/vt_create.html'
    model = VirtualTable
    fields = ('name', 'description', 'max_character', 'token_valid_duration', 'active_avalon')
    redirect_to = 'virtualtable:vt_list'
    success_url = reverse_lazy('virtualtable:vt_list')
    
    def form_valid(self, form):
        form.instance.game_master = self.request.user
        form.instance.token_creation_date = timezone.now()
        form.instance.invitation_token = token_urlsafe(6)
        
        return_value = super().form_valid(form)
        
        # Doing this after the final validation so it sync the extensions with the newly created virtual table
        if form.instance.active_avalon:
            form.instance.avalon = VirtualTableAvalon()
            form.instance.avalon.save()
            form.instance.avalon.sync_extensions()
        
        return return_value
        
class UpdateTableView(LoginRequiredMixin, generic.UpdateView):
    template_name = 'virtualtable/vt_update.html'
    model = VirtualTable
    form_class = VirtualTableForm
    # fields = ('name', 'description', 'max_character', 'token_valid_duration')
    redirect_to = 'virtualtable:vt_list'
    success_url = reverse_lazy('virtualtable:vt_list')
    
    def get_object(self):
        vt = super(UpdateTableView, self).get_object()
        
        if self.request.user != vt.game_master:
            return None
        
        vt.sisters = VirtualTable.objects.filter(game_master=self.request.user).order_by('name').all()
        
        return vt
        
    def form_valid(self, form):
        if form.instance.game_master != self.request.user:
            raise PermissionError(gettext("Cette table virtuelle ne vous appartient pas et ne peut être modifiée"), "tried_to_modify_vt_but_not_gm")
        
        if form.cleaned_data['regen']:
            form.instance.token_creation_date = timezone.now()
            form.instance.invitation_token = token_urlsafe(6)
        
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            ("virtual_table_%s" % form.instance.id),
            {
                'type': 'vt_message',
                'params': {
                    "command": 'description',
                    "description": markdownify(form.cleaned_data['description']),
                }
            }
        )
        
        
        if form.instance.active_avalon:
            try:
                avalon = form.instance.avalon
            except ObjectDoesNotExist as e:
                form.instance.avalon = VirtualTableAvalon()
                form.instance.avalon.save()
            except Exception as e:
                raise e
            
            form.instance.avalon.sync_extensions()
        
        if form.cleaned_data['vtp_to_del']:
            vtp_to_del = json.loads(form.cleaned_data['vtp_to_del'])
            
            # Stop autocommit during the deletion of vtp, to be sure we delete all of them at once.
            transaction.set_autocommit(False)
            
            for vtp_id in vtp_to_del:
                if not isinstance(vtp_id, int):
                    transaction.rollback()
                    raise ValueError(gettext("Erreur dans le passage des personnages à supprimer de cette table."), "vtp_not_valid")
                
                if VirtualTablePlayer.objects.get(pk=vtp_id).virtual_table.id != form.instance.id:
                    transaction.rollback()
                    raise Exception(gettext("Le joueur n'est pas dans la table en cours de modification"), "vtp_not_in_vt")
                
                VirtualTablePlayer.objects.get(pk=vtp_id).delete()
            
            transaction.commit()
            transaction.set_autocommit(True)
            
            async_to_sync(channel_layer.group_send)(
                ("virtual_table_%s" % form.instance.id),
                {
                    'type': 'vt_message',
                    'params': {
                        "command": 'del',
                        "vtp": vtp_to_del,
                    }
                }
            )
            
        if form.cleaned_data['vtp_to_migrate']:
            vtp_to_migrate = json.loads(form.cleaned_data['vtp_to_migrate'])
            vtp_to_move = []
            
            for vt_id in vtp_to_migrate:
                vtp_list = vtp_to_migrate[vt_id]
                
                try:
                    vt_id = int(vt_id)
                except ValueError:
                    # Don't know what the people tried, but it's trolling obviously. So we ignore it.
                    continue
                
                # Ignoring player staying in the same virtualtable.
                if vt_id == form.instance.id:
                    continue
                
                try:
                    vt = VirtualTable.objects.get(pk=vt_id)
                except ObjectDoesNotExist:
                    raise Exception(gettext("Impossible de migrer sur la table demandée, elle n'existe pas / plus."), "unknown_migration_target")
                    
                if VirtualTable.objects.get(pk=vt_id).game_master != self.request.user:
                    raise Exception(gettext("Impossible de migrer sur une table dont vous n'êtes pas le MJ."), "unauthorized_migration_target")
                
                for vtp_id in vtp_list:
                    vtp = VirtualTablePlayer.objects.get(pk=vtp_id)
                    vtp.virtual_table = vt
                    vtp.save()
                    
                    vtp_to_move.append(vtp_id)
                    
                    async_to_sync(channel_layer.group_send)(
                        ("virtual_table_%s" % vt.id),
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": 'new',
                                "vtp_id": vtp_id,
                            }
                        }
                    )
            
            async_to_sync(channel_layer.group_send)(
                ("virtual_table_%s" % form.instance.id),
                {
                    'type': 'vt_message',
                    'params': {
                        "command": 'del',
                        "vtp": vtp_to_move,
                    }
                }
            )
        return super().form_valid(form)

class ListTableView(LoginRequiredMixin, generic.ListView):
    template_name = 'virtualtable/vt_list.html'
    context_object_name = 'virtualtable_list'

    def get_queryset(self):
        """Return list of virtual table."""
        return VirtualTable.objects.filter(Q(game_master=self.request.user) | Q(players__player=self.request.user)).annotate( 
            owned_by_user=Case( 
                When(game_master=self.request.user, then=True), 
                default=False, 
                output_field=BooleanField()
            ) 
        ).distinct().order_by('name').prefetch_related('players')

class DeleteTableView(LoginRequiredMixin, generic.DeleteView):
    model = VirtualTable
    success_url = reverse_lazy('virtualtable:vt_list')
    template_name = 'virtualtable/vt_delete.html'
    
    def get_object(self):
        vt = super(DeleteTableView, self).get_object()
        
        if self.request.user != vt.game_master:
            return None
        
        return vt
    
    def delete(self, request, *args, **kwargs):
        return_val = super(DeleteTableView, self).delete(request, *args, **kwargs)
        
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            ("virtual_table_%s" % kwargs['pk']),
            {
                'type': 'vt_message',
                'params': {
                    "command": 'kick',
                }
            }
        )
        
        return return_val

class DetailTableView(LoginRequiredMixin, generic.DetailView):
    model = VirtualTable
    template_name = 'virtualtable/vt_detail.html'
    
    def get_object(self):
        vt = super(DetailTableView, self).get_object()
        
        if self.request.user != vt.game_master and len(vt.players.filter(player=self.request.user)) == 0:
            return None
        
        vt.players_list = VirtualTablePlayer.objects.filter(virtual_table=vt.id).annotate(
            editable_by_user=Case(
                When(edit_level=VirtualTablePlayer.EDIT_LVL_ALL, then=True),
                When(edit_level=VirtualTablePlayer.EDIT_LVL_PLAYER, player=self.request.user, then=True),
                When(virtual_table__game_master=self.request.user, then=True),
                default=False,
                output_field=BooleanField()
            )
        ).order_by('-editable_by_user', 'name').all()
        
        if vt.active_avalon:
            vt.avalon.ordered_extensions_by_user = vt.avalon.get_extensions_by_user_vote(self.request.user)
        return vt

class HelpTableView(LoginRequiredMixin, generic.TemplateView):
    template_name = 'virtualtable/vt_help.html'
    
    
class PlayerJoinTableView(LoginRequiredMixin, generic.CreateView):
    invitation_token = None
    form_class = PlayerJoinTableForm
    template_name = "virtualtable/vt_join.html"
    success_url = reverse_lazy('virtualtable:vt_list')
    
    def form_valid(self, form):
        vt = form.cleaned_data['token']
        # import pdb; pdb.set_trace()
        form.instance.player = self.request.user
        form.instance.virtual_table = vt
        
        if (form.cleaned_data['max_health_points'] is not None
          and (form.cleaned_data['current_health_points'] is None
          or form.cleaned_data['current_health_points'] > form.cleaned_data['max_health_points'])):
            form.instance.current_health_points = form.cleaned_data['max_health_points']
        elif (form.cleaned_data['current_health_points'] is not None
          and form.cleaned_data['max_health_points'] is None):
            form.instance.max_health_points = form.cleaned_data['current_health_points']
        
        if (form.cleaned_data['max_armour_points'] is not None
          and (form.cleaned_data['current_armour_points'] is None
          or form.cleaned_data['current_armour_points'] > form.cleaned_data['max_armour_points'])):
            form.instance.current_armour_points = form.cleaned_data['max_armour_points']
        elif (form.cleaned_data['current_armour_points'] is not None
          and form.cleaned_data['max_armour_points'] is None):
            form.instance.max_armour_points = form.cleaned_data['current_armour_points']
        
        if (form.cleaned_data['max_energy_points'] is not None
          and (form.cleaned_data['current_energy_points'] is None
          or form.cleaned_data['current_energy_points'] > form.cleaned_data['max_energy_points'])):
            form.instance.current_energy_points = form.cleaned_data['max_energy_points']
        elif (form.cleaned_data['current_energy_points'] is not None
          and form.cleaned_data['max_energy_points'] is None):
            form.instance.max_energy_points = form.cleaned_data['current_energy_points']
        
        if (form.cleaned_data['max_hope_points'] is not None
          and (form.cleaned_data['current_hope_points'] is None
          or form.cleaned_data['current_hope_points'] > form.cleaned_data['max_hope_points'])):
            form.instance.current_hope_points = form.cleaned_data['max_hope_points']
        elif (form.cleaned_data['current_hope_points'] is not None
          and form.cleaned_data['max_hope_points'] is None):
            form.instance.max_hope_points = form.cleaned_data['current_hope_points']
        
        return_val = super().form_valid(form)
        
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            ("virtual_table_%s" % vt.id),
            {
                'type': 'vt_message',
                'params': {
                    "command": 'new',
                    "vtp_id": form.instance.id,
                }
            }
        )
        
        # form.instance.name = form.cleaned_data['name']
        return return_val
    
    def get_initial(self):
        return {
            'token': self.invitation_token
        }
    
    def get(self, request, *args, **kwargs):
        self.invitation_token = self.kwargs.get('invitation_token', None)
        
        try:
            vt = VirtualTable.objects.get(invitation_token=self.invitation_token)
        except ObjectDoesNotExist:
            vt = None
        finally:
            
            if vt is None or vt.token_has_expired():
                self.template_name = 'virtualtable/vt_invitation_expired.html'
            elif vt.max_character <= len(VirtualTablePlayer.objects.filter(virtual_table=vt)):
                self.template_name = 'virtualtable/vt_max_reached.html'
                
            return render(request, self.template_name, {
                'token': self.invitation_token,
                'form': self.get_form(),
                'vt': vt,
            })
        

class JoinTableView(LoginRequiredMixin, generic.FormView):
    invitation_token = None
    form_class = CharacterCreationForm
    template_name = "virtualtable/character_creation.html"
    
    # We set the initial form to have the invitation token (if it exists)
    def get_initial(self):
        return {
            'token': self.invitation_token
        }
    
    def get(self, request, *args, **kwargs):
        
        # template_name = 'virtualtable/vt_join.html'
        characters = Character.objects.filter(player=self.request.user)
        
        if len(characters) == 0:
            self.template_name = 'virtualtable/character_creation.html'
        
        self.success_url = 'virtualtable:vt_join'
        self.invitation_token = self.kwargs.get('invitation_token', None)
        # print(self.invitation_token)
        # import ipdb; ipdb.set_trace()
        
        # TODO: check if user has already characters.
        # If not, redirect to character creation.
        # If one, render the join.
        # If multiple, display a choose the character.
        return render(request, self.template_name, {
            'token': self.invitation_token,
            'form': self.get_form(),
            'aspects': Aspect.objects.prefetch_related(Prefetch('characteristics', queryset=Characteristic.objects.annotate(max_level_overdrive=Max('overdrives__level'))))
        })
        # return super().get(request)



class TestView(LoginRequiredMixin, generic.FormView):
    form_class = TestForm
    template_name = "virtualtable/test.html"
