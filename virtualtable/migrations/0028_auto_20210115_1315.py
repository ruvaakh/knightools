# Generated by Django 2.2.2 on 2021-01-15 13:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('virtualtable', '0027_auto_20210106_1340'),
    ]

    operations = [
        migrations.AlterField(
            model_name='virtualtableplayer',
            name='classes_to_show',
            field=models.TextField(blank=True, default='.tab-status,.bar-health-points,.bar-armour-points,.bar-energy-points,.bar-hope-points,.counter-force-field,.tab-stock,.bar-health-nods,.bar-armour-nods,.bar-energy-nods,.bar-grenades-stock,.tab-legend,.bar-hero-points,.bar-glory-points,.bar-experience-points', verbose_name='classes CSS à afficher'),
        ),
        migrations.AlterField(
            model_name='virtualtableplayer',
            name='current_armour_nod',
            field=models.PositiveSmallIntegerField(blank=True, default=3, verbose_name="nods d'armure actuels"),
        ),
        migrations.AlterField(
            model_name='virtualtableplayer',
            name='current_energy_nod',
            field=models.PositiveSmallIntegerField(blank=True, default=3, verbose_name="nods d'énergie actuels"),
        ),
        migrations.AlterField(
            model_name='virtualtableplayer',
            name='current_grenade_stock',
            field=models.PositiveSmallIntegerField(blank=True, default=5, verbose_name='grenades actuelles'),
        ),
        migrations.AlterField(
            model_name='virtualtableplayer',
            name='current_health_nod',
            field=models.PositiveSmallIntegerField(blank=True, default=3, verbose_name='nods de soin actuels'),
        ),
        migrations.AlterField(
            model_name='virtualtableplayer',
            name='max_armour_nod',
            field=models.PositiveSmallIntegerField(blank=True, default=3, verbose_name="nods d'armure max"),
        ),
        migrations.AlterField(
            model_name='virtualtableplayer',
            name='max_energy_nod',
            field=models.PositiveSmallIntegerField(blank=True, default=3, verbose_name="nods d'énergie max"),
        ),
        migrations.AlterField(
            model_name='virtualtableplayer',
            name='max_grenade_stock',
            field=models.PositiveSmallIntegerField(blank=True, default=5, verbose_name='grenades max'),
        ),
        migrations.AlterField(
            model_name='virtualtableplayer',
            name='max_health_nod',
            field=models.PositiveSmallIntegerField(blank=True, default=3, verbose_name='nods de soin max'),
        ),
    ]
