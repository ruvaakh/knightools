# Generated by Django 2.2.2 on 2020-03-02 09:28

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('virtualtable', '0019_auto_20200228_1645'),
    ]

    operations = [
        migrations.RenameField(
            model_name='character',
            old_name='armor',
            new_name='armour',
        ),
        migrations.RenameField(
            model_name='character',
            old_name='default_armor_nod',
            new_name='default_armour_nod',
        ),
        migrations.RenameField(
            model_name='virtualtablecharacter',
            old_name='current_armor_nod',
            new_name='current_armour_nod',
        ),
        migrations.RenameField(
            model_name='virtualtableplayer',
            old_name='current_armor_nod',
            new_name='current_armour_nod',
        ),
        migrations.RenameField(
            model_name='virtualtableplayer',
            old_name='ignore_health_loss_by_armor',
            new_name='ignore_health_loss_by_armour',
        ),
        migrations.RenameField(
            model_name='virtualtableplayer',
            old_name='max_armor_nod',
            new_name='max_armour_nod',
        ),
    ]
