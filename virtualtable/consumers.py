import json
import subprocess
import logging
import uuid
from django.core.exceptions import ObjectDoesNotExist

from django.forms.models import model_to_dict
from django.db.models import F, Case, When, BooleanField
from django.template import loader

from django.urls import reverse
from django.utils import timezone

from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer


from .models import AvalonExtension, VirtualTableAvalon, VirtualTableAvalonExtensionVote, VirtualTablePlayer, VirtualTableRoll, VirtualTable
from gm.models.character import MajorArcana

from markdown import markdown

logger = logging.getLogger(__name__)

class VirtualTableConsumer(WebsocketConsumer):
    def connect(self):
        self.vt_id = int(self.scope['url_route']['kwargs']['vt_id'])
        self.vt_group_name = 'virtual_table_%s' % self.vt_id
        
        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.vt_group_name,
            self.channel_name
        )
        # import pdb; pdb.set_trace()
        
        async_to_sync(self.channel_layer.group_send)(
            self.vt_group_name,
            {
                'type': 'vt_message',
                'params': {
                    "command": 'join',
                    "vtp": list(VirtualTablePlayer.objects.filter(player=self.scope['user'].id).values_list('id', flat=True)),
                    "name": self.scope['user'].first_name,
                }
            }
        )
        
        self.accept()

    def disconnect(self, close_code):
        # import pdb; pdb.set_trace()
        async_to_sync(self.channel_layer.group_send)(
            self.vt_group_name,
            {
                'type': 'vt_message',
                'params': {
                    "command": 'quit',
                    "vtp": list(VirtualTablePlayer.objects.filter(player=self.scope['user'].id).values_list('id', flat=True)),
                }
            }
        )
        async_to_sync(self.channel_layer.group_discard)(
            self.vt_group_name,
            self.channel_name
        )

    def receive(self, text_data):
        """
        When receiving a message, the websocket will do what is asked in the message.command
        and return the newly data, refreshed from database.
        
        text_data: valid json object with at least attributes:
            - command: update|status|hurt|toggle|combo|damage|destiny|config|avalon_vote|avalon_extension_toggle|avalon_reset_votes
            - vtp: id of the virtualtableplayer asking that command
            
            Additional attributes for update command:
                subcommand: edit|add|sub|reset
                attr: attribute code to update
                max: max value for this attribute (can be None for some subcommand)
                current: current value for this attribute (can be None for some subcommand)
                
            Additional attributes for status command:
                counter: counter of status command asked for this user. Used to avoid conflict with async tech.
                
            Additional attributes for hurt command:
                armour-piercing: Integer          Under a certain level of armour points, it ignores them. Default 0
                ignore-ff:    Boolean          Ignore force field. Default False if None.
                ignore-armour: Boolean          Ignore armour. If armour-piercing and ignore-armour are given, ignore-armour will be prioritary. Default False if None
                target:       Char             Target directly Hope Points, not health or armour. List between "hp" (health points),
                                                 "hop" (for hope points), "ap" (armour points) or "ep" (energy points). Default "hp"
                                                 If "ap", can't be combined with ignore-armour or armour-piercing (target will be prioritary).
                value:        Integer          Damage value to be calculated. Required
                vtp:          Array(Integer)   VirtualTablePlayer ids taking damages. They must belong to the VirtualTable.
            
            Additional attributes for toggle command:
                attr:  Char      Either rights or ignore_health_loss. Rights need a second attributes "value". Required
                value: Char      The new value (must be a valid value for VirtualTablePlayer.edit_level_choices)
                vtp:   Integer   VirtualTablePlayer id to change. It must belong to the VirtualTable.
            
            Additional attributes for combo command:
                dice_number:     Integer   Number of dice rolling for the combo test. Required
                overdrive_value: Integer   Overdrive and other automatic success for this roll. They will automatically be added if the roll is not a critical fail. Can be empty or 0.
                vtp:             Integer   VirtualTablePlayer id doing the combo roll.
            
            Additional attributes for damage command:
                dice_number: Integer   The number of dice launched for the damage roll. Can be 0 if the damage are only fixed without roll (could happen). If dice number equals 0 and bonus_value too, nothing is done.
                bonus_value: Integer   The bonus to add to the attack. They will automatically be added to the result, even if no dice are rolled. If dice number equals 0 and bonus_value too, nothing is done.
                vtp:         Integer   VirtualTablePlayer id doing the damage roll.
            
            Additional attributes for config command:
                classes_to_show: Char    Lists of css classes, separated by commas, needing to be shown on the interface.
                vtp:             Integer VirtualTablePlayer id having its configuration changed
            
            Additional attributes for destiny command:
                vtp:         Integer   VirtualTablePlayer id doing the destiny card draw.
            
            Additional attributes for avalon_vote command:
                new_ordered_extensions:   Array(Integer)  New avalon extensions ordered list id for this user
                
            Additional attributes for avalon_extension_toggle command:
                extension_id:   Integer  Avalon extension id to toggle (build or destroy) for this table
        """
        try:
            text_data_json = json.loads(text_data)
            command = text_data_json['command']
            command_id = uuid.uuid4()
            
            def restore_vtp_counters(vtp):
                
                dice_energy = vtp.lost_energy_counter // 6
                dice_hope = vtp.lost_hope_counter // 6
                logger.info('[%s] [RESTORE] VTP: %d restore %d d6 energy and %d d6 hope  ' % (command_id, vtp.id, dice_energy, dice_hope))
                
                if dice_energy > 0:
                    response = {
                        'damage': 0,
                        'dice': dice_energy,
                        'bonus': 0,
                        'detail': {
                            1: {
                                'class': 'dice-one',
                                'count': 0
                            },
                            2: {
                                'class': 'dice-two',
                                'count': 0
                            },
                            3: {
                                'class': 'dice-three',
                                'count': 0
                            },
                            4: {
                                'class': 'dice-four',
                                'count': 0
                            },
                            5: {
                                'class': 'dice-five',
                                'count': 0
                            },
                            6: {
                                'class': 'dice-six',
                                'count': 0
                            },
                        },
                    }
                    
                    try:
                        byte_output = subprocess.check_output(['dice', '-j', '%(dice)dd6' % {'dice': dice_energy}])
                    except subprocess.CalledProcessError as e:
                        logger.critical('[%s] [RESTORE] exec command failed, maybe binary is not installed? %s' % (command_id, str(e)))
                        raise e
                    except Exception as e:
                        logger.critical('[%s] [RESTORE] unknown exception on exec command %s' % (command_id, str(e)))
                        raise e
                    
                    result_object = json.loads(byte_output.decode())
                    
                    response['damage'] = int(result_object['scalar'])
                    
                    logger.debug('[%s] [RESTORE] restored: %d' % (command_id, response['damage']))
                    
                    logger.debug('[%s] [RESTORE] current_energy_points: %d' % (command_id, vtp.current_energy_points))
                    vtp.current_energy_points = vtp.current_energy_points + response['damage']
                    if vtp.current_energy_points < 0:
                        current_value = 0
                    if vtp.current_energy_points > vtp.max_energy_points:
                        vtp.current_energy_points = vtp.max_energy_points
                    
                    logger.debug('[%s] [RESTORE] new current_energy_points: %d' % (command_id, vtp.current_energy_points))
                    
                    for dice in result_object['values'][0]['values']:
                        response['detail'][dice['total']]['count'] = response['detail'][dice['total']]['count'] + 1
                    
                    logger.info('[%s] [RESTORE] result (energy): %s' % (command_id, json.dumps(response)))
                    
                    roll = VirtualTableRoll()
                    roll.datetime = timezone.now()
                    roll.response_json = json.dumps(response)
                    roll.combo = False
                    roll.public = True
                    roll.virtual_table_player = vtp
                    
                    roll.save()
                    VirtualTableRoll.objects.filter(datetime__lte=timezone.now()-timezone.timedelta(hours=12)).delete()
                    
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "damage_result",
                                "public": True,
                                "vtp": model_to_dict(vtp),
                                "response": response,
                            }
                        }
                    )
                
                vtp.lost_energy_counter = 0
                
                if dice_hope > 0:
                    response = {
                        'damage': 0,
                        'dice': dice_hope,
                        'bonus': 0,
                        'detail': {
                            1: {
                                'class': 'dice-one',
                                'count': 0
                            },
                            2: {
                                'class': 'dice-two',
                                'count': 0
                            },
                            3: {
                                'class': 'dice-three',
                                'count': 0
                            },
                            4: {
                                'class': 'dice-four',
                                'count': 0
                            },
                            5: {
                                'class': 'dice-five',
                                'count': 0
                            },
                            6: {
                                'class': 'dice-six',
                                'count': 0
                            },
                        },
                    }
                    
                    try:
                        byte_output = subprocess.check_output(['dice', '-j', '%(dice)dd6' % {'dice': dice_hope}])
                    except subprocess.CalledProcessError as e:
                        logger.critical('[%s] [RESTORE] exec command failed, maybe binary is not installed? %s' % (command_id, str(e)))
                        raise e
                    except Exception as e:
                        logger.critical('[%s] [RESTORE] unknown exception on exec command %s' % (command_id, str(e)))
                        raise e
                    
                    result_object = json.loads(byte_output.decode())
                    
                    response['damage'] = int(result_object['scalar'])
                    
                    logger.debug('[%s] [RESTORE] restored: %d' % (command_id, response['damage']))
                    
                    logger.debug('[%s] [RESTORE] current_hope_points: %d' % (command_id, vtp.current_hope_points))
                    vtp.current_hope_points = vtp.current_hope_points + response['damage']
                    if vtp.current_hope_points < 0:
                        current_value = 0
                    if vtp.current_hope_points > vtp.max_hope_points:
                        vtp.current_hope_points = vtp.max_hope_points
                    
                    logger.debug('[%s] [RESTORE] new current_hope_points: %d' % (command_id, vtp.current_hope_points))
                    
                    for dice in result_object['values'][0]['values']:
                        response['detail'][dice['total']]['count'] = response['detail'][dice['total']]['count'] + 1
                    
                    logger.info('[%s] [RESTORE] result (hope): %s' % (command_id, json.dumps(response)))
                    
                    roll = VirtualTableRoll()
                    roll.datetime = timezone.now()
                    roll.response_json = json.dumps(response)
                    roll.combo = False
                    roll.public = True
                    roll.virtual_table_player = vtp
                    
                    roll.save()
                    VirtualTableRoll.objects.filter(datetime__lte=timezone.now()-timezone.timedelta(hours=12)).delete()
                    
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "damage_result",
                                "public": True,
                                "vtp": model_to_dict(vtp),
                                "response": response,
                            }
                        }
                    )
                
                vtp.lost_hope_counter = 0
                
                vtp.save()
            
            if command == "update":
                subcommand = text_data_json['subcommand']
                attr_code = text_data_json['attr']
                vtp_id = text_data_json['vtp']
                
                if vtp_id is None:
                    raise Exception('Invalid parameters', 'invalid')
                
                vtp_id = int(vtp_id)
                
                vtp = VirtualTablePlayer.objects.get(pk=vtp_id)
                
                if vtp.virtual_table.id != self.vt_id:
                    # import pdb; pdb.set_trace()
                    raise Exception('Incoherent parameters', 'incoherent')
                
                if ((vtp.edit_level == vtp.EDIT_LVL_PLAYER
                  and vtp.player.id != self.scope['user'].id
                  and vtp.virtual_table.game_master.id != self.scope['user'].id)
                  or (vtp.edit_level == vtp.EDIT_LVL_GM
                  and vtp.virtual_table.game_master.id != self.scope['user'].id)):
                    raise Exception('Unauthorized modification for this user and this virtual table player', 'unauthorized_user_vtp')
                
                if subcommand == 'edit':
                    # import pdb; pdb.set_trace()
                    
                    try:
                        current_value = text_data_json['current']
                    except KeyError:
                        current_value = None
                    except Exception as e:
                        raise e
                        
                    try:
                        current_value_add = text_data_json['current_add']
                    except KeyError:
                        current_value_add = None
                    except Exception as e:
                        raise e
                    
                    try:
                        max_value = text_data_json['max']
                    except KeyError:
                        max_value = None
                    except Exception as e:
                        raise e
                    
                    try:
                        recoverable_value = text_data_json['recoverable']
                    except KeyError:
                        recoverable_value = None
                    except Exception as e:
                        raise e
                    
                    if max_value is not None:
                        max_value = int(max_value)
                    
                    if current_value is not None:
                        current_value = int(current_value)
                        
                        if current_value_add is not None:
                            current_value_add = int(current_value_add)
                            current_value = current_value + current_value_add
                            if current_value < 0:
                                current_value = 0
                    
                    if recoverable_value is not None:
                        recoverable_value = int(recoverable_value)
                    else:
                        recoverable_value = 0
                    
                    if attr_code == 'name':
                        try:
                            name = text_data_json['name']
                        except Exception as e:
                            raise e
                        
                        if name.strip() ==  '':
                            raise Exception('Empty name is not allowed', 'empty_name')
                        
                        vtp.name = name
                        vtp.save()
                        
                    elif attr_code == 'hp':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_health_points = current_value
                        vtp.max_health_points = max_value
                        vtp.save()
                        
                    elif attr_code == 'hn':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_health_nod = current_value
                        vtp.max_health_nod = max_value
                        vtp.save()
                        
                    elif attr_code == 'ap':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_armour_points = current_value
                        vtp.max_armour_points = max_value
                        vtp.save()
                        
                    elif attr_code == 'an':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_armour_nod = current_value
                        vtp.max_armour_nod = max_value
                        vtp.save()
                        
                    elif attr_code == 'ep':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_energy_points = current_value
                        vtp.max_energy_points = max_value
                        vtp.lost_energy_counter = recoverable_value
                        vtp.save()
                    
                    elif attr_code == 'en':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_energy_nod = current_value
                        vtp.max_energy_nod = max_value
                        vtp.save()
                    
                    elif attr_code == 'hop':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_hope_points = current_value
                        vtp.max_hope_points = max_value
                        vtp.lost_hope_counter = recoverable_value
                        vtp.save()
                    
                    elif attr_code == 'gs':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_grenade_stock = current_value
                        vtp.max_grenade_stock = max_value
                        vtp.save()
                    
                    elif attr_code == 'ff':
                        vtp.current_force_field = current_value
                        vtp.default_force_field = max_value
                        vtp.save()
                    
                    elif attr_code == 'ae':
                        vtp.current_aegis = current_value
                        vtp.default_aegis = max_value
                        vtp.save()
                    
                    elif attr_code == 'hep':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_heroism_points = current_value
                        vtp.max_heroism_points = max_value
                        vtp.save()
                    
                    elif attr_code == 'flux':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_flux_points = current_value
                        vtp.max_flux_points = max_value
                        vtp.save()
                    
                    elif attr_code == 'imp':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_impregnation_points = current_value
                        vtp.max_impregnation_points = max_value
                        vtp.save()
                    
                    elif attr_code == 'gp':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_gp = current_value
                        vtp.total_gp = max_value
                        vtp.save()
                    
                    elif attr_code == 'xp':
                        if current_value is None or current_value > max_value:
                            current_value = max_value
                        
                        vtp.current_xp = current_value
                        vtp.total_xp = max_value
                        vtp.save()
                    
                    elif attr_code == 'pard':
                        vtp.time_paradox_difficulty = current_value
                        vtp.save()
                    
                    elif attr_code == 'impd':
                        vtp.impregnation_difficulty = current_value
                        vtp.save()
                    
                    # import pdb; pdb.set_trace()
                else:
                    if subcommand == 'reset':
                        if attr_code == 'ff':
                            vtp.current_force_field = vtp.default_force_field
                            vtp.save()
                        elif attr_code == 'ae':
                            vtp.current_aegis = vtp.default_aegis
                            vtp.save()
                        else:
                            raise Exception('Not known subcommand for this attribute', 'unknown_attr_subcommand')
                    else:
                        if subcommand == 'add':
                            step = 1
                        elif subcommand == 'sub':
                            step = -1
                        else:
                            raise Exception('Not known subcommand for this attribute', 'unknown_attr_subcommand')
                        
                        # import pdb; pdb.set_trace()
                        
                        if attr_code == 'hp':
                            vtp.current_health_points = F('current_health_points') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_health_points > vtp.max_health_points:
                                vtp.current_health_points = vtp.max_health_points
                                vtp.save()
                            
                        elif attr_code == 'hn':
                            vtp.current_health_nod = F('current_health_nod') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_health_nod > vtp.max_health_nod:
                                vtp.current_health_nod = vtp.max_health_nod
                                vtp.save()
                            
                        elif attr_code == 'ap':
                            vtp.current_armour_points = F('current_armour_points') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_armour_points > vtp.max_armour_points:
                                vtp.current_armour_points = vtp.max_armour_points
                                vtp.save()
                            
                        elif attr_code == 'an':
                            vtp.current_armour_nod = F('current_armour_nod') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_armour_nod > vtp.max_armour_nod:
                                vtp.current_armour_nod = vtp.max_armour_nod
                                vtp.save()
                            
                        elif attr_code == 'ep':
                            vtp.current_energy_points = F('current_energy_points') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_energy_points > vtp.max_energy_points:
                                vtp.current_energy_points = vtp.max_energy_points
                                vtp.save()
                            
                        elif attr_code == 'en':
                            vtp.current_energy_nod = F('current_energy_nod') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_energy_nod > vtp.max_energy_nod:
                                vtp.current_energy_nod = vtp.max_energy_nod
                                vtp.save()
                            
                        elif attr_code == 'hop':
                            vtp.current_hope_points = F('current_hope_points') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_hope_points > vtp.max_hope_points:
                                vtp.current_hope_points = vtp.max_hope_points
                                vtp.save()
                            
                        elif attr_code == 'gs':
                            vtp.current_grenade_stock = F('current_grenade_stock') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_grenade_stock > vtp.max_grenade_stock:
                                vtp.current_grenade_stock = vtp.max_grenade_stock
                                vtp.save()
                            
                        elif attr_code == 'ff':
                            vtp.current_force_field = F('current_force_field') + step
                            vtp.save()
                            
                        elif attr_code == 'ae':
                            vtp.current_aegis = F('current_aegis') + step
                            vtp.save()
                            
                        elif attr_code == 'hep':
                            vtp.current_heroism_points = F('current_heroism_points') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_heroism_points > vtp.max_heroism_points:
                                vtp.current_heroism_points = vtp.max_heroism_points
                                vtp.save()
                            
                        elif attr_code == 'flux':
                            vtp.current_flux_points = F('current_flux_points') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_flux_points > vtp.max_flux_points:
                                vtp.current_flux_points = vtp.max_flux_points
                                vtp.save()
                            
                        elif attr_code == 'imp':
                            vtp.current_impregnation_points = F('current_impregnation_points') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_impregnation_points > vtp.max_impregnation_points:
                                vtp.current_impregnation_points = vtp.max_impregnation_points
                                vtp.save()
                            
                        elif attr_code == 'gp':
                            vtp.current_gp = F('current_gp') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_gp > vtp.total_gp:
                                vtp.current_gp = vtp.total_gp
                                vtp.save()
                            
                        elif attr_code == 'xp':
                            vtp.current_xp = F('current_xp') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.current_xp > vtp.total_xp:
                                vtp.current_xp = vtp.total_xp
                                vtp.save()
                            
                        elif attr_code == 'pard':
                            if vtp.time_paradox_difficulty is None:
                                vtp.time_paradox_difficulty = 1
                            else:
                                vtp.time_paradox_difficulty = F('time_paradox_difficulty') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.time_paradox_difficulty < 0:
                                vtp.time_paradox_difficulty = 0
                                vtp.save()
                            
                        elif attr_code == 'impd':
                            if vtp.impregnation_difficulty is None:
                                vtp.impregnation_difficulty = 1
                            else:
                                vtp.impregnation_difficulty = F('impregnation_difficulty') + step
                            vtp.save()
                            vtp.refresh_from_db()
                            if vtp.impregnation_difficulty < 0:
                                vtp.impregnation_difficulty = 0
                                vtp.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "refresh",
                            "vtp_to_load": vtp.id
                        }
                    }
                )
            elif command == "status":
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_status',
                        'params': {
                            'user': self.scope['user'].id,
                            'counter': text_data_json['counter'],
                        }
                    }
                )
            elif command == "hurt":
                # import pdb; pdb.set_trace()
                
                logger.info('[%s] [HURT] JSON: %s ' % (command_id, text_data))
                
                vtp_ids = text_data_json['vtps']
                
                if len(vtp_ids) == 0:
                    logger.error('[%s] [HURT] no vtp found' % (command_id,))
                    raise Exception('Damage without targeted vtp are not possible', 'damage_no_vtp')
                
                vtps = VirtualTablePlayer.objects.filter(pk__in=vtp_ids, virtual_table=self.vt_id, virtual_table__game_master__id=self.scope['user'].id)
                
                if len(vtp_ids) != len(vtps):
                    logger.error('[%s] [HURT] GM %d tried to damage unknown players %s ' % (command_id, self.scope['user'].id, vtp_ids))
                    raise Exception("Damage can be sent only to vtp under this virtual table and by the game master of this table", "damage_vtp_unknown_to_vt")
                
                try:
                    ignore_ff = text_data_json['ignore-ff']
                except KeyError:
                    ignore_ff = False
                except Exception as e:
                    logger.critical('[%s] [HURT] unknown exception on ignore ff %s' % (command_id, str(e)))
                    raise e
                
                try:
                    ignore_armour = text_data_json['ignore-armour']
                except KeyError:
                    ignore_armour = False
                except Exception as e:
                    logger.critical('[%s] [HURT] unknown exception on ignore armour %s' % (command_id, str(e)))
                    raise e
                
                try:
                    aegis_active = text_data_json['aegis-active']
                except KeyError:
                    aegis_active = False
                except Exception as e:
                    logger.critical('[%s] [HURT] unknown exception on aegis active %s' % (command_id, str(e)))
                    raise e
                
                try:
                    armour_piercing = text_data_json['armour-piercing']
                except KeyError:
                    armour_piercing = 0
                except Exception as e:
                    logger.critical('[%s] [HURT] unknown exception on armour piercing %s' % (command_id, str(e)))
                    raise e
                
                try:
                    penetrating = text_data_json['penetrating']
                except KeyError:
                    penetrating = 0
                except Exception as e:
                    logger.critical('[%s] [HURT] unknown exception on penetrating %s' % (command_id, str(e)))
                    raise e
                
                try:
                    target = text_data_json['target']
                except KeyError:
                    target = 'hp'
                except Exception as e:
                    logger.critical('[%s] [HURT] unknown exception on target %s' % (command_id, str(e)))
                    raise e
                
                try:
                    recoverable = text_data_json['recoverable']
                except KeyError:
                    recoverable = False
                except Exception as e:
                    logger.critical('[%s] [HURT] unknown exception on recoverable %s' % (command_id, str(e)))
                    raise e
                
                try:
                    value = text_data_json['value']
                except KeyError:
                    raise Exception("No damage output given", "no_damage")
                except Exception as e:
                    logger.critical('[%s] [HURT] unknown exception on value %s' % (command_id, str(e)))
                    raise e
                
                if not isinstance(value, int):
                    logger.error('[%s] [HURT] value "%s" is not an integer ' % (command_id, value))
                    raise Exception("The damage value must be an integer", "damage_not_int")
                
                if not isinstance(armour_piercing, int):
                    armour_piercing = 0
                    
                if not isinstance(penetrating, int):
                    penetrating = 0
                
                for vtp in vtps:
                    damage = value
                    logger.debug('[%s] [HURT] [VTP: %d] object: %s ' % (command_id, vtp.id, model_to_dict(vtp)))
                    logger.debug('[%s] [HURT] [VTP: %d] initial damage: %d ' % (command_id, vtp.id, damage))
                    
                    logger.debug('[%s] [HURT] [VTP: %d] ignore force field? %s ' % (command_id, vtp.id, ignore_ff))
                    if not ignore_ff:
                        force_field = vtp.current_force_field
                        
                        if force_field is None:
                            force_field = vtp.default_force_field
                        
                        if force_field is None:
                            force_field = 0
                        
                        logger.debug('[%s] [HURT] [VTP: %d] final force field: %s ' % (command_id, vtp.id, force_field))
                        # if force field is lesser or equal to the penetrating score of the damage, it will be purely ignored.
                        if penetrating > 0 and force_field <= penetrating:
                            penetrating = force_field
                        
                        logger.debug('[%s] [HURT] [VTP: %d] max(damage[=%d] - (force_field[=%d] - penetrating[=%d]), 0) ' % (command_id, vtp.id, damage, force_field, penetrating))
                        damage = max(damage - (force_field - penetrating), 0)
                        logger.debug('[%s] [HURT] [VTP: %d] new damage value: %d ' % (command_id, vtp.id, damage))
                    
                    logger.debug('[%s] [HURT] [VTP: %d] aegis active? %s ' % (command_id, vtp.id, aegis_active))
                    if aegis_active:
                        aegis = vtp.current_aegis
                        
                        if aegis is None:
                            aegis = vtp.default_aegis
                        
                        if aegis is None:
                            aegis = 0
                        
                        logger.debug('[%s] [HURT] [VTP: %d] final aegis: %s ' % (command_id, vtp.id, aegis))
                        logger.debug('[%s] [HURT] [VTP: %d] max(damage[=%d] - aegis[=%d], 0) ' % (command_id, vtp.id, damage, aegis))
                        damage = max(damage - aegis, 0)
                        logger.debug('[%s] [HURT] [VTP: %d] new damage value: %d ' % (command_id, vtp.id, damage))
                    
                    if target == 'ap':
                        current_armour = vtp.current_armour_points
                        logger.debug('[%s] [HURT] [VTP: %d] max(current_armour_points[=%d] - damage[=%d], 0) ' % (command_id, vtp.id, vtp.current_armour_points, damage))
                        vtp.current_armour_points = max(vtp.current_armour_points - damage, 0) # if armour is fully destroyed, it must be set to 0+
                        logger.debug('[%s] [HURT] [VTP: %d] new armour value: ' % (command_id, vtp.id, vtp.current_armour_points))
                        
                        logger.debug('[%s] [HURT] [VTP: %d] ignore_health_loss_by_armour? %s ' % (command_id, vtp.id, vtp.ignore_health_loss_by_armour))
                        if not vtp.ignore_health_loss_by_armour:
                            logger.debug('[%s] [HURT] [VTP: %d] max(current_health_points[=%d] - ((current_armour[=%d] - current_armour_points[=%d]) // 5), 0) ' % (command_id, vtp.id, vtp.current_health_points, current_armour, vtp.current_armour_points))
                            vtp.current_health_points = max(vtp.current_health_points - ((current_armour - vtp.current_armour_points) // 5), 0)
                            logger.debug('[%s] [HURT] [VTP: %d] new health value: %d ' % (command_id, vtp.id, vtp.current_health_points))
                    else:
                        logger.debug('[%s] [HURT] [VTP: %d] ignore armour? %s ' % (command_id, vtp.id, ignore_armour))
                        if not ignore_armour:
                            # Armour take damage before the target
                            armour = vtp.current_armour_points
                            logger.debug('[%s] [HURT] [VTP: %d] initial armour: %d ' % (command_id, vtp.id, armour))
                            logger.debug('[%s] [HURT] [VTP: %d] armour_piercing: %d ' % (command_id, vtp.id, armour_piercing))
                            if armour_piercing > 0 and armour - damage < armour_piercing:
                                logger.debug('[%s] [HURT] [VTP: %d] effective pierce armour!' % (command_id, vtp.id))
                                # the pierce armour will be effective
                                armour_damage = 0
                                if armour > armour_piercing:
                                    logger.debug('[%s] [HURT] [VTP: %d] armour damaged despite pierce armour!' % (command_id, vtp.id))
                                    armour_damage = armour - armour_piercing
                                final_damage = damage - armour_damage
                            else:
                                if armour - damage < 0:
                                    armour_damage = armour
                                    final_damage = damage - armour_damage
                                else:
                                    armour_damage = damage
                                    final_damage = 0
                        else:
                            # Damage are directly done to the target
                            armour_damage = 0
                            final_damage = damage
                        
                        logger.debug('[%s] [HURT] [VTP: %d] armour_damage: %d - final_damage: %d' % (command_id, vtp.id, armour_damage, final_damage))
                        logger.debug('[%s] [HURT] [VTP: %d] ignore_health_loss_by_armour? %s ' % (command_id, vtp.id, vtp.ignore_health_loss_by_armour))
                        if not vtp.ignore_health_loss_by_armour:
                            logger.debug('[%s] [HURT] [VTP: %d] max(current_health_points[=%d] - ((armour_damage[=%d]) // 5), 0) ' % (command_id, vtp.id, vtp.current_health_points, armour_damage))
                            vtp.current_health_points = max(vtp.current_health_points - (armour_damage // 5), 0)
                            logger.debug('[%s] [HURT] [VTP: %d] new health value: %d ' % (command_id, vtp.id, vtp.current_health_points))
                        
                        if target == 'hp':
                            vtp.current_armour_points -= armour_damage
                            logger.debug('[%s] [HURT] [VTP: %d] max(current_health_points[=%d] - ((armour_damage[=%d]) // 5), 0) ' % (command_id, vtp.id, vtp.current_health_points, armour_damage))
                            vtp.current_health_points = max(vtp.current_health_points - final_damage, 0)
                            logger.debug('[%s] [HURT] [VTP: %d] new health value: %d ' % (command_id, vtp.id, vtp.current_health_points))
                        elif target == 'ep':
                            vtp.current_armour_points -= armour_damage
                            
                            logger.debug('[%s] [HURT] [VTP: %d] target energy recoverable? %s ' % (command_id, vtp.id, recoverable))
                            if recoverable:
                                if vtp.current_energy_points > final_damage:
                                    vtp.lost_energy_counter += final_damage
                                else:
                                    vtp.lost_energy_counter += vtp.current_energy_points
                                logger.debug('[%s] [HURT] [VTP: %d] new energy lost counter: %d ' % (command_id, vtp.id, vtp.lost_energy_counter))
                            
                            vtp.current_energy_points = max(vtp.current_energy_points - final_damage, 0)
                            logger.debug('[%s] [HURT] [VTP: %d] new energy value: %d ' % (command_id, vtp.id, vtp.current_energy_points))
                        elif target == 'hop':
                            vtp.current_armour_points -= armour_damage
                            
                            logger.debug('[%s] [HURT] [VTP: %d] target hope recoverable? %s ' % (command_id, vtp.id, recoverable))
                            if recoverable:
                                if vtp.current_hope_points > final_damage:
                                    vtp.lost_hope_counter += final_damage
                                else:
                                    vtp.lost_hope_counter += vtp.current_hope_points
                                logger.debug('[%s] [HURT] [VTP: %d] new hope lost counter: %d ' % (command_id, vtp.id, vtp.lost_hope_counter))
                            
                            vtp.current_hope_points = max(vtp.current_hope_points - final_damage, 0)
                            logger.debug('[%s] [HURT] [VTP: %d] new hope value: %d ' % (command_id, vtp.id, vtp.current_hope_points))
                        else:
                            raise Exception('Unknown target for damage', 'damage_unknown_target')
                    
                    vtp.save()
                    
                    # Send message to room group
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "refresh",
                                "vtp_to_load": vtp.id
                            }
                        }
                    )
            elif command == "toggle":
                attr = text_data_json['attr']
                vtp_id = text_data_json['vtp']
                
                if vtp_id is None:
                    raise Exception('Invalid parameters', 'invalid')
                
                vtp_id = int(vtp_id)
                
                vtp = VirtualTablePlayer.objects.get(pk=vtp_id)
                
                if vtp.virtual_table.id != self.vt_id:
                    # import pdb; pdb.set_trace()
                    raise Exception('Incoherent parameters', 'incoherent')
                
                if vtp.virtual_table.game_master.id != self.scope['user'].id:
                    raise Exception('You have no rights to change this data', 'unauthorized_toggle')
                
                if attr == 'rights':
                    try:
                        right = text_data_json['value']
                    except KeyError:
                        raise Exception('Missing access right', 'missing_access_right')
                    except Exception as e:
                        raise e
                    
                    if right not in (vtp.EDIT_LVL_ALL, vtp.EDIT_LVL_GM, vtp.EDIT_LVL_PLAYER):
                        raise ValueError('Unkown access right', 'unknown_access_right')
                    
                    vtp.edit_level = right
                elif attr == 'gest_advantage':
                    try:
                        gest_advantage = text_data_json['value']
                    except KeyError:
                        raise Exception('Missing gest advantage', 'missing_access_gest_advantage')
                    except Exception as e:
                        raise e
                    
                    if gest_advantage not in (vtp.GEST_ADVANTAGE_BALANCE, vtp.GEST_ADVANTAGE_LIGHT, vtp.GEST_ADVANTAGE_NONE):
                        raise ValueError('Unkown gest_advantage', 'unknown_access_gest_advantage')
                    
                    vtp.gest_advantage = gest_advantage
                elif attr == 'ignore_health_loss':
                    try:
                        value = text_data_json['value']
                    except KeyError:
                        value = False
                    except Exception as e:
                        raise e
                    
                    vtp.ignore_health_loss_by_armour = value
                else:
                    raise Exception("Not known commande for toggle", "unknown_toggle_command")
                
                vtp.save()
                vtp = VirtualTablePlayer.objects.annotate(
                    editable_by_user=Case(
                        When(edit_level=VirtualTablePlayer.EDIT_LVL_ALL, then=True),
                        When(edit_level=VirtualTablePlayer.EDIT_LVL_PLAYER, player=self.scope['user'], then=True),
                        When(virtual_table__game_master=self.scope['user'], then=True),
                        default=False,
                        output_field=BooleanField()
                    )
                ).get(pk=vtp.id)
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "refresh",
                            "vtp_to_load": vtp.id
                        }
                    }
                )
            elif command == "combo":
                # import pdb; pdb.set_trace()
                
                vtp = None
                gm_roll = text_data_json['gm_roll']
                logger.info('[%s] [COMBO] JSON: %s ' % (command_id, text_data))
                
                reroll_failure = False
                
                if gm_roll:
                    vt = VirtualTable.objects.get(pk=self.vt_id)
                    if vt.game_master.id != self.scope['user'].id:
                        logger.error('[%s] [COMBO] %d tried a GM roll (%d) ' % (command_id, self.scope['user'].id, vt.game_master.id))
                        raise Exception('You can not do a gm roll if you are not the GM of the table', 'unauthorized_gm_combo_roll')
                else:
                    vtp_id = text_data_json['vtp']
                    
                    if vtp_id is None:
                        logger.error('[%s] [COMBO] no VTP found ' % command_id)
                        raise Exception('Invalid parameters', 'invalid')
                    
                    vtp_id = int(vtp_id)
                    
                    vtp = VirtualTablePlayer.objects.get(pk=vtp_id)
                    
                    if vtp.virtual_table.id != self.vt_id:
                        logger.error('[%s] [COMBO] VTP %d do not belong to the VT %d ' % (command_id, vtp.id, self.vt_id))
                        raise Exception('Incoherent parameters', 'incoherent')
                    
                    if vtp.gest_advantage == vtp.GEST_ADVANTAGE_BALANCE:
                        reroll_failure = True
                
                try:
                    dice_number = text_data_json['dice_number']
                except KeyError:
                    dice_number = 0
                except Exception as e:
                    logger.critical('[%s] [COMBO] unknown exception on dice number %s' % (command_id, str(e)))
                    raise e
                
                try:
                    overdrive_value = text_data_json['overdrive_value']
                except KeyError:
                    overdrive_value = 0
                except Exception as e:
                    logger.critical('[%s] [COMBO] unknown exception on overdrive value %s' % (command_id, str(e)))
                    raise e
                
                try:
                    support = text_data_json['support']
                except KeyError:
                    support = False
                except Exception as e:
                    logger.critical('[%s] [COMBO] unknown exception on support value %s' % (command_id, str(e)))
                    raise e
                
                if not isinstance(dice_number, int):
                    dice_number = 0
                
                if not isinstance(overdrive_value, int):
                    overdrive_value = 0
                
                if dice_number > 999:
                    dice_number = 999
                elif dice_number < 0:
                    dice_number = 0
                
                if overdrive_value > 999:
                    overdrive_value = 999
                elif overdrive_value < 0:
                    overdrive_value = 0
                    
                response = {
                    'success': 0,
                    'exploit': False,
                    'fail': False,
                    'dice': dice_number,
                    'od': overdrive_value,
                    'detail': {
                        2: {
                            'class': 'dice-two',
                            'count': 0
                        },
                        4: {
                            'class': 'dice-four',
                            'count': 0
                        },
                        6: {
                            'class': 'dice-six',
                            'count': 0
                        },
                        1: {
                            'class': 'dice-one',
                            'count': 0
                        },
                        3: {
                            'class': 'dice-three',
                            'count': 0
                        },
                        5: {
                            'class': 'dice-five',
                            'count': 0
                        }
                    },
                }
                
                if dice_number > 0:
                    try:
                        dice_command = '%(dt)dd6c[%%2=0]i:[=0]{0}{i:[=%(dt)d]{+%(dt)dd6c[%%2=0]+%(od)d}{+%(od)d}}' % {'dt': dice_number, 'od': overdrive_value}
                        if support:
                            dice_command = '%(dt)dd6c[%%2=0]' % {'dt': dice_number, 'od': overdrive_value}
                        elif reroll_failure:
                            # command line: ${COMBO}d6;\$1c[%2=0];\$1c[%2=1];\$3i:[>0]{\$3d6}{0}c[%2=0];\$2+\$4;\$5i:[=${COMBO}]{${COMBO}d6c[%2=0]}{0};\$5i:[=0]{0}{\$6+\$5+${OD}}
                            # full explanation:
                            #                instruction                -                                        description                                        --         commentary
                            # ${COMBO}d6;                               - 1. roll COMBO d6                                                                          -- initial roll
                            # \$1c[%2=0];                               - 2. on instruction 1, count even                                                           -- INITIAL SUCCESS
                            # \$1c[%2=1];                               - 3. on instruction 1, count odd                                                            -- tmp
                            # \$3i:[>0]{\$3d6}{0}c[%2=0];               - 4. if instruction 3 > 0 then roll [instruction 3]D6, else roll 0 and anyway count even    -- BALANCE KNIGHT SUCCESS
                            # \$2+\$4;                                  - 5. add instruction 2 and instruction 4                                                    -- initial + balance
                            # \$5i:[=${COMBO}]{${COMBO}d6c[%2=0]}{0};   - 6. if instruction 5 = COMBO then roll COMBO d6, count even, else 0                        -- exploit roll
                            # \$5i:[=0]{0}{\$6+\$5+${OD}}               - 7. if instruction 5 = 0 then 0 (critical fail), else add instructions 6, 5 and OD         -- final count
                            dice_command = '%(dt)dd6;$1c[%%2=0];$1c[%%2=1];$3i:[>0]{$3d6}{0}c[%%2=0];$2+$4;$5i:[=%(dt)d]{%(dt)dd6c[%%2=0]}{0};$5i:[=0]{0}{$6+$5+%(od)d}' % {'dt': dice_number, 'od': overdrive_value}
                        
                        logger.debug(dice_command)
                        byte_output = subprocess.check_output(['dice', '-j', dice_command])
                    except subprocess.CalledProcessError as e:
                        logger.critical('[%s] [COMBO] exec command failed, maybe binary is not installed? %s' % (command_id, str(e)))
                        raise e
                    except Exception as e:
                        logger.critical('[%s] [COMBO] unknown exception on exec command %s' % (command_id, str(e)))
                        raise e
                    
                    result_object = json.loads(byte_output.decode())
                    logger.debug(result_object)
                    
                    last_scalar = result_object['scalar'].split(',')[-1]
                    response['success'] = int(last_scalar)
                    response['support'] = support
                    
                    if not support:
                        if response['success'] > 0 and response['success'] - overdrive_value >= dice_number:
                            response['exploit'] = True
                        
                        if response['success'] == 0:
                            response['fail'] = True
                    
                    first_roll = True
                    for diceroll in result_object['values']:
                        for dice in diceroll['values']:
                            # Ignore an even dice if it's on the first roll of a non-support knight of balance... :)
                            if (reroll_failure
                              and first_roll
                              and dice['total'] % 2 == 1
                              and not support):
                                continue
                            
                            response['detail'][dice['total']]['count'] = response['detail'][dice['total']]['count'] + 1
                        
                        first_roll = False
                    
                    logger.info('[%s] [COMBO] result: %s' % (command_id, json.dumps(response)))
                    
                    roll = VirtualTableRoll()
                    roll.datetime = timezone.now()
                    roll.response_json = json.dumps(response)
                    roll.combo = True
                    roll.public = text_data_json['public']
                    
                    if gm_roll:
                        roll.virtual_table_gm = vt
                    else:
                        roll.virtual_table_player = vtp
                    
                    roll.save()
                    VirtualTableRoll.objects.filter(datetime__lte=timezone.now()-timezone.timedelta(hours=12)).delete()
                
                # Send message to room group
                if gm_roll:
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "combo_gm_result",
                                "public": text_data_json['public'],
                                "response": response,
                            }
                        }
                    )
                else:
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "combo_result",
                                "public": text_data_json['public'],
                                "vtp": model_to_dict(vtp),
                                "response": response,
                            }
                        }
                    )
            elif command == "damage":
                # import pdb; pdb.set_trace()
                
                vtp = None
                gm_roll = text_data_json['gm_roll']
                logger.info('[%s] [DAMAGE] JSON: %s ' % (command_id, text_data))
                
                force_low_result_to_4 = False
                if gm_roll:
                    vt = VirtualTable.objects.get(pk=self.vt_id)
                    if vt.game_master.id != self.scope['user'].id:
                        logger.error('[%s] [DAMAGE] %d tried a GM roll (%d) ' % (command_id, self.scope['user'].id, vt.game_master.id))
                        raise Exception('You can not do a gm roll if you are not the GM of the table', 'unauthorized_gm_combo_roll')
                else:
                    vtp_id = text_data_json['vtp']
                    
                    if vtp_id is None:
                        logger.error('[%s] [DAMAGE] no VTP found ' % command_id)
                        raise Exception('Invalid parameters', 'invalid')
                    
                    vtp_id = int(vtp_id)
                    
                    vtp = VirtualTablePlayer.objects.get(pk=vtp_id)
                    
                    if vtp.virtual_table.id != self.vt_id:
                        logger.error('[%s] [DAMAGE] VTP %d do not belong to the VT %d ' % (command_id, vtp.id, self.vt_id))
                        raise Exception('Incoherent parameters', 'incoherent')
                    
                    if vtp.gest_advantage == vtp.GEST_ADVANTAGE_LIGHT:
                        force_low_result_to_4 = True
                    
                try:
                    dice_number = text_data_json['dice_number']
                except KeyError:
                    dice_number = 0
                except Exception as e:
                    logger.critical('[%s] [DAMAGE] unknown exception on dice number %s' % (command_id, str(e)))
                    raise e
                
                try:
                    bonus_value = text_data_json['bonus_value']
                except KeyError:
                    bonus_value = 0
                except Exception as e:
                    logger.critical('[%s] [DAMAGE] unknown exception on bonus value %s' % (command_id, str(e)))
                    raise e
                
                if not isinstance(dice_number, int):
                    dice_number = 0
                
                if not isinstance(bonus_value, int):
                    bonus_value = 0
                
                if dice_number > 999:
                    dice_number = 999
                elif dice_number < 0:
                    dice_number = 0
                
                if bonus_value > 999:
                    bonus_value = 999
                elif bonus_value < -999:
                    bonus_value = -999
                
                response = {
                    'damage': bonus_value,
                    'dice': dice_number,
                    'bonus': bonus_value,
                    'detail': {
                        1: {
                            'class': 'dice-one',
                            'count': 0
                        },
                        2: {
                            'class': 'dice-two',
                            'count': 0
                        },
                        3: {
                            'class': 'dice-three',
                            'count': 0
                        },
                        4: {
                            'class': 'dice-four',
                            'count': 0
                        },
                        5: {
                            'class': 'dice-five',
                            'count': 0
                        },
                        6: {
                            'class': 'dice-six',
                            'count': 0
                        },
                    },
                }
                
                if dice_number > 0:
                    try:
                        dice_command = '%(dice)dd6+%(bonus)d' % {'dice': dice_number, 'bonus': bonus_value}
                        logger.debug(dice_command)
                        byte_output = subprocess.check_output(['dice', '-j', dice_command])
                    except subprocess.CalledProcessError as e:
                        logger.critical('[%s] [DAMAGE] exec command failed, maybe binary is not installed? %s' % (command_id, str(e)))
                        raise e
                    except Exception as e:
                        logger.critical('[%s] [DAMAGE] unknown exception on exec command %s' % (command_id, str(e)))
                        raise e
                    
                    result_object = json.loads(byte_output.decode())
                    logger.debug(result_object)
                    
                    response['damage'] = int(result_object['scalar'])
                    response['stronger_damage'] = int(result_object['scalar'])
                    
                    for dice in result_object['values'][0]['values']:
                        response['detail'][dice['total']]['count'] = response['detail'][dice['total']]['count'] + 1
                        val = int(dice['total'])
                        if force_low_result_to_4 and val < 4:
                            response['stronger_damage'] = response['stronger_damage'] + (4 - val)
                    
                    logger.info('[%s] [DAMAGE] result: %s' % (command_id, json.dumps(response)))
                    
                    roll = VirtualTableRoll()
                    roll.datetime = timezone.now()
                    roll.response_json = json.dumps(response)
                    roll.combo = False
                    roll.public = text_data_json['public']
                    
                    if gm_roll:
                        roll.virtual_table_gm = vt
                    else:
                        roll.virtual_table_player = vtp
                    
                    roll.save()
                    VirtualTableRoll.objects.filter(datetime__lte=timezone.now()-timezone.timedelta(hours=12)).delete()
                
                # Send message to room group
                if gm_roll:
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "damage_gm_result",
                                "public": text_data_json['public'],
                                "response": response,
                            }
                        }
                    )
                else:
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "damage_result",
                                "public": text_data_json['public'],
                                "vtp": model_to_dict(vtp),
                                "response": response,
                            }
                        }
                    )
            elif command == "destiny":
                # import pdb; pdb.set_trace()
                
                vtp_id = text_data_json['vtp']
                logger.info('[%s] [DESTINY] JSON: %s ' % (command_id, text_data))
                
                if vtp_id is None:
                    raise Exception('Invalid parameters', 'invalid')
                
                vtp_id = int(vtp_id)
                
                vtp = VirtualTablePlayer.objects.get(pk=vtp_id)
                
                if vtp.virtual_table.id != self.vt_id:
                    raise Exception('Incoherent parameters', 'incoherent')
                    
                arcana = MajorArcana.objects.order_by('?').first()
                
                final_arcana = {
                    "number": arcana.number,
                    "roman_number": arcana.roman_number,
                    "name": arcana.name,
                    "destiny_effect": markdown(arcana.destiny_effect),
                }
                logger.info('[%s] [DESTINY] result: %d - %s' % (command_id, arcana.number, arcana.name))
                
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "destiny_result",
                            "arcana": final_arcana,
                            "vtp": model_to_dict(vtp),
                        }
                    }
                )
            elif command == "recover":
                vtp_ids = text_data_json['vtps']
                
                if len(vtp_ids) == 0:
                    raise Exception('Recover without targeted vtp are not possible', 'recover_no_vtp')
                
                vtps = VirtualTablePlayer.objects.filter(pk__in=vtp_ids, virtual_table=self.vt_id, virtual_table__game_master__id=self.scope['user'].id)
                
                if len(vtp_ids) != len(vtps):
                    raise Exception("Recover can be sent only to vtp under this virtual table and by the game master of this table", "recover_vtp_unknown_to_vt")
                
                for vtp in vtps:
                    restore_vtp_counters(vtp)
                    
                    # Send message to room group
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "refresh",
                                "vtp_to_load": vtp.id
                            }
                        }
                    )
            elif command == "end_battle":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                if vt.game_master.id != self.scope['user'].id:
                    logger.error('[%s] [END_BATTLE] %d tried to end battle ' % (command_id, self.scope['user'].id))
                    raise Exception('You can not end battle if you are not the GM of the table', 'unauthorized_gm_end_battle')
                
                for vtp in vt.players.all():
                    restore_vtp_counters(vtp)
                    
                    vtp.current_flux_points = 0
                    vtp.time_paradox_difficulty = 0
                    
                    vtp.save()
                    
                    # Send message to room group
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "refresh",
                                "vtp_to_load": vtp.id
                            }
                        }
                    )
            elif command == "end_mission":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                if vt.game_master.id != self.scope['user'].id:
                    logger.error('[%s] [END_MISSION] %d tried to end mission ' % (command_id, self.scope['user'].id))
                    raise Exception('You can not end mission if you are not the GM of the table', 'unauthorized_gm_end_mission')
                
                try:
                    rewards = text_data_json['rewards']
                except KeyError:
                    rewards = {}
                except Exception as e:
                    raise e
                
                for vtp in vt.players.all():
                    restore_vtp_counters(vtp)
                    
                    vtp.current_health_points = vtp.max_health_points
                    vtp.current_armour_points = vtp.max_armour_points
                    vtp.current_energy_points = vtp.max_energy_points
                    vtp.current_health_nod = vtp.max_health_nod
                    vtp.current_armour_nod = vtp.max_armour_nod
                    vtp.current_energy_nod = vtp.max_energy_nod
                    vtp.current_grenade_stock = vtp.max_grenade_stock
                    vtp.current_flux_points = 0
                    vtp.time_paradox_difficulty = 0
                    vtp.impregnation_difficulty = 0
                    
                    # Managing GP and XP here
                    try:
                        vtp_reward = rewards[str(vtp.id)]
                        
                        try:
                            vtp.current_gp = vtp.current_gp + vtp_reward['gp']
                            vtp.total_gp = vtp.total_gp + vtp_reward['gp']
                        except KeyError:
                            # strange, but meh.
                            pass
                        except Exception as e:
                            raise e
                        
                        try:
                            vtp.current_xp = vtp.current_xp + vtp_reward['xp']
                            vtp.total_xp = vtp.total_xp + vtp_reward['xp']
                        except KeyError:
                            # strange, but meh.
                            pass
                        except Exception as e:
                            raise e
                        
                        try:
                            vtp.current_heroism_points = vtp.current_heroism_points + vtp_reward['hep']
                            if vtp.current_heroism_points > vtp.max_heroism_points:
                                vtp.current_heroism_points = vtp.max_heroism_points
                        except KeyError:
                            # strange, but meh.
                            pass
                        except Exception as e:
                            raise e
                            
                    except KeyError:
                        pass
                    except Exception as e:
                        raise e
                        
                    vtp.save()
                    
                    # Send message to room group
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": "refresh",
                                "vtp_to_load": vtp.id
                            }
                        }
                    )
            elif command == "recap":
                vtp_ids = text_data_json['vtps']
                
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                for vtp in vt.players.all():
                    # import pdb; pdb.set_trace()
                    if vtp.id in vtp_ids: 
                        async_to_sync(self.channel_layer.group_send)(
                            self.vt_group_name,
                            {
                                'type': 'vt_message',
                                'params': {
                                    "command": "refresh",
                                    "vtp_to_load": vtp.id,
                                    "self_target": True,
                                    "origin": self.scope['user'].id
                                }
                            }
                        )
                        vtp_ids.remove(vtp.id)
                    else:
                        async_to_sync(self.channel_layer.group_send)(
                            self.vt_group_name,
                            {
                                'type': 'vt_message',
                                'params': {
                                    "command": 'new',
                                    "vtp_id": vtp.id,
                                    "self_target": True,
                                    "origin": self.scope['user'].id
                                }
                            }
                        )
                
                if len(vtp_ids) > 0:
                    async_to_sync(self.channel_layer.group_send)(
                        self.vt_group_name,
                        {
                            'type': 'vt_message',
                            'params': {
                                "command": 'del',
                                "vtp": vtp_ids,
                                "self_target": True,
                                "origin": self.scope['user'].id
                            }
                        }
                    )
            elif command == "config":
                classes_to_show = text_data_json['classes_to_show']
                vtp_id = text_data_json['vtp']
                
                if vtp_id is None:
                    raise Exception('Invalid parameters', 'invalid')
                
                vtp_id = int(vtp_id)
                
                vtp = VirtualTablePlayer.objects.get(pk=vtp_id)
                
                if vtp.virtual_table.id != self.vt_id:
                    # import pdb; pdb.set_trace()
                    raise Exception('Incoherent parameters', 'incoherent')
                
                if ((vtp.edit_level == vtp.EDIT_LVL_PLAYER
                  and vtp.player.id != self.scope['user'].id
                  and vtp.virtual_table.game_master.id != self.scope['user'].id)
                  or (vtp.edit_level == vtp.EDIT_LVL_GM
                  and vtp.virtual_table.game_master.id != self.scope['user'].id)):
                    raise Exception('You have no rights to change this data', 'unauthorized_config')
                
                vtp.classes_to_show = classes_to_show
                vtp.save()
                # import pdb; pdb.set_trace()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "refresh",
                            "vtp_to_load": vtp.id
                        }
                    }
                )
            elif command == "avalon_vote":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                new_ordered_extensions = text_data_json['new_ordered_extensions']
                
                current_weight = len(new_ordered_extensions)
                relative_current_weight = vt.avalon.extensions.filter(built=False).count()
                
                for extension_id in new_ordered_extensions:
                    extension_id = int(extension_id)
                    avalon_extension = vt.avalon.extensions.get(pk=extension_id)
                    
                    try:
                        vote_to_update = VirtualTableAvalonExtensionVote.objects.get(virtual_table_avalon_extension=avalon_extension, user=self.scope['user'])
                    except ObjectDoesNotExist as e:
                        vote_to_update = VirtualTableAvalonExtensionVote()
                        vote_to_update.user = self.scope['user']
                        vote_to_update.virtual_table_avalon_extension = avalon_extension
                    except Exception as e:
                        raise e
                    
                    vote_to_update.weight = current_weight
                    current_weight = current_weight - 1
                    if not avalon_extension.built:
                        vote_to_update.relative_weight = relative_current_weight
                        relative_current_weight = relative_current_weight - 1
                    else:
                        vote_to_update.relative_weight = 0
                    
                    vote_to_update.save()
                    
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "refresh_avalon",
                        }
                    }
                )
            elif command == "avalon_extension_toggle":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                if vt.game_master.id != self.scope['user'].id:
                    logger.error('[%s] [AVALON_TOGGLE] %d tried to toggle an avalon extension ' % (command_id, self.scope['user'].id))
                    raise Exception('You can not toggle avalon extension if you are not the GM of the table', 'unauthorized_gm_toggle_avalon_extension')
                
                extension_id = text_data_json['extension_id']
                
                extension_id = int(extension_id)
                avalon_extension = vt.avalon.extensions.get(pk=extension_id)
                avalon_extension.built = not avalon_extension.built
                avalon_extension.save()
                
                vote_user = None
                max_relative_weight = vt.avalon.extensions.filter(built=False).count()
                
                for vote in VirtualTableAvalonExtensionVote.objects.filter(virtual_table_avalon_extension__virtual_table_avalon=vt.avalon).order_by('user', '-weight').all():
                    if vote_user != vote.user:
                        vote_user = vote.user
                        relative_weight = max_relative_weight
                    
                    if not vote.virtual_table_avalon_extension.built:
                        vote.relative_weight = relative_weight
                        relative_weight = relative_weight - 1
                        vote.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "refresh_avalon",
                        }
                    }
                )
            elif command == "avalon_reset_votes":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                if vt.game_master.id != self.scope['user'].id:
                    logger.error('[%s] [AVALON_VOTE_RESET] %d tried to reset avalon votes ' % (command_id, self.scope['user'].id))
                    raise Exception('You can not reset avalon votes if you are not the GM of the table', 'unauthorized_gm_avalon_vote_reset')
                
                VirtualTableAvalonExtensionVote.objects.filter(virtual_table_avalon_extension__virtual_table_avalon=vt.avalon).delete()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "refresh_avalon",
                        }
                    }
                )
            elif command == "avalon_update_points":
                vt = VirtualTable.objects.get(pk=self.vt_id)
                
                if vt.game_master.id != self.scope['user'].id:
                    logger.error('[%s] [AVALON_UPDATE_POINTS] %d tried to change avalon construction points ' % (command_id, self.scope['user'].id))
                    raise Exception('You can not update avalon total construction points if you are not the GM of the table', 'unauthorized_gm_avalon_update_points')
                
                points = text_data_json['points']
                
                if not isinstance(points, int):
                    logger.error('[%s] [AVALON_UPDATE_POINTS] construction points "%s" is not an integer, updated to 0.' % (command_id, points))
                    points = 0
                
                vt.avalon.total_construction_points = points
                vt.avalon.save()
                
                # Send message to room group
                async_to_sync(self.channel_layer.group_send)(
                    self.vt_group_name,
                    {
                        'type': 'vt_message',
                        'params': {
                            "command": "refresh_avalon",
                        }
                    }
                )
            else:
                raise Exception('Not known command.', 'unknown_command')
            
            
            
        except Exception as e:
            # import pdb; pdb.set_trace()
            logger.exception("[%s] [GLOBAL] a global exception occured while executing the command (JSON: %s) " % (command_id, text_data))
            try:
                self.send(text_data=json.dumps({
                    'error': {
                        "message": e.args[0],
                        "code": e.args[1],
                    }
                }))
            except Exception as e:
                self.send(text_data=json.dumps({
                    'error': {
                        "message": str(e),
                        "code": "",
                    }
                }))
        
    # Receive message from room group
    def vt_message(self, event):
        params = event['params']
        
        if 'self_target' in params and params['self_target'] and params['origin'] != self.scope['user'].id:
            return
        
        if params['command'] == 'status':
            if params['origin'] != self.scope['user'].id:
                # we want to refresh only for the user who asked the refresh.
                return
        elif params['command'] == 'del':
            params['new_url'] = ''
            if len(VirtualTablePlayer.objects.filter(player=self.scope['user'],virtual_table=self.vt_id)) == 0:
                params['new_url'] = reverse('virtualtable:vt_list')
        elif params['command'] == 'kick':
            params['new_url'] = reverse('virtualtable:vt_list')
        elif params['command'] == 'new':
            vtp = VirtualTablePlayer.objects.annotate(
                editable_by_user=Case(
                    When(edit_level=VirtualTablePlayer.EDIT_LVL_ALL, then=True),
                    When(edit_level=VirtualTablePlayer.EDIT_LVL_PLAYER, player=self.scope['user'], then=True),
                    When(virtual_table__game_master=self.scope['user'], then=True),
                    default=False,
                 
                    output_field=BooleanField()
                )
            ).get(pk=params['vtp_id'])
            
            template = loader.get_template('virtualtable/vt_detail_character.html')
            html = template.render({
                "character": vtp,
                "user": self.scope['user'],
            })
            params['html'] = html
            params['vtp'] = model_to_dict(vtp)
            params["editable_by_user"] = vtp.editable_by_user
        elif params['command'] == 'refresh':
            vtp = VirtualTablePlayer.objects.annotate(
                editable_by_user=Case(
                    When(edit_level=VirtualTablePlayer.EDIT_LVL_ALL, then=True),
                    When(edit_level=VirtualTablePlayer.EDIT_LVL_PLAYER, player=self.scope['user'], then=True),
                    When(virtual_table__game_master=self.scope['user'], then=True),
                    default=False,
                    
                    output_field=BooleanField()
                )
            ).get(pk=params['vtp_to_load'])
            params['vtp'] = model_to_dict(vtp)
            params["editable_by_user"] = vtp.editable_by_user
        elif params['command'] == 'refresh_avalon':
            template = loader.get_template('virtualtable/vt_detail_avalon.html')
            vt = VirtualTable.objects.get(pk=self.vt_id)
            vt.avalon.ordered_extensions_by_user = vt.avalon.get_extensions_by_user_vote(self.scope['user'])
            html = template.render({
                "user": self.scope['user'],
                "virtualtable": vt,
            })
            params['html'] = html
        elif params['command'] == 'combo_result' or params['command'] == 'damage_result':
            vt = VirtualTable.objects.get(pk=self.vt_id)
            
            if vt is None:
                return
            
            if not params['public'] and vt.game_master.id != self.scope['user'].id and params['vtp']['player'] != self.scope['user'].id:
                return
        elif params['command'] == 'destiny_result':
            vt = VirtualTable.objects.get(pk=self.vt_id)
            
            if vt is None:
                return
        elif params['command'] == 'combo_gm_result' or params['command'] == 'damage_gm_result':
            vt = VirtualTable.objects.get(pk=self.vt_id)
            
            if vt is None:
                return
            
            if not params['public'] and (vt.game_master.id != self.scope['user'].id):
                return
        self.send(text_data=json.dumps(params))

    # Receive message from room group
    def vt_status(self, event):
        params = event['params']
        
        async_to_sync(self.channel_layer.group_send)(
            self.vt_group_name,
            {
                'type': 'vt_message',
                'params': {
                    'command': 'status',
                    'origin': params['user'],
                    'counter': params['counter'],
                    'vtp': list(VirtualTablePlayer.objects.filter(player=self.scope['user'].id).values_list('id', flat=True)),
                }
            }
        )
        
        self.send(text_data=json.dumps(params))