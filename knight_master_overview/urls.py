"""knight_master_overview URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf.urls.i18n import i18n_patterns
from django.conf import settings
from django.contrib.auth.decorators import login_required

from .views import account

urlpatterns = [
    path('camelot/', admin.site.urls),
    path('martor/', include('martor.urls')),
    path('i18n/', include('django.conf.urls.i18n')),
    path('api/', include('api.urls')),
    path('accounts/login/', account.JointLoginSignupView.as_view(), name='account_login'),
    path('accounts/', include('allauth.urls')),
    path('robots.txt', include('robots.urls')),
]

gm_urls = ([
    path('', include('gm.urls')),
], 'gm')


virtualtable_urls = ([
    path('', include('virtualtable.urls')),
], 'virtualtable')

urlpatterns += i18n_patterns(
    path('', include('gm.urls')),
    path('virtualtable/', include('virtualtable.urls')),
    path('users/', include('users.urls')),
    prefix_default_language=True
)

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns += [
        path('rosetta/', include('rosetta.urls'))
    ]
