from django.db import models
from django.urls import reverse
from mptt.models import MPTTModel, TreeForeignKey

from martor.models import MartorField
from gettext import gettext
import bleach
from markdown import markdown

class Category(MPTTModel):
    name = models.CharField("nom", max_length=128)
    css_classes = models.CharField("classes CSS", max_length=256, blank=True)
    parent = TreeForeignKey(
        'self',
        on_delete=models.CASCADE,
        verbose_name="Catégorie parent",
        related_name="children",
        blank=True,
        null=True
    )
    
    class MPTTMeta:
        order_insertion_by = ['name']
    
    def __str__(self):
        return self.name
    

class Question(models.Model):
    question = models.CharField("question", max_length=512)
    answer = MartorField("réponse", blank=True, null=True)
    gm = models.BooleanField('filtre MJ', blank=True, default=False)
    v1 = models.BooleanField('compatible v1', blank=True, default=True)
    v1_5 = models.BooleanField('compatible v1.5', blank=True, default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    category = TreeForeignKey(
        Category,
        on_delete=models.CASCADE,
        verbose_name="catégorie",
        related_name="questions"
    )
    
    def __str__(self):
        return self.question
        
    def question_text_only(self):
        return bleach.clean(markdown(self.question), tags=[], strip=True)
    
    def answer_text_only(self):
        if self.answer:
            return bleach.clean(markdown(self.answer), tags=[], strip=True)
        else:
            return ''
    
