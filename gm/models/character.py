from django.db import models
from django.urls import reverse

from martor.models import MartorField
from gettext import gettext
import bleach
from markdown import markdown

from .faq import Question

class Aspect(models.Model):
    name = models.CharField("nom", max_length=32)
    player_description = models.TextField("description")
    adjectives = models.CharField("adjectifs", max_length=128)
    npc_description = models.TextField("description pour NPC")
    exceptional_global_description = models.CharField("description aspect exceptionnel", max_length=256)
    exceptional_minor_effect_description = models.CharField("effet exceptionnel mineur", max_length=256)
    exceptional_major_effect_description = models.CharField("effet exceptionnel majeur", max_length=256)
    
    def __str__(self):
        return self.name

class Characteristic(models.Model):
    name = models.CharField("nom", max_length=32)
    description = models.TextField()
    overdrive_global_description = models.CharField("Descriptif général overdrive", max_length=128)
    aspect = models.ForeignKey(Aspect, on_delete=models.PROTECT, related_name="characteristics")
    
    def __str__(self):
        return self.name
    
class Source(models.Model):
    name = models.CharField(max_length=32,default='Livre de Base')
    def __str__(self):
        return self.name

class Archetype(models.Model):
    name = models.CharField("nom", max_length=64)
    description = models.TextField()
    characteristic_bonus_1 = models.ForeignKey(Characteristic, on_delete=models.PROTECT, related_name='+', verbose_name="carac. bonus 1", blank=True, null=True) # # Custom archetype forced the optional attribute for this one...
    characteristic_bonus_2 = models.ForeignKey(Characteristic, on_delete=models.PROTECT, related_name='+', verbose_name="carac. bonus 2", blank=True, null=True) # # Custom archetype forced the optional attribute for this one...
    characteristic_bonus_3 = models.ForeignKey(Characteristic, on_delete=models.PROTECT, related_name='+', verbose_name="carac. bonus 3", blank=True, null=True)
    slug = models.SlugField(null=True, blank=True, unique=True)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.name
            
    def get_absolute_url(self):
        return reverse('gm:archetype_detail', args=[self.slug])
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
    
class GreatDeed(models.Model):
    name = models.CharField("nom", max_length=64)
    description = models.TextField()
    restriction = MartorField(max_length=256)
    aspect_1 = models.ForeignKey(Aspect, on_delete=models.PROTECT, related_name='+', verbose_name="choix aspect", blank=True, null=True) # Custom great deed forced the optional attribute for this one...
    aspect_2 = models.ForeignKey(Aspect, on_delete=models.PROTECT, related_name='+', verbose_name="choix aspect", blank=True, null=True)
    nickname_suggestion = models.CharField("surnoms possibles", max_length=256)
    slug = models.SlugField(null=True, blank=True, unique=True)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.name
    
    def restriction_text_only(self):
        return bleach.clean(markdown(self.restriction), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
            
    def get_absolute_url(self):
        return reverse('gm:great_deed_detail', args=[self.slug])

class Crest(models.Model):
    name = models.CharField("nom", max_length=32)
    description = models.CharField(max_length=512)
    vow = models.CharField("vœu", max_length=128)
    slug = models.SlugField(null=True, blank=True, unique=True)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.name
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
            
    def get_absolute_url(self):
        return reverse('gm:crest_detail', args=[self.slug])

class MajorArcana(models.Model):
    name = models.CharField("nom", max_length=64)
    description = models.TextField()
    destiny_quote = models.CharField("citation destinée", max_length=128) 
    destiny_effect = MartorField("effet destinée")
    past = models.TextField("passé")
    creation_bonus = MartorField("bonus en création")
    aspect_point = models.PositiveSmallIntegerField("points d'aspect", default=1)
    characteristic_point = models.PositiveSmallIntegerField("points de carac.", default=3)
    forced_aspect = models.ForeignKey(Aspect,on_delete=models.PROTECT,verbose_name="aspect associé",blank=True,null=True) # Arcana 0 and 16 forced the optional attribute for this one...
    advantage_name = models.CharField("avantage", max_length=64)
    advantage = MartorField("avantage détaillé")
    disadvantage_name = models.CharField("inconvénient", max_length=64)
    disadvantage = MartorField("inconvénient détaillé")
    ai_description = models.TextField("description IA")
    ai_letter = models.CharField("lettre IA", max_length=2)
    ai_keywords = models.CharField("mots-clés IA", max_length=256)
    ai_behavior = models.TextField("caractère IA")
    ai_advantage_name = models.CharField("avantage IA", max_length=64)  
    ai_advantage = MartorField("avantage IA détaillé")
    ai_disadvantage_name = models.CharField("inconvénient IA", max_length=64)  
    ai_disadvantage = MartorField("inconvénient IA détaillé")
    slug = models.SlugField(null=True, blank=True, unique=True)
    roman_number = models.CharField("numéro romain", max_length=5, blank=True)
    number = models.PositiveSmallIntegerField("numéro", default=0)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.name
    
    def destiny_effect_text_only(self):
        return bleach.clean(markdown(self.destiny_effect), tags=[], strip=True)
    
    def creation_bonus_text_only(self):
        return bleach.clean(markdown(self.creation_bonus), tags=[], strip=True)
    
    def advantage_text_only(self):
        return bleach.clean(markdown(self.advantage), tags=[], strip=True)
    
    def disadvantage_text_only(self):
        return bleach.clean(markdown(self.disadvantage), tags=[], strip=True)
    
    def ai_advantage_text_only(self):
        return bleach.clean(markdown(self.ai_advantage), tags=[], strip=True)
    
    def ai_disadvantage_text_only(self):
        return bleach.clean(markdown(self.ai_disadvantage), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
            
    def get_absolute_url(self):
        return reverse('gm:arcana_detail', args=[self.slug])

class Division(models.Model):
    name = models.CharField("nom", max_length=64)
    description = models.TextField()
    aspect_bonus = models.ForeignKey(Aspect, on_delete=models.PROTECT)
    disadvantage_name = models.CharField("inconvénient", max_length=64)
    disadvantage = MartorField("inconvénient détaillé")
    slug = models.SlugField(null=True, blank=True, unique=True)
    motto = models.CharField("devise", max_length=128, default="")
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.name
    
    def disadvantage_text_only(self):
        return bleach.clean(markdown(self.disadvantage), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
            
    def get_absolute_url(self):
        return reverse('gm:division_detail', args=[self.slug])

class DivisionOverdrive(models.Model):
    division = models.ForeignKey(Division, on_delete=models.CASCADE, related_name="overdrives")
    characteristic = models.ForeignKey(Characteristic, on_delete=models.PROTECT, related_name="divisions")
    
    def __str__(self):
        return self.division.name + ' ' + self.characteristic.name
    
    class Meta:
        unique_together = ("division", "characteristic")

class Armour(models.Model):
    name = models.CharField("nom", max_length=64)
    background_description = models.TextField("description background", blank=True)
    technical_description = models.TextField("description technique", blank=True)
    additional_notes = MartorField("addenda", blank=True)
    generation = models.PositiveSmallIntegerField("génération", default=1)
    energy_points = models.PositiveSmallIntegerField("points d'énergie (pe)")
    armour_points = models.PositiveSmallIntegerField("points d'armure (pa)")
    force_field = models.PositiveSmallIntegerField("champ de force (cdf)",default=12)
    slot_head = models.PositiveSmallIntegerField("slots tête",default=5)
    slot_left_arm = models.PositiveSmallIntegerField("slots bras g.",default=5)
    slot_right_arm = models.PositiveSmallIntegerField("slots bras d.",default=5)
    slot_torso = models.PositiveSmallIntegerField("slots torse",default=8)
    slot_left_leg = models.PositiveSmallIntegerField("slots jambe g.",default=5)
    slot_right_leg = models.PositiveSmallIntegerField("slots jambe d.",default=5)
    slug = models.SlugField(null=True, blank=True, unique=True)
    source = models.ForeignKey(Source, on_delete=models.CASCADE, related_name="armours")
    faq_corrected = models.BooleanField("corrigé par FAQ", default=False)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    password = models.CharField("mot de passe", max_length=256, blank=True, null=True)
    aegis = models.PositiveSmallIntegerField("égide", blank=True, null=True)
    
    def __str__(self):
        return self.name
    
    def additional_notes_text_only(self):
        return bleach.clean(markdown(self.additional_notes), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
    
    def origin(self):
        if self.faq_corrected:
            return '%s (corrigé par FAQ)' % self.source.name
        else:
            return self.source.name
            
    def get_absolute_url(self):
        return reverse('gm:armour_detail', args=[self.slug])

class ArmourOverdrive(models.Model):
    armour = models.ForeignKey(Armour,on_delete=models.CASCADE,verbose_name="armure", related_name="overdrives")
    characteristic = models.ForeignKey(Characteristic,on_delete=models.PROTECT,verbose_name="caractéristique", related_name="armours")
    
    def __str__(self):
        return self.armour.name + ' ' + self.characteristic.name
    
    class Meta:
        unique_together = ("armour", "characteristic")

class ArmourAbility(models.Model):
    armour = models.ForeignKey(Armour,on_delete=models.CASCADE,verbose_name="armure", related_name="abilities")
    name = models.CharField("nom", max_length=64)
    description = MartorField()
    default_max_variant = models.PositiveSmallIntegerField("max variantes / défaut", blank=True, null=True)
    energy = models.CharField("énergie", max_length=128, null=True, blank=True) # yes, this is confusing, but energy in armour abilities are so conditionals...
    activation = models.CharField("activation", max_length=256, null=True, blank=True) # yes, also confusing, but again: activation can be very conditional here.
    hope = models.CharField("espoir", max_length=128, null=True, blank=True) # here again, not an int but a char because it may vary depending on a lot of things...
    duration = models.CharField("durée", max_length=128, null=True, blank=True)
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)

class ArmourEvolution(models.Model):
    armour = models.ForeignKey(Armour,on_delete=models.CASCADE,verbose_name="armure", related_name="evolutions")
    level = models.PositiveSmallIntegerField("niveau")
    unlock_at = models.PositiveSmallIntegerField("condition d'accès")
    description = MartorField()
    
    def __str__(self):
        return self.armour.name + ' evol. ' + str(self.level)
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)

class NonPlayerCharacter(models.Model):
    name = models.CharField("nom", max_length=64)
    defence = models.PositiveSmallIntegerField("défense", null=True, blank=True, default=1)
    reaction = models.PositiveSmallIntegerField("réaction", null=True, blank=True, default=1)
    initiative = models.PositiveSmallIntegerField("initiative", null=True, blank=True, default=1)
    health_points = models.PositiveSmallIntegerField("points de santé (ps)", null=True, blank=True, default=0)
    energy_points = models.PositiveSmallIntegerField("points d'énergie (pe)", null=True, blank=True, default=0)
    armour_points = models.PositiveSmallIntegerField("points d'armure (pa)", null=True, blank=True, default=0)
    force_field = models.PositiveSmallIntegerField("champ de force (cdf)", null=True, blank=True, default=0)
    cohesion = models.PositiveSmallIntegerField("cohésion", null=True, blank=True, default=0)
    outbreak = models.PositiveSmallIntegerField("débordement", null=True, blank=True, default=0)
    speed = models.SmallIntegerField("vitesse", null=True, blank=True, default=0)
    slot_head = models.PositiveSmallIntegerField("slots tête", blank=True, null=True)
    slot_left_arm = models.PositiveSmallIntegerField("slots bras g.", blank=True, null=True)
    slot_right_arm = models.PositiveSmallIntegerField("slots bras d.", blank=True, null=True)
    slot_torso = models.PositiveSmallIntegerField("slots torse", blank=True, null=True)
    slot_left_leg = models.PositiveSmallIntegerField("slots jambe g.", blank=True, null=True)
    slot_right_leg = models.PositiveSmallIntegerField("slots jambe d.", blank=True, null=True)
    simplified = models.BooleanField("NPC Simplifié", default=False) # If False, use NonPlayerCharacterAspect ; if True, use NonPlayerCharacterCharacteristic
    
    def __str__(self):
        return self.name
        
    def get_colspan(self):
        counter = 1
        if (self.initiative):
            counter += 1
        if (self.health_points):
            counter += 1
        if (self.energy_points):
            counter += 1
        if (self.armour_points):
            counter += 1
        if (self.force_field):
            counter += 1
        if (self.cohesion):
            counter += 1
        if (self.outbreak):
            counter += 1
        if (self.speed):
            counter += 1
        return counter
        
    def has_slots(self):
        return (not self.simplified
            and (
                (self.slot_head is not None and self.slot_head > 0)
                or (self.slot_left_arm is not None and self.slot_left_arm > 0)
                or (self.slot_right_arm is not None and self.slot_right_arm > 0)
                or (self.slot_torso is not None and self.slot_torso > 0)
                or (self.slot_left_leg is not None and self.slot_left_leg > 0)
                or (self.slot_right_leg is not None and self.slot_right_leg > 0)
            )
        )
        
    def get_weapons(self):
        return self.weapons.order_by('weapon__category', 'weapon__name').all()
        
    def get_modules(self):
        return self.modules.order_by('module_level__module__name').all()
    
class NonPlayerCharacterAspect(models.Model):
    npc = models.ForeignKey(NonPlayerCharacter, on_delete=models.CASCADE, related_name="aspects", limit_choices_to={"simplified": False})
    aspect = models.ForeignKey(Aspect, on_delete=models.PROTECT, related_name="npcs")
    basic_value = models.SmallIntegerField("valeur de base", default=1)
    except_value = models.PositiveSmallIntegerField("valeur exceptionnelle", null=True, blank=True, default=0)
    except_major = models.BooleanField("aspect except. majeure", default=False)
    
    def __str__(self):
        if self.except_value > 0:
            if self.except_major:
                return "%s - %s : %d (Majeur %d) " % (self.npc.name, self.aspect.name, self.basic_value, self.except_value)
            else:
                return "%s - %s : %d (Mineur %d) " % (self.npc.name, self.aspect.name, self.basic_value, self.except_value)
        else:
            return "%s - %s : %d " % (self.npc.name, self.aspect.name, self.basic_value)
    
    class Meta:
        unique_together = ("npc", "aspect")

class NonPlayerCharacterCharacteristic(models.Model):
    npc = models.ForeignKey(NonPlayerCharacter, on_delete=models.CASCADE, related_name="characteristics", limit_choices_to={"simplified": True})
    characteristic = models.ForeignKey(Characteristic,on_delete=models.PROTECT,verbose_name="caractéristique")
    test_dice = models.PositiveSmallIntegerField("dés de test",default=0)
    overdrive_bonus = models.PositiveSmallIntegerField("bonus overdrive",default=0)
    
    def __str__(self):
        return self.characteristic.name
    
    def get_test(self):
        result = str(self.test_dice) + 'D6'
        if self.overdrive_bonus > 0:
            result += ' + ' + str(self.overdrive_bonus) + 'OD'
        return result

class TraumaCategory(models.Model):
    name = models.CharField("nom", max_length=64)
    description = models.TextField()
    hop_recovered = models.PositiveSmallIntegerField()
    
    def __str__(self):
        return self.name

class Trauma(models.Model):
    name = models.CharField("nom", max_length=64)
    description = MartorField()
    die_value = models.PositiveSmallIntegerField("valeur du dé", blank=True, null=True)
    category = models.ForeignKey(TraumaCategory, on_delete=models.CASCADE, verbose_name="catégorie", related_name="traumas")
    slug = models.SlugField(null=True, blank=True, unique=True)
    password = models.CharField("mot de passe", max_length=256, blank=True, null=True)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
            
    def get_absolute_url(self):
        return reverse('gm:trauma_detail', args=[self.slug])
    
class MechaArmour(models.Model):
    name = models.CharField("nom", max_length=64)
    description = MartorField()
    speed = models.PositiveSmallIntegerField("vitesse", default=0)
    maneuverability = models.SmallIntegerField("manœuvrabilité", default=0)
    power = models.PositiveSmallIntegerField("puissance", default=0)
    sensors = models.PositiveSmallIntegerField("senseurs", default=0)
    systems = models.PositiveSmallIntegerField("systèmes", default=0)
    resilience = models.PositiveSmallIntegerField("résilience", default=0)
    armour_plating = models.PositiveSmallIntegerField("blindage", default=0)
    force_field = models.PositiveSmallIntegerField("champ de force", default=0)
    energy_core = models.PositiveSmallIntegerField("noyau d'énergie", default=100)
    password = models.CharField("mot de passe", max_length=256, blank=True, null=True)
    slug = models.SlugField(null=True, blank=True, unique=True)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
            
    def get_absolute_url(self):
        return reverse('gm:mecha_detail', args=[self.slug])
    
class MechaArmourConfiguration(models.Model):
    name = models.CharField("nom", max_length=64)
    description = MartorField()
    mecha = models.ForeignKey(MechaArmour, on_delete=models.CASCADE, verbose_name="mécha-armure", related_name="configurations")
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)

class NonPlayerCharacterDifficulty(models.Model):
    name = models.CharField("nom", max_length=64)
    
    def __str__(self):
        return self.name

class NonPlayerCharacterType(models.Model):
    name = models.CharField("nom", max_length=64)
    
    def __str__(self):
        return self.name

class NonPlayerCharacterCapacity(models.Model):
    name = models.CharField("nom", max_length=128)
    description = MartorField()
    min_difficulty = models.ForeignKey(NonPlayerCharacterDifficulty, on_delete=models.CASCADE, verbose_name="diff. minimum")
    common = models.BooleanField("commune", default=True)
    penalty = models.BooleanField("malus", default=False)
    elite = models.BooleanField("élite", default=False)
    human_allowed = models.BooleanField("compatible humain", default=False)
    anathema_allowed = models.BooleanField("compatible anathème", default=False)
    ether_allowed = models.BooleanField("compatible éther", default=False)
    void_allowed = models.BooleanField("compatible vide", default=False)
    precision = models.CharField("précision", max_length=128, blank=True)
    slug = models.SlugField(blank=True)
    types = models.ManyToManyField(NonPlayerCharacterType, related_name="capacities")
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.name
        
    def get_absolute_url(self):
        return reverse('gm:npc_capacity_detail', args=[self.slug])
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
    
    def get_absolute_url(self):
        return reverse('gm:npc_capacity_detail', args=[self.slug])