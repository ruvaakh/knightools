from django.db import models
from django.urls import reverse

from martor.models import MartorField
import gettext
import bleach
from markdown import markdown

from .character import Characteristic, Division, ArmourAbility, ArmourEvolution, NonPlayerCharacter, Source, MechaArmour, MechaArmourConfiguration
from .faq import Question

class Activation(models.Model):
    name = models.CharField("nom", max_length=256)
    def __str__(self):
        return self.name

class Effect(models.Model):
    name = models.CharField("nom", max_length=256)
    description = MartorField()
    example = models.TextField("exemple",blank=True)
    short_description = models.TextField(default='')
    source = models.ForeignKey(Source, on_delete=models.CASCADE, related_name="effects")
    faq_corrected = models.BooleanField("Corrigé par FAQ", default=False)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    slug = models.SlugField(null=True, blank=True, unique=True)
    characteristic_damage_bonus = models.ForeignKey(Characteristic, on_delete=models.PROTECT, verbose_name='Carac. bonus dégâts', null=True, blank=True)
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
    
    def origin(self):
        if self.faq_corrected:
            return '%s (corrigé par FAQ)' % self.source.name
        else:
            return self.source.name
            
    def get_absolute_url(self):
        return reverse('gm:effect_detail', args=[self.slug])
        
    class Meta:
        ordering = ['name']

class ModuleCategory(models.Model):
    name = models.CharField("nom", max_length=64)
    short_name = models.CharField("nom", max_length=8, blank=True, default='')
    def __str__(self):
        return self.name

class WeaponCategory(models.Model):
    name = models.CharField("nom", max_length=64)
    def __str__(self):
        return self.name

    def get_src(self):
        if self.id == 1:
            return "army-knife"
        else:
            return "gun"

class Rarity(models.Model):
    name = models.CharField("nom", max_length=32)
    unlocked = models.PositiveSmallIntegerField("condition d'accès")
    def __str__(self):
        return self.name

class Reach(models.Model):
    name = models.CharField("nom", max_length=32)
    short_description = models.TextField("description courte", max_length=32, blank=True, null=True)
    description = models.TextField()
    def __str__(self):
        return self.name

class TitanReach(models.Model):
    name = models.CharField("nom", max_length=32)
    short_description = models.TextField("description courte", max_length=32, blank=True, null=True)
    description = models.TextField()
    def __str__(self):
        return self.name

class Enhancement(models.Model):
    name = models.CharField("nom", max_length=64)
    description = MartorField(blank=True)
    cost = models.PositiveSmallIntegerField("prix")
    effect_desc = models.CharField("Description effet (DEPRECATED)", max_length=512, blank=True)
    restriction = models.CharField(max_length=512)
    category = models.ForeignKey(WeaponCategory,on_delete=models.PROTECT,verbose_name="catégorie")
    group = models.CharField(max_length=2,choices=(
        ('CS', 'Structurelle'),
        ('CO', 'Ornementale')
    ), blank=True, default='')
    source = models.ForeignKey(Source, on_delete=models.CASCADE, related_name="enhancements")
    faq_corrected = models.BooleanField("Corrigé par FAQ", default=False)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    slug = models.SlugField(null=True, blank=True, unique=True)
    
    def __str__(self):
        if self.group:
            return '[%s] %s - %s' % (self.get_group_display(), self.category.name, self.name)
        
        return '%s - %s' % (self.category.name, self.name)
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
    
    def origin(self):
        if self.faq_corrected:
            return '%s (corrigé par FAQ)' % self.source.name
        else:
            return self.source.name
            
    def get_absolute_url(self):
        return reverse('gm:enhancement_detail', args=[self.slug])
    
    class Meta:
        ordering = ['category', 'group', 'name']

class Module(models.Model):
    name = models.CharField("nom", max_length=64)
    gear = models.CharField("équipement", max_length=256, blank=True)
    category = models.ForeignKey(ModuleCategory, on_delete=models.PROTECT, verbose_name="catégorie", related_name="modules")
    source = models.ForeignKey(Source, on_delete=models.CASCADE, related_name="modules")
    faq_corrected = models.BooleanField("Corrigé par FAQ", default=False)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    slug = models.SlugField(null=True, blank=True, unique=True)
    enhancements = models.ManyToManyField(
        Enhancement,
        verbose_name="améliorations",
        related_name="modules",
        blank=True,
    )
    
    def __str__(self):
        return self.name
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
    
    def origin(self):
        if self.faq_corrected:
            return '%s (corrigé par FAQ)' % self.source.name
        else:
            return self.source.name
            
    def get_absolute_url(self):
        return reverse('gm:module_detail', args=[self.slug])

class ModuleLevel(models.Model):
    module = models.ForeignKey(Module,on_delete=models.PROTECT, related_name="levels")
    level = models.PositiveSmallIntegerField("niveau")
    description = MartorField()
    rarity = models.ForeignKey(Rarity,on_delete=models.PROTECT,verbose_name="rareté")
    cost = models.PositiveSmallIntegerField("prix")
    activation = models.ForeignKey(Activation,on_delete=models.PROTECT, blank=True, null=True)
    duration = models.CharField("durée", max_length=128)
    damage_dice = models.SmallIntegerField("dés dégât")
    damage_bonus = models.SmallIntegerField("dégât bonus")
    characteristic_damage_bonus = models.ForeignKey(Characteristic, on_delete=models.PROTECT, verbose_name='Carac. bonus dégâts', null=True, blank=True)
    violence_dice = models.SmallIntegerField("dés violence")
    violence_bonus = models.SmallIntegerField("violence bonus")
    reach = models.ForeignKey(Reach,on_delete=models.PROTECT,verbose_name="portée", blank=True, null=True)
    energy = models.SmallIntegerField("énergie")
    npcs = models.ManyToManyField(NonPlayerCharacter, related_name="modules_source", blank=True)
    
    def __str__(self):
        if self.module.levels.count() > 1:
            return self.module.name + ' niv.' + str(self.level)
        else:
            return self.module.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
        
    def get_damage(self):
        val = ''
        if self.damage_dice > 0:
            val = str(self.damage_dice) + 'D6 (' + str(self.damage_dice * 3) + ')'
        if self.damage_bonus > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.damage_bonus)
        
        if self.characteristic_damage_bonus:
            if val != '':
                val = val + ' + '
            
            val = val + self.characteristic_damage_bonus.name
        
        for fx in self.effects.all():
            if fx.choice_number == 0 and fx.effect and fx.effect.characteristic_damage_bonus:
                if val != '':
                    val = val + ' + '
                
                val = "%s %s (par %s)" % (val, fx.effect.characteristic_damage_bonus.name, fx.effect.name)
        
        return val
    
    def get_violence(self):
        val = ''
        if self.violence_dice > 0:
            val = str(self.violence_dice) + 'D6 (' + str(self.violence_dice * 3) + ')'
        if self.violence_bonus > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.violence_bonus)
        return val
        
    class Meta:
        unique_together = ("module", "level")

class ModuleLevelEffect(models.Model):
    module_level = models.ForeignKey(ModuleLevel,on_delete=models.CASCADE, related_name="effects")
    choice_number = models.PositiveSmallIntegerField("choix")
    effect = models.ForeignKey(Effect,on_delete=models.PROTECT,verbose_name="effet", related_name="moduleleveleffects", blank=True, null=True)
    effect_level = models.SmallIntegerField("niv. effet")
    damage = models.SmallIntegerField("dégâts")
    violence = models.SmallIntegerField()
    forced_by_previous_level = models.SmallIntegerField("forcé par niveau précédent")
    effect_condition = models.CharField("condition", max_length=128, blank=True, default='')
    
    def __str__(self):
        return str(self.module_level) + self.get_title()
    
    def get_title(self):
        damage_text = gettext.gettext('Dégâts')
        violence_text = gettext.gettext('Violence')
        
        if self.damage != 0:
            if self.damage > 0:
                last_part = ('%s +%dD6' % (damage_text, self.damage))
            else:
                last_part = ('%s %dD6' % (damage_text, self.damage))
        elif self.violence != 0:
            if self.violence > 0:
                last_part = ('%s +%dD6' % (violence_text, self.violence))
            else:
                last_part = ('%s %dD6' % (violence_text, self.violence))
        else:
            last_part = ' ' + self.get_name()
        return last_part
    
    def get_name(self):
        if self.effect is not None:
            if self.effect_level > 0:
                return self.effect.name.replace(' X', ' (' + str(self.effect_level) + ')')
            else:
                return self.effect.name
        else:
            return ''
    
    def get_description(self):
        if self.effect is not None:
            if self.effect_level > 0:
                return self.effect.short_description.replace('$X', str(self.effect_level))
            else:
                return self.effect.short_description
        else:
            return ''
    
    class Meta:
        unique_together = ("module_level", "choice_number", "effect", "damage", "violence", "effect_condition")

class ModuleSlot(models.Model):
    module = models.ForeignKey(Module,on_delete=models.PROTECT, related_name="slots")
    head = models.SmallIntegerField("tête")
    left_arm = models.SmallIntegerField("bras g.")
    right_arm = models.SmallIntegerField("bras d.")
    torso = models.SmallIntegerField("torse")
    left_leg = models.SmallIntegerField("jambe g.")
    right_leg = models.SmallIntegerField("jambe d.")
    
    def __str__(self):
        return 'H' + str(self.head) + ' LA' + str(self.left_arm) + ' RA' + str(self.right_arm) + ' T' + str(self.torso) + ' LL' + str(self.left_leg) + ' RL' + str(self.right_leg)
    
    class Meta:
        unique_together = ("module", "head", "left_arm", "right_arm", "torso", "left_leg", "right_leg")

class Overdrive(models.Model):
    characteristic = models.ForeignKey(Characteristic,on_delete=models.PROTECT,verbose_name="caractéristique", related_name="overdrives")
    level = models.PositiveSmallIntegerField("niveau")
    description = MartorField(blank=True)
    rarity = models.ForeignKey(Rarity,on_delete=models.PROTECT,verbose_name="rareté")
    cost = models.PositiveSmallIntegerField("prix")
    slug = models.SlugField(null=True, blank=True, unique=True)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.full_name()
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
    
    def full_name(self):
        return self.characteristic.name + ' niv. ' + str(self.level)
            
    def get_absolute_url(self):
        return reverse('gm:overdrive_detail', args=[self.slug])
    
    class Meta:
        unique_together = ("characteristic", "level")

class EnhancementEffect(models.Model):
    enhancement = models.ForeignKey(Enhancement, on_delete=models.CASCADE, verbose_name="amélioration", related_name="effects")
    effect = models.ForeignKey(Effect, on_delete=models.PROTECT, verbose_name="effet", blank=True, null=True, related_name="enhancements")
    effect_level = models.SmallIntegerField("niv. effet")
    damage_dice = models.SmallIntegerField("dés dégât",default=0)
    violence_dice = models.SmallIntegerField("dés violence",default=0)
    effect_condition = models.CharField("condition", max_length=128, blank=True)
    def __str__(self):
        return self.enhancement.name + ' ' + self.get_name()
    
    def get_name(self):
        if self.effect is not None:
            if self.effect_level != 0:
                return self.effect.name.replace(' X', ' (' + str(self.effect_level) + ')')
            else:
                return self.effect.name
        else:
            effects = []
            damage_text = gettext.gettext('Dégâts')
            violence_text = gettext.gettext('Violence')
            
            if self.damage_dice > 0:
                effects.append('%s +%dD6' % (damage_text, self.damage_dice))
            elif self.damage_dice < 0:
                effects.append('%s %dD6' % (damage_text, self.damage_dice))
                
            if self.violence_dice > 0:
                effects.append('%s +%dD6' % (violence_text, self.violence_dice))
            elif self.violence_dice > 0:
                effects.append('%s %dD6' % (violence_text, self.violence_dice))
            
            return ', '.join(effects)
    
    def get_description(self):
        if self.effect is not None:
            if self.effect_level > 0:
                return self.effect.short_description.replace('$X', str(self.effect_level))
            elif self.effect_level < 0:
                return self.effect.short_description.replace('+$X', str(self.effect_level)).replace('$X', str(self.effect_level))
            else:
                return self.effect.short_description
        else:
            return ''
    
    class Meta:
        unique_together = ("enhancement", "effect")

class Weapon(models.Model):
    name = models.CharField("nom", max_length=256)
    description = MartorField()
    nicknames = models.CharField("surnoms", max_length=256,blank=True)
    rarity = models.ForeignKey(Rarity,on_delete=models.PROTECT,verbose_name="rareté")
    category = models.ForeignKey(WeaponCategory,on_delete=models.PROTECT,verbose_name="catégorie",related_name="weapons")
    cost = models.PositiveSmallIntegerField("prix")
    enhancements = models.ManyToManyField(Enhancement,verbose_name="améliorations",blank=True)
    source = models.ForeignKey(Source, on_delete=models.CASCADE, related_name="weapons")
    faq_corrected = models.BooleanField("Corrigé par FAQ", default=False)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    slug = models.SlugField(null=True, blank=True, unique=True)
    player_enabled = models.BooleanField("accessible PJ", default=True)
    npc_enabled = models.BooleanField("accessible PNJ", default=True)
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
    
    def origin(self):
        if self.faq_corrected:
            return '%s (corrigé par FAQ)' % self.source.name
        else:
            return self.source.name
            
    def get_absolute_url(self):
        return reverse('gm:weapon_detail', args=[self.slug])

class WeaponAttack(models.Model):
    weapon = models.ForeignKey(Weapon,on_delete=models.CASCADE,verbose_name="arme", related_name="attacks")
    position = models.PositiveSmallIntegerField()
    name = models.CharField("nom", max_length=64)
    damage_dice = models.SmallIntegerField("dés dégât")
    damage_bonus = models.SmallIntegerField("dégât bonus")
    characteristic_damage_bonus = models.ForeignKey(Characteristic, on_delete=models.PROTECT, verbose_name="Carac. bonus dégâts", null=True, blank=True)
    violence_dice = models.SmallIntegerField("dés violence")
    violence_bonus = models.SmallIntegerField("violence bonus")
    reach = models.ForeignKey(Reach,on_delete=models.PROTECT,verbose_name="portée")
    energy = models.SmallIntegerField("énergie")
    
    def __str__(self):
        return self.name
    
    def get_damage(self):
        val = ''
        if self.damage_dice > 0:
            val = str(self.damage_dice) + 'D6 (' + str(self.damage_dice * 3) + ')'
        if self.damage_bonus > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.damage_bonus)
            
        if self.characteristic_damage_bonus:
            if val != '':
                val = val + ' + '
            
            val = val + self.characteristic_damage_bonus.name
        
        for fx in self.effects.all():
            if fx.effect and fx.effect.characteristic_damage_bonus:
                if val != '':
                    val = val + ' + '
                
                val = "%s %s (par %s)" % (val, fx.effect.characteristic_damage_bonus.name, fx.effect.name)
        
        return val
    
    def get_violence(self):
        val = ''
        if self.violence_dice > 0:
            val = str(self.violence_dice) + 'D6 (' + str(self.violence_dice * 3) + ')'
        if self.violence_bonus > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.violence_bonus)
        return val
    
    def max_effect_energy(self):
        energy = 0
        
        for effect in self.effects.all():
            if effect.energy > energy:
                energy = effect.energy
        
        return energy
    
    class Meta:
        unique_together = ("weapon", "position")

class WeaponAttackEffect(models.Model):
    weapon_attack = models.ForeignKey(WeaponAttack,on_delete=models.CASCADE,verbose_name="attaque", related_name="effects")
    effect = models.ForeignKey(Effect, on_delete=models.CASCADE, verbose_name="effet", related_name="weaponattackeffects")
    effect_level = models.SmallIntegerField("niv. effet")
    energy = models.SmallIntegerField("énergie")
    effect_condition = models.CharField("condition", max_length=128, blank=True, default='')
    
    def __str__(self):
        return self.get_name()
    
    def get_name(self):
        if self.effect_level > 0:
            return self.effect.name.replace(' X', ' (' + str(self.effect_level) + ')')
        else:
            return self.effect.name
    
    def get_description(self):
        if self.effect_level > 0:
            return self.effect.short_description.replace('$X', str(self.effect_level))
        else:
            return self.effect.short_description
    
    class Meta:
        unique_together = ("weapon_attack", "effect", "effect_level", "energy")
        ordering = ['energy', 'effect__name']

class DivisionModule(models.Model):
    division = models.ForeignKey(Division, on_delete=models.CASCADE, related_name="modules")
    module_level = models.ForeignKey(ModuleLevel, on_delete=models.CASCADE, related_name="divisions")
    forced_choice_effect = models.ForeignKey(Effect, on_delete=models.CASCADE, blank=True, null=True) # Forced with division Ogre: alternate view predator (vue alternative prédatrice).
    
    def __str__(self):
        return self.division.name + ' ' + self.module_level.module.name + ''
    
    class Meta:
        unique_together = ("division", "module_level")

class DivisionWeapon(models.Model):
    division = models.ForeignKey(Division, on_delete=models.CASCADE, related_name="weapons")
    weapon = models.ForeignKey(Weapon, on_delete=models.CASCADE, related_name="divisions")
    
    def __str__(self):
        return self.division.name + ' ' + self.weapon.name
    
    class Meta:
        unique_together = ("division", "weapon")

class ArmourAbilityVariant(models.Model):
    armour_ability = models.ForeignKey(ArmourAbility, on_delete=models.CASCADE, verbose_name="capacité armure", related_name="variants")
    name = models.CharField("nom", max_length=64)
    description = MartorField()
    energy = models.CharField("énergie", max_length=128, blank=True) # yes, this is confusing, but energy in armour abilities are so conditionals...
    hope = models.CharField("espoir", max_length=128, null=True, blank=True) # here again, not an int but a char because it may vary depending on a lot of things...
    activation = models.CharField("activation", max_length=256, blank=True) # yes, also confusing, but again: activation can be very conditionals here.
    duration = models.CharField("durée", max_length=128, blank=True)
    damage_dice = models.SmallIntegerField("dés dégât", blank=True, null=True)
    damage_bonus = models.SmallIntegerField("dégât bonus", blank=True, null=True)
    violence_dice = models.SmallIntegerField("dés violence", blank=True, null=True)
    violence_bonus = models.SmallIntegerField("violence bonus", blank=True, null=True)
    reach = models.ForeignKey(Reach,on_delete=models.PROTECT,verbose_name="portée", blank=True, null=True)
    npc = models.ForeignKey(NonPlayerCharacter, on_delete=models.CASCADE, null=True, blank=True, limit_choices_to={"simplified": False})
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def get_damage(self):
        val = ''
        if self.damage_dice is not None and self.damage_dice > 0:
            val = str(self.damage_dice) + 'D6 (' + str(self.damage_dice * 3) + ')'
        if self.damage_bonus is not None and self.damage_bonus  > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.damage_bonus)
        return val
    
    def get_violence(self):
        val = ''
        if self.violence_dice is not None and self.violence_dice > 0:
            val = str(self.violence_dice) + 'D6 (' + str(self.violence_dice * 3) + ')'
        if self.violence_bonus is not None and self.violence_bonus > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.violence_bonus)
        return val

class NonPlayerCharacterWeapon(models.Model):
    weapon = models.ForeignKey(Weapon, on_delete=models.CASCADE, related_name="npcs", limit_choices_to={'npc_enabled': True})
    npc = models.ForeignKey(NonPlayerCharacter, on_delete=models.CASCADE, related_name="weapons")
    quantity = models.PositiveSmallIntegerField("quantité", default=1)
    
    def __str__(self):
        return "%s - %s (×%d)" % (self.npc.name, self.weapon.name, self.quantity)
    
    class Meta:
        unique_together = ("weapon", "npc")

class NonPlayerCharacterModuleLevel(models.Model):
    module_level = models.ForeignKey(ModuleLevel, on_delete=models.CASCADE, related_name="owning_npcs")
    npc = models.ForeignKey(NonPlayerCharacter, on_delete=models.CASCADE, related_name="modules")
    
    def __str__(self):
        return "%s %s (niv. %d)" % (self.npc.name, self.module_level.module.name, self.module_level.level)
    
    class Meta:
        unique_together = ("module_level", "npc")

class HeroicSkillCategory(models.Model):
    name = models.CharField("nom", max_length=64)
    def __str__(self):
        return self.name

class HeroicSkill(models.Model):
    name = models.CharField("nom", max_length=64)
    description = MartorField()
    xp_cost = models.PositiveSmallIntegerField("acquisition (XP)")
    category = models.ForeignKey(HeroicSkillCategory, on_delete=models.CASCADE, related_name="skills")
    activation = models.ForeignKey(Activation,on_delete=models.PROTECT)
    duration = models.CharField("durée", max_length=128)
    heroic_cost = models.PositiveSmallIntegerField("coût (héroïsme)")
    source = models.ForeignKey(Source, on_delete=models.CASCADE, related_name="heroicskills")
    faq_corrected = models.BooleanField("Corrigé par FAQ", default=False)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    slug = models.SlugField(unique=True)
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
    
    def origin(self):
        if self.faq_corrected:
            return '%s (corrigé par FAQ)' % self.source.name
        else:
            return self.source.name
            
    def get_absolute_url(self):
        return reverse('gm:skill_detail', args=[self.slug])

class Vehicle(models.Model):
    name = models.CharField("nom", max_length=64)
    description = MartorField(blank=True)
    armour_points = models.PositiveSmallIntegerField("points d'armure (pa)", default=0)
    energy_points = models.PositiveSmallIntegerField("points d'énergie (pe)", default=0)
    force_field = models.PositiveSmallIntegerField("champ de force (cdf)",default=0)
    maneuverability = models.SmallIntegerField("manœuvrabilité", default=0)
    speed = models.TextField("vitesse", max_length=64)
    crew =  models.TextField("équipage", max_length=64)
    weaponry = MartorField(blank=True)
    slug = models.SlugField(null=True, blank=True, unique=True)
    faq_text = MartorField(blank=True, null=True)
    questions = models.ManyToManyField(
        Question,
        verbose_name="faq",
        blank=True,
    )
    
    def __str__(self):
        return self.name
    
    def crew_order(self):
        for elem in self.crew.split():
            if elem.isnumeric():
                return int(elem)
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
    
    def weaponry_text_only(self):
        return bleach.clean(markdown(self.weaponry), tags=[], strip=True)
    
    def faq_text_text_only(self):
        if self.faq_text:
            return bleach.clean(markdown(self.faq_text), tags=[], strip=True)
        else:
            return ''
            
    def get_absolute_url(self):
        return reverse('gm:vehicle_detail', args=[self.slug])

class ModuleVehicle(models.Model):
    module = models.ForeignKey(Module, on_delete=models.CASCADE, related_name='vehicles')
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE, related_name='modules')

class MechaArmourAction(models.Model):
    name = models.CharField("nom", max_length=64)
    description = MartorField()
    activation = models.CharField("activation", max_length=256, blank=True) # see armourabilityvariant activation for explanation of the non-ForeignKey.
    core = models.CharField("noyaux", max_length=128, blank=True)
    duration = models.CharField("durée", max_length=256, blank=True)
    reach = models.ForeignKey(TitanReach,on_delete=models.PROTECT, verbose_name="portée", blank=True, null=True)
    damage_dice = models.SmallIntegerField("dés dégât", blank=True, null=True)
    damage_bonus = models.SmallIntegerField("dégât bonus", blank=True, null=True)
    violence_dice = models.SmallIntegerField("dés violence", blank=True, null=True)
    violence_bonus = models.SmallIntegerField("violence bonus", blank=True, null=True)
    effects = MartorField(blank=True) # some fulltext effect exists, so we'll use the power of markdown to link effects when needed...
    
    def __str__(self):
        return self.name
    
    def description_text_only(self):
        return bleach.clean(markdown(self.description), tags=[], strip=True)
        
    def effects_text_only(self):
        return bleach.clean(markdown(self.effects), tags=[], strip=True)
        
    def get_damage(self):
        val = ''
        if self.damage_dice is not None and self.damage_dice > 0:
            val = str(self.damage_dice) + 'D6 (' + str(self.damage_dice * 3) + ')'
        if self.damage_bonus is not None and self.damage_bonus  > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.damage_bonus)
        return val
    
    def get_violence(self):
        val = ''
        if self.violence_dice is not None and self.violence_dice > 0:
            val = str(self.violence_dice) + 'D6 (' + str(self.violence_dice * 3) + ')'
        if self.violence_bonus is not None and self.violence_bonus > 0:
            if val != '':
                val = val + ' + '
            
            val = val + str(self.violence_bonus)
        return val
    
class MechaArmourActionCommon(MechaArmourAction):
    mecha = models.ForeignKey(MechaArmour, on_delete=models.CASCADE, verbose_name="mécha-armure", related_name="actions", blank=True, null=True) # Either mecha or configuration must be set, but not both
    
class MechaArmourActionConfiguration(MechaArmourAction):
    configuration = models.ForeignKey(MechaArmourConfiguration, on_delete=models.CASCADE, related_name="actions", blank=True, null=True) # Either mecha or configuration must be set, but not both
    