from django.db import models
from django.contrib import admin
from martor.widgets import AdminMartorWidget
from mptt.admin import MPTTModelAdmin
from mptt.admin import DraggableMPTTAdmin

from .models.character import GreatDeed, Armour, ArmourAbility, ArmourEvolution, ArmourOverdrive, Characteristic, MajorArcana, NonPlayerCharacter, NonPlayerCharacterAspect, NonPlayerCharacterCharacteristic, Division, DivisionOverdrive, TraumaCategory, Trauma, MechaArmour, MechaArmourConfiguration, NonPlayerCharacterCapacity, Archetype, Crest
from .models.weaponry import Overdrive, ArmourAbilityVariant, Effect, Enhancement, EnhancementEffect, HeroicSkill, HeroicSkillCategory, Module, ModuleLevel, ModuleLevelEffect, ModuleSlot, NonPlayerCharacterModuleLevel, NonPlayerCharacterWeapon, DivisionModule, DivisionWeapon, Weapon, WeaponAttack, WeaponAttackEffect, Vehicle, ModuleVehicle, MechaArmourActionCommon, MechaArmourActionConfiguration, TitanReach
from .models.faq import Category, Question


# Register your models here.

class HeroicSkillAdmin(admin.ModelAdmin):
    list_display = ('name', 'category', 'xp_cost', 'heroic_cost')
    prepopulated_fields = {"slug": ("name",)}
    
class OverdriveAdmin(admin.ModelAdmin):
    list_display = ('characteristic', 'level', 'description')
    prepopulated_fields = {"slug": ("characteristic","level")}
    
class HeroicSkillCategoryAdmin(admin.ModelAdmin):
    list_display = ('name',)
    
class DivisionModuleInline(admin.TabularInline):
    model = DivisionModule
    extra = 1
    
class DivisionWeaponInline(admin.TabularInline):
    model = DivisionWeapon
    extra = 2
    
class DivisionOverdriveInline(admin.TabularInline):
    model = DivisionOverdrive
    extra = 1

class GreatDeedAdmin(admin.ModelAdmin):
    list_display = ('name', 'restriction')

class MajorArcanaAdmin(admin.ModelAdmin):
    list_display = ('name', 'roman_number', 'ai_letter')

class CrestAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')

class CategoryAdmin(DraggableMPTTAdmin):
    mptt_indent_field = "name"
    list_display = ('tree_actions', 'indented_title',)
    list_display_links = ('indented_title',)

class QuestionAdmin(admin.ModelAdmin):
    list_display = ('question', 'answer', 'gm', 'v1', 'v1_5', 'category')

class ArchetypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')
    
class DivisionAdmin(admin.ModelAdmin):
    list_display = ('name', 'aspect_bonus', 'disadvantage_name', 'motto')
    inlines = [DivisionWeaponInline, DivisionModuleInline, DivisionOverdriveInline]

class ArmourEvolutionStacked(admin.StackedInline):
    model = ArmourEvolution
    extra = 3

class ArmourOverdriveInline(admin.TabularInline):
    model = ArmourOverdrive
    extra = 1

class ArmourAdmin(admin.ModelAdmin):
    list_display = ('name', 'generation')
    inlines = [ArmourOverdriveInline, ArmourEvolutionStacked]

class ArmourAbilityVariantInline(admin.StackedInline):
    model = ArmourAbilityVariant
    extra = 1

class ArmourAbilityAdmin(admin.ModelAdmin):
    list_display = ('armour', 'name')
    inlines = [ArmourAbilityVariantInline]

class CharacteristicAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'aspect', 'overdrive_global_description')
    
class EffectAdmin(admin.ModelAdmin):
    list_display = ('name', 'origin', 'description', 'short_description')
    search_fields = ['name', 'description', 'source__name']

class ModuleLevelEffectInline(admin.TabularInline):
    model = ModuleLevelEffect
    extra = 1
    
class ModuleSlotInline(admin.TabularInline):
    model = ModuleSlot
    extra = 1

class ModuleAdmin(admin.ModelAdmin):
    list_display = ('name', 'gear')
    search_fields = ['name']
    inlines = [ModuleSlotInline]
    prepopulated_fields = {"slug": ("name",)}

class ModuleLevelAdmin(admin.ModelAdmin):
    list_display = ('module', 'level', 'rarity')
    search_fields = ['module__name', 'module__category__name']
    inlines = [ModuleLevelEffectInline]

class EnhancementEffectInline(admin.TabularInline):
    model = EnhancementEffect
    extra = 1
    
class WeaponAttackEffectInline(admin.TabularInline):
    model = WeaponAttackEffect
    extra = 1

class WeaponAttackAdmin(admin.ModelAdmin):
    list_display = ('weapon', 'name')
    model = WeaponAttack
    inlines = [WeaponAttackEffectInline]

class WeaponAdmin(admin.ModelAdmin):
    list_display = ('name', 'rarity', 'category')
    search_fields = ['name']
    prepopulated_fields = {"slug": ("name",)}
    
    
class EnhancementAdmin(admin.ModelAdmin):
    list_display = ('name', 'restriction', 'group', 'category')
    search_fields = ['restriction']
    ordering = ['category__name', 'name']
    inlines = [EnhancementEffectInline]
    
class ModuleVehicleInline(admin.TabularInline):
    model = ModuleVehicle
    extra = 1

class VehicleAdmin(admin.ModelAdmin):
    list_display = ('name', 'weaponry')
    search_fields = ['name']
    ordering = ['name']
    prepopulated_fields = {"slug": ("name",)}
    inlines = [ModuleVehicleInline]
    

class NonPlayerCharacterAspectInline(admin.TabularInline):
    model = NonPlayerCharacterAspect
    extra = 5
    
class NonPlayerCharacterCharacteristicInline(admin.TabularInline):
    model = NonPlayerCharacterCharacteristic
    extra = 5
    
class NonPlayerCharacterModuleLevelInline(admin.TabularInline):
    model = NonPlayerCharacterModuleLevel
    extra = 1
    
class NonPlayerCharacterWeaponInline(admin.TabularInline):
    model = NonPlayerCharacterWeapon
    extra = 1
    
class NonPlayerCharacterAdmin(admin.ModelAdmin):
    list_display = ('name', 'defence', 'reaction', 'initiative')
    inlines = [NonPlayerCharacterAspectInline, NonPlayerCharacterCharacteristicInline, NonPlayerCharacterModuleLevelInline, NonPlayerCharacterWeaponInline]

class TraumaCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'hop_recovered')
    search_fields = ['name', 'description']
    
class TraumaAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'category')
    search_fields = ['name', 'description', 'category']

class MechaArmourActionCommonStacked(admin.StackedInline):
    model = MechaArmourActionCommon
    extra = 2

class MechaArmourActionConfigurationStacked(admin.StackedInline):
    model = MechaArmourActionConfiguration
    extra = 2
    
class MechaArmourConfigurationAdmin(admin.ModelAdmin):
    list_display = ('name', 'mecha')
    inlines = [MechaArmourActionConfigurationStacked]

class MechaArmourAdmin(admin.ModelAdmin):
    list_display = ('name',)
    inlines = [MechaArmourActionCommonStacked]
    prepopulated_fields = {"slug": ("name",)}
    
class TitanReachAdmin(admin.ModelAdmin):
    list_display = ('name',)
    
class NonPlayerCharacterCapacityAdmin(admin.ModelAdmin):
    list_display = ('name', 'precision', 'description')
    search_fields = ['name', 'description']
    prepopulated_fields = {"slug": ("name",)}
    
    
admin.site.register(Effect, EffectAdmin)
admin.site.register(Weapon, WeaponAdmin)
admin.site.register(Enhancement, EnhancementAdmin)
admin.site.register(Module, ModuleAdmin)
admin.site.register(ModuleLevel, ModuleLevelAdmin)
admin.site.register(WeaponAttack, WeaponAttackAdmin)
admin.site.register(Armour, ArmourAdmin)
admin.site.register(ArmourAbility, ArmourAbilityAdmin)
admin.site.register(NonPlayerCharacter, NonPlayerCharacterAdmin)
admin.site.register(MajorArcana, MajorArcanaAdmin)
admin.site.register(GreatDeed, GreatDeedAdmin)
admin.site.register(HeroicSkill, HeroicSkillAdmin)
admin.site.register(HeroicSkillCategory, HeroicSkillCategoryAdmin)
admin.site.register(Overdrive, OverdriveAdmin)
admin.site.register(Vehicle, VehicleAdmin)
admin.site.register(TraumaCategory, TraumaCategoryAdmin)
admin.site.register(Trauma, TraumaAdmin)
admin.site.register(MechaArmour, MechaArmourAdmin)
admin.site.register(MechaArmourConfiguration, MechaArmourConfigurationAdmin)
admin.site.register(TitanReach, TitanReachAdmin)
admin.site.register(NonPlayerCharacterCapacity, NonPlayerCharacterCapacityAdmin)
admin.site.register(Division, DivisionAdmin)
admin.site.register(Archetype, ArchetypeAdmin)
admin.site.register(Crest, CrestAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Question, QuestionAdmin)


# admin.site.register(Characteristic, CharacteristicAdmin)
# admin.site.register(WeaponAttack)
# admin.site.register(WeaponAttackEffect)
