from django_elasticsearch_dsl import fields
from django_elasticsearch_dsl import Document
from django_elasticsearch_dsl.registries import registry

from django.db.models import Q

from gm.models.weaponry import ArmourAbilityVariant, Effect, Enhancement, HeroicSkill, Module, Overdrive, Weapon
from gm.models.character import GreatDeed, Archetype, Armour, ArmourAbility, ArmourEvolution, Crest, MajorArcana, Division, Trauma, MechaArmour, NonPlayerCharacterCapacity
# New import line just for related_models, for readability
from gm.models.weaponry import WeaponAttack, WeaponAttackEffect, ModuleLevel, ModuleLevelEffect, EnhancementEffect, ArmourAbilityVariant, MechaArmourActionCommon, MechaArmourActionConfiguration
from gm.models.character import Characteristic, Aspect, ArmourAbility, ArmourEvolution, ArmourOverdrive, MechaArmourConfiguration

@registry.register_document
class WeaponDocument(Document):
    class Index:
        name = "weapons"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    description = fields.TextField(attr="description_text_only")
    faq_text = fields.TextField(attr="faq_text_text_only")
    
    attacks = fields.NestedField(properties={
        "name": fields.TextField(),
        "effects": fields.NestedField(properties={
            "name": fields.TextField(attr="get_name"),
        }),
    })
    
    class Django:
        model = Weapon
        related_models = [WeaponAttack, WeaponAttackEffect, Effect]
        fields = [
            "name",
            "nicknames",
            "slug",
        ]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, WeaponAttack):
            return related_instance.weapon
        elif isinstance(related_instance, WeaponAttackEffect):
            return related_instance.weapon_attack.weapon
        elif isinstance(related_instance, Effect):
            return Weapon.objects.filter(attacks__effects__effect__pk=related_instance.pk).all() # don't know if there is a better way...

@registry.register_document
class ModuleDocument(Document):
    class Index:
        name = "modules"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
        
    faq_text = fields.TextField(attr="faq_text_text_only")
    
    levels = fields.NestedField(properties={
        "description": fields.TextField(attr="description_text_only"),
        "effects": fields.NestedField(properties={
            "name": fields.TextField(attr="get_title"),
        }),
    })
    
    class Django:
        model = Module
        related_models = [ModuleLevel, ModuleLevelEffect, Effect]
        fields = [
            "name",
            "slug",
        ]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, ModuleLevel):
            return related_instance.module
        elif isinstance(related_instance, ModuleLevelEffect):
            return related_instance.module_level.module
        elif isinstance(related_instance, Effect):
            return Module.objects.filter(levels__effects__effect__pk=related_instance.pk).all() # don't know if there is a better way...

@registry.register_document
class EffectDocument(Document):
    class Index:
        name = "effects"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    description = fields.TextField(attr="description_text_only")
    faq_text = fields.TextField(attr="faq_text_text_only")
    
    class Django:
        model = Effect
        fields = [
            "name",
            "short_description",
            "slug",
        ]

@registry.register_document
class EnhancementDocument(Document):
    class Index:
        name = "enhancements"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
        
    description = fields.TextField(attr="description_text_only")
    faq_text = fields.TextField(attr="faq_text_text_only")
    effects = fields.NestedField(properties={
        "name": fields.TextField(attr="get_name"),
    })
    
    
    class Django:
        model = Enhancement
        related_models = [EnhancementEffect, Effect]
        fields = [
            "name",
            "restriction",
            "slug",
        ]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, EnhancementEffect):
            return related_instance.enhancement
        elif isinstance(related_instance, Effect):
            return Enhancement.objects.filter(effects__effect__pk=related_instance.pk).all() # don't know if there is a better way...

@registry.register_document
class OverdriveDocument(Document):
    class Index:
        name = "overdrives"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
        
    description = fields.TextField(attr="description_text_only")
    faq_text = fields.TextField(attr="faq_text_text_only")
    name = fields.TextField(attr="full_name")
    
    characteristic = fields.ObjectField(properties={
        "overdrive_global_description": fields.TextField(),
        "aspect": fields.ObjectField(properties={
            "name": fields.TextField(),
        }),
    })
    
    class Django:
        model = Overdrive
        related_models = [Characteristic, Aspect]
        fields= [
            "slug",
        ]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, Characteristic):
            return related_instance.overdrives.all()
        elif isinstance(related_instance, Aspect):
            return Overdrive.objects.filter(characteristic__aspect__pk=related_instance.pk).all() # don't know if there is a better way...

@registry.register_document
class ArchetypeDocument(Document):
    class Index:
        name = "archetypes"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
        
    faq_text = fields.TextField(attr="faq_text_text_only")
    
    characteristic_bonus_1 = fields.ObjectField(properties={
        "name": fields.TextField(),
    })
    characteristic_bonus_2 = fields.ObjectField(properties={
        "name": fields.TextField(),
    })
    characteristic_bonus_3 = fields.ObjectField(properties={
        "name": fields.TextField(),
    })
    
    class Django:
        model = Archetype
        related_models = [Characteristic]
        fields = [
            "name",
            "description",
            "slug",
        ]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, Characteristic):
            return Archetype.objects.filter(Q(characteristic_bonus_1__pk=related_instance.pk) | Q(characteristic_bonus_2__pk=related_instance.pk) | Q(characteristic_bonus_3__pk=related_instance.pk)).all(),

@registry.register_document
class MajorArcanaDocument(Document):
    class Index:
        name = "arcanas"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
        
    destiny_effect = fields.TextField(attr="destiny_effect_text_only")
    advantage = fields.TextField(attr="advantage_text_only")
    disadvantage = fields.TextField(attr="disadvantage_text_only")
    ai_advantage = fields.TextField(attr="ai_advantage_text_only")
    ai_disadvantage = fields.TextField(attr="ai_disadvantage_text_only")
    faq_text = fields.TextField(attr="faq_text_text_only")
    forced_aspect = fields.ObjectField(properties={
        "name": fields.TextField(),
    })
    
    class Django:
        model = MajorArcana
        related_models = [Aspect]
        fields = [
            "name",
            "description",
            "past",
            "advantage_name",
            "disadvantage_name",
            "ai_description",
            "ai_advantage_name",
            "ai_disadvantage_name",
            "ai_behavior",
            "slug",
        ]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, Aspect):
            return related_instance.majorarcana_set.all()

@registry.register_document
class GreatDeedDocument(Document):
    class Index:
        name = "great_deeds"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
        
    restriction = fields.TextField(attr="restriction_text_only")
    faq_text = fields.TextField(attr="faq_text_text_only")
    aspect_1 = fields.ObjectField(properties={
        "name": fields.TextField(),
    })
    aspect_2 = fields.ObjectField(properties={
        "name": fields.TextField(),
    })
    
    class Django:
        model = GreatDeed
        related_models = [Aspect]
        fields = [
            "name",
            "description",
            "slug",
        ]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, Characteristic):
            return GreatDeed.objects.filter(Q(aspect_1__pk=related_instance.pk) | Q(aspect_2__pk=related_instance.pk)).all(),

@registry.register_document
class CrestDocument(Document):
    class Index:
        name = "crests"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    faq_text = fields.TextField(attr="faq_text_text_only")
    
    class Django:
        model = Crest
        fields = [
            "name",
            "description",
            "vow",
            "slug",
        ]

@registry.register_document
class ArmourDocument(Document):
    class Index:
        name = "armours"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    additional_notes = fields.TextField(attr="additional_notes_text_only")
    faq_text = fields.TextField(attr="faq_text_text_only")
    overdrives = fields.NestedField(properties={
        "characteristic": fields.ObjectField(properties={
            "name": fields.TextField(),
        }),
    })
    evolutions = fields.NestedField(properties={
        "description": fields.TextField(attr="description_text_only"),
    })
    abilities = fields.NestedField(properties={
        "name": fields.TextField(),
        "description": fields.TextField(attr="description_text_only"),
        "variants" : fields.NestedField(properties={
            "name": fields.TextField(),
            "description": fields.TextField(attr="description_text_only"),
        }),
    })
    
    class Django:
        model = Armour
        related_models = [ArmourAbility, ArmourAbilityVariant, ArmourEvolution, ArmourOverdrive, Characteristic]
        fields = [
            "id", # used to filter allowed armours
            "name",
            "background_description",
            "technical_description",
            "slug",
        ]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, ArmourAbility):
            return related_instance.armour
        if isinstance(related_instance, ArmourAbilityVariant):
            return related_instance.armour_ability.armour
        if isinstance(related_instance, ArmourEvolution):
            return related_instance.armour
        if isinstance(related_instance, ArmourOverdrive):
            return related_instance.armour
        if isinstance(related_instance, Characteristic):
            return Armour.objects.filter(overdrives__characteristic__pk=related_instance.pk).all()

@registry.register_document
class DivisionDocument(Document):
    class Index:
        name = "divisions"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    aspect_bonus = fields.ObjectField(properties={
        "name": fields.TextField(),
    })
    disadvantage = fields.TextField(attr="disadvantage_text_only")
    faq_text = fields.TextField(attr="faq_text_text_only")
    
    class Django:
        model = Division
        related_models = [Aspect]
        fields = [
            "name",
            "description",
            "disadvantage_name",
            "slug",
        ]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, Aspect):
            return related_instance.division_set.all()

@registry.register_document
class HeroicSkillDocument(Document):
    class Index:
        name = "heroic_skills"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    description = fields.TextField(attr="description_text_only")
    faq_text = fields.TextField(attr="faq_text_text_only")
    
    class Django:
        model = HeroicSkill
        fields = [
            "name",
            "slug",
        ]

@registry.register_document
class TraumaDocument(Document):
    class Index:
        name = "traumas"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    description = fields.TextField(attr="description_text_only")
    faq_text = fields.TextField(attr="faq_text_text_only")
    
    class Django:
        model = Trauma
        fields = [
            "id", # used to filter allowed traumas
            "name",
            "slug",
        ]

@registry.register_document
class MechaArmourDocument(Document):
    class Index:
        name = "mecha_armours"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    description = fields.TextField(attr="description_text_only")
    faq_text = fields.TextField(attr="faq_text_text_only")
    actions = fields.NestedField(properties={
        "name": fields.TextField(),
        "description": fields.TextField(attr="description_text_only"),
        "effects": fields.TextField(attr="effects_text_only"),
    })
    configurations = fields.NestedField(properties={
        "name": fields.TextField(),
        "description": fields.TextField(attr="description_text_only"),
        "actions" : fields.NestedField(properties={
            "name": fields.TextField(),
            "description": fields.TextField(attr="description_text_only"),
            "effects": fields.TextField(attr="effects_text_only"),
        }),
    })
    
    class Django:
        model = MechaArmour
        related_models = [MechaArmourConfiguration, MechaArmourActionCommon, MechaArmourActionConfiguration]
        fields = [
            "id", # used to filter allowed MechaArmours
            "name",
            "slug",
        ]
    
    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, MechaArmourConfiguration):
            return related_instance.mecha
        if isinstance(related_instance, MechaArmourActionCommon):
            return related_instance.mecha
        if isinstance(related_instance, MechaArmourActionConfiguration):
            return related_instance.configuration.mecha

@registry.register_document
class NonPlayerCharacterCapacityDocument(Document):
    class Index:
        name = "npc_capacities"
        
        settings = {"number_of_shards": 1,
                    "number_of_replicas": 0}
    
    description = fields.TextField(attr="description_text_only")
    faq_text = fields.TextField(attr="faq_text_text_only")
    
    class Django:
        model = NonPlayerCharacterCapacity
        fields = [
            "id", # used to filter allowed traumas
            "name",
            "slug",
        ]
