# Generated by Django 2.1.4 on 2019-02-28 15:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gm', '0033_armorabilityvariant_npc'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='armorevolutioneffect',
            name='armor_evolution',
        ),
        migrations.RemoveField(
            model_name='armorevolutioneffect',
            name='effect',
        ),
        migrations.DeleteModel(
            name='ArmorEvolutionEffect',
        ),
    ]
