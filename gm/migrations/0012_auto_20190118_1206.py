# Generated by Django 2.1.4 on 2019-01-18 12:06

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gm', '0011_auto_20190117_1243'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='moduleleveleffect',
            unique_together={('module_level', 'choice_number', 'effect', 'damage', 'violence', 'effect_condition')},
        ),
    ]
