# Generated by Django 2.2.2 on 2021-02-10 12:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gm', '0078_titanreach_short_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='nonplayercharacter',
            name='slot_head',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='slots tête'),
        ),
        migrations.AddField(
            model_name='nonplayercharacter',
            name='slot_left_arm',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='slots bras g.'),
        ),
        migrations.AddField(
            model_name='nonplayercharacter',
            name='slot_left_leg',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='slots jambe g.'),
        ),
        migrations.AddField(
            model_name='nonplayercharacter',
            name='slot_right_arm',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='slots bras d.'),
        ),
        migrations.AddField(
            model_name='nonplayercharacter',
            name='slot_right_leg',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='slots jambe d.'),
        ),
        migrations.AddField(
            model_name='nonplayercharacter',
            name='slot_torso',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='slots torse'),
        ),
    ]
