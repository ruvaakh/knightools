from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from django.db.models import Q, Min
from django.contrib.auth.mixins import LoginRequiredMixin

from ..models.character import Armour, Division, MajorArcana, GreatDeed, ArmourAbility, ArmourEvolution, ArmourOverdrive, DivisionOverdrive, Crest, Archetype, Trauma, MechaArmour, MechaArmourConfiguration, NonPlayerCharacterCapacity, NonPlayerCharacterType
from ..models.weaponry import Weapon, Effect, Module, Overdrive, Enhancement, HeroicSkill, EnhancementEffect, ModuleLevel, ModuleSlot, DivisionModule, DivisionWeapon, WeaponAttack, Vehicle, ModuleVehicle, MechaArmourActionCommon
from users.models import FavoriteWeapon, FavoriteEffect, FavoriteEnhancement, FavoriteArmour, FavoriteModule, FavoriteMechaArmour

class WeaponDetailView(generic.DetailView):
    model = Weapon
    template_name = 'gm/details/weapon.html'
    
    def get_object(self):
        weapon = super(WeaponDetailView, self).get_object()
        weapon.attacks_list = WeaponAttack.objects.filter(weapon=weapon.id).prefetch_related('effects', 'effects__effect').select_related('reach').order_by('position').all()
        weapon.enhancements_list = Enhancement.objects.filter(weapon=weapon.id).prefetch_related('effects', 'effects__effect').all()
        weapon.count_attacks = len(weapon.attacks_list)
        weapon.count_enhancements = len(weapon.enhancements_list)
        weapon.is_favorite = False
        if self.request.user.is_authenticated and len(FavoriteWeapon.objects.filter(user=self.request.user, weapon=weapon)):
            weapon.is_favorite = True
        return weapon


class EffectDetailView(generic.DetailView):
    model = Effect
    template_name = 'gm/details/effect.html'
    
    def get_object(self):
        effect = super(EffectDetailView, self).get_object()
        effect.weapons_list = Weapon.objects.filter(pk__in=effect.weaponattackeffects.values_list('weapon_attack__weapon__pk', flat=True)).order_by('rarity', 'name').all()
        effect.modules_list = (Module
            .objects
            .filter(
                pk__in=effect.moduleleveleffects.values_list('module_level__module__pk', flat=True)
            )
            .annotate(
                min_rarity=Min(
                    'levels__rarity__id',
                    filter=Q(levels__pk__in=effect.moduleleveleffects.values_list('module_level__pk', flat=True))
                ),
            )
            .select_related('category')
            .order_by('min_rarity', 'name')
            .all()
        )
        effect.enhancements_list = Enhancement.objects.filter(pk__in=effect.enhancements.values_list('enhancement__pk', flat=True)).all()
        effect.count_weapons = len(effect.weapons_list)
        effect.count_modules = len(effect.modules_list)
        effect.count_enhancements = len(effect.enhancements_list)
        effect.is_favorite = False
        if self.request.user.is_authenticated and len(FavoriteEffect.objects.filter(user=self.request.user, effect=effect)):
            effect.is_favorite = True
        return effect


class ModuleDetailView(generic.DetailView):
    model = Module
    template_name = 'gm/details/module.html'
    
    def get_object(self):
        module = super(ModuleDetailView, self).get_object()
        module.slots_list = ModuleSlot.objects.filter(module=module.id).all()
        module.levels_list = ModuleLevel.objects.filter(module=module.id).select_related('reach', 'activation', 'rarity').prefetch_related('effects', 'effects__effect', 'npcs', 'npcs__characteristics', 'npcs__characteristics__characteristic').all()
        module.vehicles_list = ModuleVehicle.objects.filter(module=module.id).select_related('vehicle').all()
        module.enhancements_list = Enhancement.objects.filter(modules=module.id).prefetch_related('effects', 'effects__effect').all()
        module.count_slots = len(module.slots_list)
        module.count_enhancements = len(module.enhancements_list)
        module.is_favorite = False
        if self.request.user.is_authenticated and len(FavoriteModule.objects.filter(user=self.request.user, module=module)):
            module.is_favorite = True
        return module


class OverdriveDetailView(generic.DetailView):
    model = Overdrive
    template_name = 'gm/details/overdrive.html'
    

class EnhancementDetailView(generic.DetailView):
    model = Enhancement
    template_name = 'gm/details/enhancement.html'
    
    def get_object(self):
        enhancement = super(EnhancementDetailView, self).get_object()
        enhancement.effects_list = EnhancementEffect.objects.filter(enhancement=enhancement.id).select_related('effect').all()
        enhancement.count_effects = len(enhancement.effects_list)
        enhancement.is_favorite = False
        if self.request.user.is_authenticated and len(FavoriteEnhancement.objects.filter(user=self.request.user, enhancement=enhancement)):
            enhancement.is_favorite = True
        return enhancement


class ArmourClassifiedView(generic.View):
    def get(self, request):
        return render(request, 'gm/details/armour_classified.html')

class ArmourDetailView(generic.DetailView):
    model = Armour
    template_name = 'gm/details/armour.html'
    
    def get_object(self):
        armour = super(ArmourDetailView, self).get_object()
        armour.overdrives_list = ArmourOverdrive.objects.filter(armour=armour.id).prefetch_related('characteristic').all()
        armour.evolutions_list = ArmourEvolution.objects.filter(armour=armour.id).all()
        armour.abilities_list = ArmourAbility.objects.filter(armour=armour.id).prefetch_related('variants', 'variants__reach', 'variants__npc', 'variants__npc__aspects', 'variants__npc__aspects__aspect').all()
        armour.is_favorite = False
        if self.request.user.is_authenticated and len(FavoriteArmour.objects.filter(user=self.request.user, armour=armour)):
            armour.is_favorite = True
        
        armour.is_allowed = True
        if armour.password is not None:
            if not self.request.user.is_authenticated:
                armour.is_allowed = False
            elif not self.request.user.game_master and armour not in self.request.user.unlocked_armours.all():
                armour.is_allowed = False
        
        return armour


class DivisionDetailView(generic.DetailView):
    model = Division
    template_name = 'gm/details/division.html'
    
    def get_object(self):
        division = super(DivisionDetailView, self).get_object()
        division.overdrives_list = DivisionOverdrive.objects.filter(division=division.id).prefetch_related('characteristic').all()
        division.modules_list = DivisionModule.objects.filter(division=division.id).select_related('module_level', 'module_level__module', 'forced_choice_effect').all()
        division.weapons_list = DivisionWeapon.objects.filter(division=division.id).select_related('weapon', 'weapon__category').all()
        return division
    

class ArcanaDetailView(generic.DetailView):
    model = MajorArcana
    template_name = 'gm/details/arcana.html'
    context_object_name = 'arcana'
    
    # def get_object(self):
    #     division = super(DivisionDetailView, self).get_object()
    #     division.overdrives_list = DivisionOverdrive.objects.filter(division=division.id).prefetch_related('characteristic').all()
    #     division.modules_list = DivisionModule.objects.filter(division=division.id).select_related('module_level', 'module_level__module', 'forced_choice_effect').all()
    #     division.weapons_list = DivisionWeapon.objects.filter(division=division.id).select_related('weapon', 'weapon__category').all()
    #     return division
    

class GreatDeedDetailView(generic.DetailView):
    model = GreatDeed
    template_name = 'gm/details/great_deed.html'
    context_object_name = 'great_deed'
    
class CrestDetailView(generic.DetailView):
    model = Crest
    template_name = 'gm/details/crest.html'
    context_object_name = 'crest'
    
class ArchetypeDetailView(generic.DetailView):
    model = Archetype
    template_name = 'gm/details/archetype.html'
    context_object_name = 'archetype'
    

class HeroicSkillDetailView(generic.DetailView):
    model = HeroicSkill
    template_name = 'gm/details/skill.html'
    context_object_name = 'skill'
    

class VehicleDetailView(generic.DetailView):
    model = Vehicle
    template_name = 'gm/details/vehicle.html'
    context_object_name = 'vehicle'

class TraumaDetailView(LoginRequiredMixin, generic.DetailView):
    model = Trauma
    template_name = 'gm/details/trauma.html'
    
    def get_object(self):
        trauma = super(TraumaDetailView, self).get_object()
        
        trauma.is_allowed = True
        if trauma.password is not None:
            if not self.request.user.is_authenticated:
                trauma.is_allowed = False
            elif not self.request.user.game_master and trauma not in self.request.user.unlocked_traumas.all():
                trauma.is_allowed = False
        
        return trauma

class MechaArmourClassifiedView(generic.View):
    def get(self, request):
        return render(request, 'gm/details/mecha_classified.html')
        
class MechaArmourDetailView(LoginRequiredMixin, generic.DetailView):
    model = MechaArmour
    template_name = 'gm/details/mecha.html'
    context_object_name = 'mecha'
    
    def get_object(self):
        mecha = super(MechaArmourDetailView, self).get_object()
        mecha.configurations_list = MechaArmourConfiguration.objects.filter(mecha=mecha.id).prefetch_related('actions', 'actions__reach').all()
        mecha.actions_list = MechaArmourActionCommon.objects.filter(mecha=mecha.id).prefetch_related('reach').all()
        mecha.is_favorite = False
        if self.request.user.is_authenticated and len(FavoriteMechaArmour.objects.filter(user=self.request.user, mecha=mecha)):
            mecha.is_favorite = True
        
        mecha.is_allowed = True
        if mecha.password is not None:
            if not self.request.user.is_authenticated:
                mecha.is_allowed = False
            elif not self.request.user.game_master and mecha not in self.request.user.unlocked_mechas.all():
                mecha.is_allowed = False
        
        return mecha
    

class NonPlayerCharacterCapacityDetailView(LoginRequiredMixin, generic.DetailView):
    model = NonPlayerCharacterCapacity
    template_name = 'gm/details/npc_capacity.html'
    context_object_name = 'capacity'
    
    def get_context_data(self, **kwargs):
        context = super(NonPlayerCharacterCapacityDetailView, self).get_context_data(**kwargs)
        context['npc_types'] = NonPlayerCharacterType.objects.all()
        return context
    
