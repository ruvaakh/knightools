from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from django.db.models import Min, Max, Count

from ..models.weaponry import Weapon, Module, Overdrive, Enhancement
from ..models.character import Trauma

class WeaponPrintListView(generic.ListView):
    template_name = 'gm/prints/weapon.html'
    context_object_name = 'weapon_list'

    def get_queryset(self):
        """Return list of weapon."""
        return Weapon.objects.filter(player_enabled=True).order_by('category', 'rarity', 'name').select_related('rarity', 'category').prefetch_related('attacks', 'attacks__reach', 'attacks__effects', 'attacks__effects__effect', 'enhancements')


class ModulePrintListView(generic.ListView):
    template_name = 'gm/prints/module.html'
    context_object_name = 'module_list'

    def get_queryset(self):
        """Return list of module."""
        return Module.objects.annotate(min_rarity=Min('levels__rarity__id'), max_level=Max('levels__level')).prefetch_related('levels', 'levels__effects', 'levels__effects__effect', 'levels__rarity', 'levels__activation', 'levels__reach').select_related('category').order_by('category', 'min_rarity', 'name')


class OverdrivePrintListView(generic.ListView):
    template_name = 'gm/prints/overdrive.html'
    context_object_name = 'overdrive_list'

    def get_queryset(self):
        """Return list of overdrive."""
        return Overdrive.objects.order_by('characteristic__aspect__id', 'characteristic__id', 'level').select_related('characteristic', 'characteristic__aspect', 'rarity')
    

class EnhancementPrintListView(generic.ListView):
    template_name = 'gm/prints/enhancement.html'
    context_object_name = 'enhancement_list'

    def get_queryset(self):
        """Return list of enhancement."""
        return Enhancement.objects.order_by('group', 'name').select_related('category')

class TraumaPrintListView(generic.ListView):
    template_name = 'gm/prints/trauma.html'
    context_object_name = 'trauma_list'

    def get_queryset(self):
        """Return list of traumas."""
        return (Trauma
            .objects
            .select_related('category')
            .order_by('category__hop_recovered', 'die_value'))

    