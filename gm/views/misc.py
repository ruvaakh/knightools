from gettext import gettext
from random import sample, choice

from django.http import JsonResponse
from django.shortcuts import render
from django.views import generic
from django.urls import reverse

from ..models.character import GreatDeed, Archetype, Armour, Crest, MajorArcana, Division, Trauma, MechaArmour, NonPlayerCharacterCapacity
from ..models.weaponry import Effect, Enhancement, HeroicSkill, Module, Overdrive, Weapon, Vehicle


class AboutView(generic.View):
    def get(self, request):
        return render(request, 'gm/misc/about.html')
        
class IndexView(generic.View):
    # template_name = 'gm/index.html'
    def get(self, request):
        return render(request, 'gm/misc/index.html')
    
class AutocompleteView(generic.View):
    def get(self, request):
        term = request.GET.get('term', None)
        results = []
        
        for res in Weapon.objects.filter(name__icontains=term).order_by('name'):
            results.append({
                'value': reverse('gm:weapon_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Arme'), res.name.upper())
            })
        
        for res in Module.objects.filter(name__icontains=term).order_by('name'):
            results.append({
                'value': reverse('gm:module_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Module'), res.name.upper())
            })
        
        for res in Overdrive.objects.filter(characteristic__name__icontains=term).order_by('characteristic__name', 'level'):
            results.append({
                'value': reverse('gm:overdrive_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Overdrive'), str(res).upper())
            })
        
        for res in Enhancement.objects.filter(name__icontains=term).order_by('name'):
            results.append({
                'value': reverse('gm:enhancement_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Amélioration'), res.name.upper())
            })
        
        for res in Effect.objects.filter(name__icontains=term).order_by('name'):
            results.append({
                'value': reverse('gm:effect_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Effet'), res.name.upper())
            })
        
        for res in HeroicSkill.objects.filter(name__icontains=term).order_by('name'):
            results.append({
                'value': reverse('gm:skill_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Capacité héroïque'), res.name.upper())
            })
        
        for res in Archetype.objects.filter(name__icontains=term).order_by('name'):
            results.append({
                'value': reverse('gm:archetype_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Archétype'), res.name.upper())
            })
        
        for res in MajorArcana.objects.filter(name__icontains=term).order_by('name'):
            results.append({
                'value': reverse('gm:arcana_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Arcane majeure'), res.name.upper())
            })
        
        for res in GreatDeed.objects.filter(name__icontains=term).order_by('name'):
            results.append({
                'value': reverse('gm:great_deed_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Haut-fait'), res.name.upper())
            })
        
        for res in Crest.objects.filter(name__icontains=term).order_by('name'):
            results.append({
                'value': reverse('gm:crest_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Blason'), res.name.upper())
            })
        
        for res in Armour.objects.filter(name__icontains=term).order_by('name'):
            results.append({
                'value': reverse('gm:armour_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Armure'), res.name.upper())
            })
        
        for res in Division.objects.filter(name__icontains=term).order_by('name'):
            results.append({
                'value': reverse('gm:division_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Section'), res.name.upper())
            })
        
        for res in Vehicle.objects.filter(name__icontains=term).order_by('name'):
            results.append({
                'value': reverse('gm:vehicle_detail', kwargs={'slug': res.slug}),
                'label': "[%s] %s" % (gettext('Véhicule'), res.name.upper())
            })
        
        if request.user.is_authenticated:
            allowed_traumas = Trauma.objects.filter(password=None)
            if request.user.game_master:
                allowed_traumas = Trauma.objects.all()
            else:
                allowed_traumas = allowed_traumas.union(request.user.unlocked_traumas.all())
            traumas_pk_allowed = list(allowed_traumas.values_list('pk', flat=True).distinct())
            
            for res in Trauma.objects.filter(name__icontains=term, pk__in=traumas_pk_allowed).order_by('name'):
                results.append({
                    'value': reverse('gm:trauma_detail', kwargs={'slug': res.slug}),
                    'label': "[%s] %s" % (gettext('Trauma'), res.name.upper())
                })
            
            allowed_mechas = MechaArmour.objects.filter(password=None)
            if request.user.game_master:
                allowed_mechas = MechaArmour.objects.all()
            else:
                allowed_mechas = allowed_mechas.union(request.user.unlocked_mechas.all())
            mechas_pk_allowed = list(allowed_mechas.values_list('pk', flat=True).distinct())
            
            for res in MechaArmour.objects.filter(name__icontains=term, pk__in=mechas_pk_allowed).order_by('name'):
                results.append({
                    'value': reverse('gm:mecha_detail', kwargs={'slug': res.slug}),
                    'label': "[%s] %s" % (gettext('Mecha'), res.name.upper())
                })
            
            if request.user.game_master:
                for res in NonPlayerCharacterCapacity.objects.filter(name__icontains=term).order_by('name'):
                    results.append({
                        'value': reverse('gm:npc_capacity_detail', kwargs={'slug': res.slug}),
                        'label': "[%s] %s" % (gettext('Capacité PNJ'), res.name.upper())
                    })
        
        # results = sorted(results, key=lambda obj: obj['label'])
        
        return JsonResponse(results, safe=False)
    
def generateAI(request, codename=None):
    def get_random():
        keys = MajorArcana.objects.values_list('id', flat=True)
        return sample(list(keys), 5)
    
    template_name = 'gm/misc/generator_ai.html'
    keys = []
    ai_letters = []
    ai_fullcode = ''
    
    if codename is not None:
        codenames = codename.upper().split('.')
        
        if len(codenames) < 5:
            keys = get_random()
        else:
            arcanas = list(MajorArcana.objects.filter(ai_letter__in=codenames))
            if (len(arcanas) < 5):
                keys = get_random()
            else:
                arcanas.sort(key=lambda arcana: codenames.index(arcana.ai_letter))
                
                for a in arcanas:
                    keys.append(a.id)
    else:
        keys = get_random()
    
    final_arcanas = list(MajorArcana.objects.filter(pk__in=keys))
    final_arcanas.sort(key=lambda arcana: keys.index(arcana.pk))
    
    for arcana in final_arcanas:
        ai_letters.append(arcana.ai_letter)
        arcana.ai_keyword = choice(arcana.ai_keywords.split(','))
    
    ai_fullcode = '.'.join(ai_letters)
    
    return render(request, template_name, {
        'ai_list': final_arcanas,
        'ai_fullcode': ai_fullcode
    })
    
class SetTheme(generic.View):
    
    def post(self, request, *args, **kwargs):
        data = {
            'updated': False,
        }
        
        try:
            theme = request.POST.get('theme', '')
            request.session['theme_name'] = theme
            # import pdb; pdb.set_trace()
            if request.user.is_authenticated:
                request.user.theme_name = theme
                request.user.save()
                
            data['updated'] = True
        except Exception as e:
            data = {
                'error': str(e)
            }
        finally:
            return JsonResponse(data)
    