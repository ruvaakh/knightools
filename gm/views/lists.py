from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views import generic
from django.db.models import Min, Max, Count, Q
from django.contrib.auth.mixins import LoginRequiredMixin

from ..models.character import Armour, Division, MajorArcana, Crest, Archetype, GreatDeed, Trauma, MechaArmour, NonPlayerCharacterCapacity, NonPlayerCharacterType
from ..models.weaponry import Weapon, Effect, Module, Overdrive, Enhancement, HeroicSkill, Rarity, Source, Activation, Vehicle

class WeaponListView(generic.ListView):
    template_name = 'gm/lists/weapon.html'
    context_object_name = 'weapon_list'
    
    def get_context_data(self, **kwargs):
        context = super(WeaponListView, self).get_context_data(**kwargs)
        context['rarities'] = Rarity.objects.filter(pk__in=Weapon.objects.values_list('rarity__pk', flat=True).distinct())
        context['sources'] = Source.objects.filter(pk__in=Weapon.objects.values_list('source__pk', flat=True).distinct())
        return context
        
    def get_queryset(self):
        """Return list of weapon."""
        return (Weapon
            .objects
            .filter(
                player_enabled=True
            )
            .select_related('rarity', 'category')
            .order_by('category', 'rarity', 'name'))


class EffectListView(generic.ListView):
    template_name = 'gm/lists/effect.html'
    context_object_name = 'effect_list'

    def get_context_data(self, **kwargs):
        context = super(EffectListView, self).get_context_data(**kwargs)
        context['sources'] = Source.objects.filter(pk__in=Effect.objects.values_list('source__pk', flat=True).distinct())
        return context
    
    def get_queryset(self):
        """Return list of effect."""
        return (Effect.
            objects
            .order_by('name'))


class ModuleListView(generic.ListView):
    template_name = 'gm/lists/module.html'
    context_object_name = 'module_list'

    def get_context_data(self, **kwargs):
        context = super(ModuleListView, self).get_context_data(**kwargs)
        context['rarities'] = Rarity.objects.filter(pk__in=Module.objects.values_list('levels__rarity__pk', flat=True).distinct())
        context['sources'] = Source.objects.filter(pk__in=Module.objects.values_list('source__pk', flat=True).distinct())
        context['activations'] = Activation.objects.filter(pk__in=Module.objects.values_list('levels__activation__pk', flat=True).distinct())
        return context
        
    def get_queryset(self):
        """Return list of module."""
        return (Module
            .objects
            .annotate(
                min_rarity=Min('levels__rarity__id'),
                max_level=Max('levels__level')
            )
            .select_related('category')
            .prefetch_related('levels', 'levels__rarity')
            .order_by('category', 'min_rarity', 'name'))


class OverdriveListView(generic.ListView):
    template_name = 'gm/lists/overdrive.html'
    context_object_name = 'overdrive_list'

    def get_queryset(self):
        """Return list of overdrive."""
        return (Overdrive
            .objects
            .select_related('characteristic', 'characteristic__aspect', 'rarity')
            .order_by('characteristic__aspect__id', 'characteristic__id', 'level'))


class EnhancementListView(generic.ListView):
    template_name = 'gm/lists/enhancement.html'
    context_object_name = 'enhancement_list'

    def get_context_data(self, **kwargs):
        context = super(EnhancementListView, self).get_context_data(**kwargs)
        context['sources'] = Source.objects.filter(pk__in=Enhancement.objects.values_list('source__pk', flat=True).distinct())
        return context
    
    def get_queryset(self):
        """Return list of enhancement."""
        return (Enhancement
            .objects
            .annotate(
                with_conditions=Count(
                    'effects__effect_condition',
                    # filter=Q(effects__effect_condition__regex=r'^.+$')
                    filter=~Q(effects__effect_condition='') # The ~ reverse the Q list to get all objects where condition is NOT ''.
                )
            )
            .select_related('category')
            .prefetch_related('effects')
            .order_by('group', 'name'))


class ArmourListView(generic.ListView):
    template_name = 'gm/lists/armour.html'
    context_object_name = 'armour_list'

    def get_context_data(self, **kwargs):
        context = super(ArmourListView, self).get_context_data(**kwargs)
        context['sources'] = Source.objects.filter(pk__in=Armour.objects.values_list('source__pk', flat=True).distinct())
        allowed_armours = Armour.objects.filter(password=None)
        if self.request.user.is_authenticated:
            if self.request.user.game_master:
                allowed_armours = Armour.objects.all()
            else:
                allowed_armours = allowed_armours.union(self.request.user.unlocked_armours.all())
        context['allowed_armours'] = allowed_armours
        return context
    
    def get_queryset(self):
        """Return list of armour."""
        return (Armour
            .objects
            .prefetch_related('abilities', 'overdrives', 'overdrives__characteristic')
            .order_by('name'))


class DivisionListView(generic.ListView):
    template_name = 'gm/lists/division.html'
    context_object_name = 'division_list'

    def get_queryset(self):
        """Return list of division."""
        return (Division
            .objects
            .prefetch_related('overdrives', 'overdrives__characteristic', 'modules', 'modules__module_level', 'modules__module_level__module', 'modules__forced_choice_effect', 'weapons')
            .order_by('name'))


class ArcanaListView(generic.ListView):
    template_name = 'gm/lists/arcana.html'
    context_object_name = 'arcana_list'

    def get_queryset(self):
        """Return list of arcana."""
        return (MajorArcana
            .objects
            .select_related('forced_aspect')
            .order_by('number'))


class CrestListView(generic.ListView):
    template_name = 'gm/lists/crest.html'
    context_object_name = 'crest_list'

    def get_queryset(self):
        """Return list of crest."""
        return (Crest
            .objects
            .order_by('name'))


class ArchetypeListView(generic.ListView):
    template_name = 'gm/lists/archetype.html'
    context_object_name = 'archetype_list'

    def get_queryset(self):
        """Return list of archetype."""
        return (Archetype
            .objects
            .select_related('characteristic_bonus_1', 'characteristic_bonus_2', 'characteristic_bonus_3')
            .order_by('name'))


class GreatDeedListView(generic.ListView):
    template_name = 'gm/lists/great_deed.html'
    context_object_name = 'great_deed_list'

    def get_queryset(self):
        """Return list of great_deed."""
        return (GreatDeed
            .objects
            .select_related('aspect_1', 'aspect_2')
            .order_by('name'))


class HeroicSkillListView(generic.ListView):
    template_name = 'gm/lists/skill.html'
    context_object_name = 'skill_list'

    def get_queryset(self):
        """Return list of heroic skill."""
        return (HeroicSkill
            .objects
            .select_related('category')
            .order_by('category', 'name'))


class VehicleListView(generic.ListView):
    template_name = 'gm/lists/vehicle.html'
    context_object_name = 'vehicle_list'

    def get_queryset(self):
        """Return list of vehicles."""
        return (Vehicle
            .objects
            .order_by('name'))

class TraumaListView(LoginRequiredMixin, generic.ListView):
    template_name = 'gm/lists/trauma.html'
    context_object_name = 'trauma_list'

    def get_context_data(self, **kwargs):
        context = super(TraumaListView, self).get_context_data(**kwargs)
        allowed_traumas = Trauma.objects.filter(password=None)
        if self.request.user.is_authenticated:
            if self.request.user.game_master:
                allowed_traumas = Trauma.objects.all()
            else:
                allowed_traumas = allowed_traumas.union(self.request.user.unlocked_traumas.all())
        context['allowed_traumas'] = allowed_traumas
        
        return context
    
    def get_queryset(self):
        """Return list of traumas."""
        return (Trauma
            .objects
            .select_related('category')
            .order_by('category__hop_recovered', 'die_value'))


class MechaArmourListView(LoginRequiredMixin, generic.ListView):
    template_name = 'gm/lists/mecha.html'
    context_object_name = 'mecha_list'

    def get_context_data(self, **kwargs):
        context = super(MechaArmourListView, self).get_context_data(**kwargs)
        allowed_mechas = MechaArmour.objects.filter(password=None)
        if self.request.user.is_authenticated:
            if self.request.user.game_master:
                allowed_mechas = MechaArmour.objects.all()
            else:
                allowed_mechas = allowed_mechas.union(self.request.user.unlocked_mechas.all())
        context['allowed_mechas'] = allowed_mechas
        return context
    
    def get_queryset(self):
        """Return list of mecha."""
        return (MechaArmour
            .objects
            .prefetch_related('configurations')
            .order_by('name'))
    
class NonPlayerCharacterCapacityListView(LoginRequiredMixin, generic.ListView):
    template_name = 'gm/lists/npc_capacity.html'
    context_object_name = 'capacity_list'
    
    def get_context_data(self, **kwargs):
        context = super(NonPlayerCharacterCapacityListView, self).get_context_data(**kwargs)
        context['npc_types'] = NonPlayerCharacterType.objects.all()
        return context
        
    def get_queryset(self):
        """Return list of npc capacities."""
        return (NonPlayerCharacterCapacity
            .objects
            .prefetch_related('types'))
    
