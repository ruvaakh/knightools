Installer KnightOOLS sur Windows via Chocolatey
===============================================================================

Installer Chocolatey
-------------------------------------------------------------------------------

Allez sur le site dédié à
`chocolatey <https://chocolatey.org/install>`_ et suivez les instructions pour
installer l'outil. Chocolatey vous permettra ensuite d'installer toutes les
autres dépendances plus facilement.


Installer Git, Python, Docker et MariaDB
-------------------------------------------------------------------------------

Depuis le même terminal où vous avez installé chocolatey (si vous êtes passé
par une autre méthode : faites ``windows + X`` et choisissez l'option
"Windows PowerShell (admin)", puis tapez la commande suivante :


.. code-block:: bash

   choco install -y python git mariadb docker


Installer virtualenv
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Fermez le terminal et rouvrez-le (afin qu'il prenne en compte les nouveaux
programmes installés), tapez la commande suivante :

.. code-block:: bash

   pip install virtualenvwrapper-win


Configurer MariaDB
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Lancez HeidiSQL (installé automatiquement avec MariaDB), il vous demande les
informations de session. Mettez un nom de session, connectez-vous en tant que
root, et vous arrivez sur l'interface suivante :

.. image:: img/heidisql-home-windows.png
   :align: center
   :alt: Accueil HeidiSQL

Faites un clic-droit sur le nom de la session (sur l'impression écran
précédente, le nom est "root"), puis faites ``Nouvelle -> Base de données``.
Donnez le nom que vous souhaitez à votre base de données, mettez bien comme
Collation ``utf8_general_ci`` puis faites OK.

.. image:: img/heidisql-create-db-windows.png
   :align: center
   :alt: Création base de données knightools

Il nous reste un dernier point à gérer : créer l'utilisateur. Pour cela,
toujours dans HeidiSQL, allez dans ``Outils -> Gestionnaire utilisateur`` puis
cliquez sur Ajouter. Renseignez les informations comme suit :

.. image:: img/heidisql-create-user-windows.png
   :align: center
   :alt: Création utilisateur knightools


Cloner le projet Knightools
-------------------------------------------------------------------------------

Si votre but est seulement d'utiliser KnightOOLS hors ligne depuis votre réseau
local sans contribuer au projet, vous pouvez directement cloner la version
http du projet. Faites un clic droit dans le dossier sur lequel vous souhaitez
installer knightools, puis faites au choix : ``Git GUI Here`` ou
``Git Bash Here``. Si vous faites ``Git Bash Here``, voici la commande à
exécuter :

.. code-block:: bash

   git clone https://gitlab.com/Renkineko/knightools.git

Si vous optez pour l'option graphique (``Git GUI Here``), faites ensuite
``Clone existing repository``, et saisissez en source :
https://gitlab.com/Renkineko/knightools.git et en target le nom du dossier
(généralement on garde le même nom que pour le projet, ici : ``knightools``).

.. image:: img/git-gui-clone.png
   :align: center
   :alt: Clone du dépôt git en interface graphique

Si vous souhaitez au contraire contribuer au projet, faire des modifications,
il vous faudra d'abord un compte gitlab sur lequel faire ce que l'on appelle un
fork, c'est-à-dire que vous allez avoir une "copie" du projet mais vous
appartenant, et les modifications que vous ferez sur votre projet pourront être
envoyées au projet initial. Lisez le guide de contribution pour connaître
le workflow utilisé pour ce projet.

Voici en ligne de commande (avec l'option ``Git Bash Here``) ce que vous devez
taper :

.. code-block:: bash

   git clone git@gitlab.com:<votre-pseudo>/knightools.git
   cd knightools
   git remote add upstream git@gitlab.com:Renkineko/knightools.git

Créer son Virtualenv
-------------------------------------------------------------------------------

Afin de faire les choses proprement et éviter des conflits systèmes, nous
allons créer un environnement virtuel pour exécuter le projet. Cela n'ajoute
rien en complexité, et ne coûte qu'une ou deux commandes supplémentaires. Notez
que vous pouvez passer cette étape, mais il est conseillé de la faire.

Dans l'exemple qui suit, nous utiliserons comme nom d'environnement virtuel
"venv", mais il y a deux noms possibles qui sont ignorés par défaut par la
configuration git du projet : "venv" ou "knightenv". Positionnez-vous depuis
votre terminal dans le dossier du projet, et exécutez la commande suivante :

.. code-block:: bash

   mkvirtualenv venv

Désormais, vous êtes sûr que le projet ne devrait pas entrer en conflit avec
d'autres projets python que vous pourriez avoir, maintenant ou à l'avenir.

A noter que les prochaines fois que vous avez besoin de lancer l'environnement
virtuel, vous aurez juste à vous positionner dans le dossier en invite de
commande (cmd.exe et pas powershell) et à exécuter la commande :

.. code-block:: bash

   workon venv

Finaliser l'installation
-------------------------------------------------------------------------------

Installation des applications python
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

L'une des applications utilisée par knightools nécessite des outils
supplémentaires de Microsoft disponible
`dans ce lien <http://go.microsoft.com/fwlink/?LinkId=691126&fixForIE=.exe.>`_
pour pouvoir être installés. La version 2015 est très fonctionnelle pour ce
dont on a besoin.

Accédez depuis un terminal au dossier où se trouve votre projet, puis exécutez
les commandes suivantes (notez le nom de l'environnement virtuel sur la
première commande, n'oubliez pas d'adapter si vous en avez choisi un autre) :

.. code-block:: bash

   workon venv
   pip install -r work/requirements.pip.txt
   pip install pypiwin32

Si vous avez toujours des erreurs, suivez les instructions sur
`cette page <https://www.scivision.co/python-windows-visual-c-14-required/>`_

Configuration de la base de données
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Copiez le fichier my.cnf.example en lui donnant le nom my.cnf et en modifiant
son contenu. Par exemple, si vous avez comme nom de base de données
``knightools-db``, comme nom d'utilisateur mysql ``knightools-user`` et comme
mot de passe ``knightools-pwd``, voici la configuration à saisir :

.. code-block:: ini
   :linenos:
   :caption: my.cnf
   :name: my.cnf

   [client]
   database=knightools-db
   user=knightools-user
   password=knightools-pwd
   host=localhost
   port=3306
   default-character-set=utf8

Migration des models :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Enfin, il ne vous reste plus qu'à exécuter la commande suivante pour en finir
avec l'installation de l'application.

.. code-block:: bash

   python manage.py migrate

Exécuter l'application
-------------------------------------------------------------------------------

Pour exécuter l'application, vous avez juste à lancer la commande suivante,
toujours en étant dans l'environnement virtuel :

.. code-block:: bash

   python manage.py runserver

Puis depuis votre navigateur, accédez à l'adresse `<http://localhost:8000>`_.
