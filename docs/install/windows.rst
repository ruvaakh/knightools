Installer KnightOOLS sur Windows
===============================================================================

Préambule
-------------------------------------------------------------------------------

Knightools n'est pas fait pour tourner sur Windows. Ce guide ne permet pas
d'utiliser knightools à son plein potentiel. En effet, l'installation de tous
les composants sous windows requiert une patience et une connaissance à toute
épreuve.

Si vous avez une machine assez puissante pour vous permettre de faire tourner
une machine virtuelle, je ne peux que vous conseiller de vous pencher plutôt
sur l'installation d'une VM sous linux qui ne servira qu'à faire tourner le
site, plutôt que d'installer énormément de programmes qui vont potentiellement
se lancer au démarrage de l'OS ou que vous devrez lancer à la main et, de fait,
dont vous devrez vous souvenir. Dans un cas, ça ralentira l'OS inutilement,
dans l'autre ce ne sera juste pas pratique.

Si un expert en installation de projet normalement-pas-si-compliqué passe et
souhaite tenter d'écrire une documentation d'installation **vraiment** complète
sous Windows, les contributions sont là pour ça. En attendant, ce guide vous
permettra d'avoir au moins accès aux fonctionnalités de base du site. Il vous
manquera surtout la possibilité de gérer des tables virtuelles.


Installer Git, MariaDB, Python 3.6, virtualenv et pip
-------------------------------------------------------------------------------


Git
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Rendez vous sur `Git for Windows <https://gitforwindows.org>`_ et téléchargez
le programme. Installez-le, en prenant soin d'activer les options suivantes :

Choix de l'éditeur par défaut : je vous conseille d'utiliser nano. Il est
simple d'utilisation, moins puissant que vim mais beaucoup plus intuitif. Si
vous préférez un éditeur graphique, prenez celui que vous préférez.

Laissez toutes les autres options par défaut, cela fera l'affaire.

Python 3.6, pip et virtualenv
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Allez sur le site de `Python <http://www.python.org/>`_ et téléchargez la
dernière version installable de Python 3.6. Notez que vous pouvez sûrement
utiliser une version plus récente de Python, mais au moment où ce guide est
écrit, c'est avec Python 3.6 que le projet a été développé et testé.

Lors de l'installation, acceptez d'installer aussi pip et ajoutez Python 3.6
dans le PATH du système, tel que présentez sur le screen ci-dessous :

.. image:: img/python3.6-windows.png
   :align: center
   :alt: Installation python3.6

Une fois installé, ouvrez une invite de commande (cmd.exe) et tapez la commande
suivante :

.. code-block:: bash

   python -m pip install --upgrade pip
   pip install virtualenvwrapper-win


MariaDB
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Il nous reste un dernier outil à installer, le gestionnaire de base de données.
Allez sur le site de `MariaDB <https://downloads.mariadb.org/>`_ et téléchargez
la dernière version pour windows.

Lorsque l'installeur vous demande le mot de passe root, ne cochez pas l'accès à
root depuis les machines distantes (plus sécurisé ainsi), mettez un mot de
passe que vous pouvez retenir et taper facilement, et enfin cochez l'option sur
l'UTF8, comme suit :

.. image:: img/mariadb-windows.png
   :align: center
   :alt: Installation MariaDB

Tout le reste de l'installation vous pouvez laisser les options par défaut.

Le package vous propose un client graphique nommé HeidiSQL. Lancez-le, il vous
demande les informations de session. Mettez un nom de session, connectez-vous
en tant que root, et vous arrivez sur l'interface suivante :

.. image:: img/heidisql-home-windows.png
   :align: center
   :alt: Accueil HeidiSQL

Faites un clic-droit sur le nom de la session (sur l'impression écran
précédente, le nom est "root"), puis faites ``Nouvelle -> Base de données``.
Donnez le nom que vous souhaitez à votre base de données, puis faites OK.

.. image:: img/heidisql-create-db-windows.png
   :align: center
   :alt: Création base de données knightools

Il nous reste un dernier point à gérer : créer l'utilisateur. Pour cela,
toujours dans HeidiSQL, allez dans ``Outils -> Gestionnaire utilisateur`` puis
cliquez sur Ajouter. Renseignez les informations comme suit :

.. image:: img/heidisql-create-user-windows.png
   :align: center
   :alt: Création utilisateur knightools


Cloner le projet Knightools
-------------------------------------------------------------------------------

Si votre but est seulement d'utiliser KnightOOLS hors ligne depuis votre réseau
local sans contribuer au projet, vous pouvez directement cloner la version
http du projet. Faites un clic droit dans le dossier sur lequel vous souhaitez
installer knightools, puis faites au choix : ``Git GUI Here`` ou
``Git Bash Here``. Si vous faites ``Git Bash Here``, voici la commande à
exécuter :

.. code-block:: bash

   git clone https://gitlab.com/Renkineko/knightools.git

Si vous optez pour l'option graphique (``Git GUI Here``), faites ensuite
``Clone existing repository``, et saisissez en source :
https://gitlab.com/Renkineko/knightools.git et en target le nom du dossier
(généralement on garde le même nom que pour le projet, ici : ``knightools``).

.. image:: img/git-gui-clone.png
   :align: center
   :alt: Clone du dépôt git en interface graphique

Si vous souhaitez au contraire contribuer au projet, faire des modifications,
il vous faudra d'abord un compte gitlab sur lequel faire ce que l'on appelle un
fork, c'est-à-dire que vous allez avoir une "copie" du projet mais vous
appartenant, et les modifications que vous ferez sur votre projet pourront être
envoyées au projet initial. Lisez le guide de contribution pour connaître
le workflow utilisé pour ce projet.

Voici en ligne de commande (avec l'option ``Git Bash Here``) ce que vous devez
taper :

.. code-block:: bash

   git clone git@gitlab.com:<votre-pseudo>/knightools.git
   cd knightools
   git remote add upstream git@gitlab.com:Renkineko/knightools.git

Créer son Virtualenv
-------------------------------------------------------------------------------

Afin de faire les choses proprement et éviter des conflits systèmes, nous
allons créer un environnement virtuel pour exécuter le projet. Cela n'ajoute
rien en complexité, et ne coûte qu'une ou deux commandes supplémentaires. Notez
que vous pouvez passer cette étape, mais il est conseillé de la faire.

Dans l'exemple qui suit, nous utiliserons comme nom d'environnement virtuel
"venv", mais il y a deux noms possibles qui sont ignorés par défaut par la
configuration git du projet : "venv" ou "knightenv". Positionnez-vous depuis
votre terminal dans le dossier du projet, et exécutez la commande suivante :

.. code-block:: bash

   mkvirtualenv venv

Désormais, vous êtes sûr que le projet ne devrait pas entrer en conflit avec
d'autres projets python que vous pourriez avoir, maintenant ou à l'avenir.

A noter que les prochaines fois que vous avez besoin de lancer l'environnement
virtuel, vous aurez juste à vous positionner dans le dossier en invite de
commande et à exécuter la commande :

.. code-block:: bash

   workon venv

Finaliser l'installation
-------------------------------------------------------------------------------

Installation des applications python
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

L'une des applications utilisée par knightools nécessite des outils
supplémentaires de Microsoft disponible
`dans ce lien <http://go.microsoft.com/fwlink/?LinkId=691126&fixForIE=.exe.>`_
pour pouvoir être installés. La version 2015 est très fonctionnelle pour ce
dont on a besoin.

Accédez depuis un terminal au dossier où se trouve votre projet, puis exécutez
les commandes suivantes (notez le nom de l'environnement virtuel sur la
première commande, n'oubliez pas d'adapter si vous en avez choisi un autre) :

.. code-block:: bash

   workon venv
   pip install -r work/requirements.pip.txt
   pip install pypiwin32

Configuration de la base de données
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Copiez le fichier my.cnf.example en lui donnant le nom my.cnf et en modifiant
son contenu. Par exemple, si vous avez comme nom de base de données
``knightools-db``, comme nom d'utilisateur mysql ``knightools-user`` et comme
mot de passe ``knightools-pwd``, voici la configuration à saisir :

.. code-block:: ini
   :linenos:
   :caption: my.cnf
   :name: my.cnf

   [client]
   database=knightools-db
   user=knightools-user
   password=knightools-pwd
   host=localhost
   port=3306
   default-character-set=utf8

Migration des models :
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Enfin, il ne vous reste plus qu'à exécuter la commande suivante pour en finir
avec l'installation de l'application.

.. code-block:: bash

   python manage.py migrate


Création du compte admin
-------------------------------------------------------------------------------

Une dernière chose à configurer, c'est le compte administrateur qui aura accès
à l'url d'administration du site. Pour cela, toujours depuis le terminal en
environnement virtuel, faites :

.. code-block:: bash

   python manage.py createsuperuser

L'application va vous demander le login, l'adresse mail et le mot de passe.


Exécuter l'application
-------------------------------------------------------------------------------

Pour exécuter l'application, vous avez juste à lancer la commande suivante,
toujours en étant dans l'environnement virtuel :

.. code-block:: bash

   python manage.py runserver

Puis depuis votre navigateur, accédez à l'adresse `<http://localhost:8000>`_.
