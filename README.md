# Presentation

KnightOOLS is an online tools for player and game master to have a global sight
of the table-top role playing game Knight weaponry (and more).

The project is french, but commits, issues and description should be in english
to help internationalization.

It is powered by django and is mainly used as a tutorial for me (Renkineko).
Anyone who wants to contribute is welcome, either by posting suggestions or
improve the code.

## API

You can find the API for KnightOOLS here:

https://knightools.docs.stoplight.io/api-reference/intro

Right now, the API does not use authentification. It will in the future, the
project is still in alpha state. Enjoy.

If you want to help with the API (documentation or possible usage), do not
hesitate to contact me.